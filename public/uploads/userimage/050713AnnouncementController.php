<?php


namespace App\Admin\Controllers;

/*require "/home/jeyakumar/Public/DMKportal/vendor/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;*/

use App\Models\Announcement;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;
//use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;
use DB;
use Storage;

class AnnouncementController extends Controller
{
    use ModelForm;
    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Announcement');
            $content->description('Management');

            $content->body($this->grid());
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Announcement');
            $content->description('Management');
              $current_date=Carbon::Now()->toDateTimeString();
            $content->body(view('bannouncement',['news'=>'','main_image'=>'','pid'=>"",'mediatype'=>'','image_array'=>array(),'current_date'=>$current_date]));

            //$content->body($this->form());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Announcement');
            $content->description('Management');
            $news = Announcement::whereId($id)->first();
            $current_date=Carbon::Now()->toDateTimeString();
            if(is_object($news) && $news->MediaFilesPath!="")
            {
                $main_image = $news->MediaFilesPath;
                $image_array=explode(",",$news->MediaFilesPath);
            }
            else 
            {
                $main_image = "";
                $image_array = array();
            }

            $content->body(view('bannouncement',['news'=>$news,'main_image'=>$main_image,'pid'=>$id,'image_array' =>$image_array,'current_date'=>$current_date]));
           // $content->body($this->form()->edit($id));
        });
    }

    /**
     * safe interface.
     *
     * @return Content
     */
    public function savenews(Request $request)
    {
       // dd($request->all());
        $picture_single="";
        $multiple_image="";

        $deleted_image = $request->has('deleted_hidden_image') ? explode(",",$request->input('deleted_hidden_image')) : array();
        $hidden_image = $request->has('hidden_image') ? explode(",",$request->input('hidden_image')) : array();
        if(count($hidden_image) > 0)
        {
            $finalresult = array_diff($hidden_image, $deleted_image);
            $multiple_image=implode(",",$finalresult);
        }

        $uploaded_count = $request->input('hiddencount');

        for($i=1;$i<$uploaded_count;$i++)
        {
            if($i==2 && count($deleted_image)>0)
            {

            }
            else
            {
                if($request->has('MediaFilesPathnew'.$i)) 
                {
                    $files = $request->file('MediaFilesPathnew'.$i);
                    if(count($files)>0)
                    {
                        foreach($files as $files_single)
                        {
                            $filename = str_replace("-","_",$files_single->getClientOriginalName()); 
                            if(!in_array($filename,$deleted_image))
                            {
                                $extension = $files_single->getClientOriginalExtension();
                                $picture_single = date('YmdHis').".".$extension;
                                $multiple_image .=($multiple_image!="") ? ",".$picture_single : $picture_single;
                                $destinationPath = base_path() . '/public/uploads/News/';
                                $files_single->move($destinationPath, $picture_single);

                                $filePath = '/News/' . $picture_single;
                                $t = Storage::disk('s3')->put($filePath, file_get_contents($destinationPath.$picture_single), 'public');
                                $imageName = Storage::disk('s3')->url($filePath);
                                unlink($destinationPath.$picture_single);
                            }
                            if(in_array($filename,$deleted_image)){
                                $key = array_search($filename, $deleted_image);
                                unset($deleted_image[$key]);
                            } 
                        }
                    }
                }
            }
        }

        if($request->input('id')!="")
        $news = Announcement::find($request->input('id'));
        else
        $news = new Announcement;

        $news->PublishedBy=  1;
        $news->PostAdminId=  Admin::user()->id;
        $news->Title=  $request->input('Title');
        $news->Content=  $request->input('Content');
        $news->MediaFilesPath=  $multiple_image;
        $news->Status=  $request->input('Status');
        $news->Linktype=  $request->input('Linktype');
        $news->PublishedOn=  $request->input('PublisedOn');
        $news->created_at=  Carbon::now()->toDateTimeString();
        $news->save();

        if($request->input('id')!="")
        admin_toastr('News Updated successfully');
        else
        admin_toastr('News created successfully');

        return redirect('admin/announcement');
    }

    public function view($id)
    {
        return Admin::content(function (Content $content) use ($id) {
            
            $news = Announcement::find($id);
            $content->header('Announcement');
            $content->description('Management');

            $content->body(view('announcementview',['news'=>$news]));

        });
    }

    protected function grid()
    {
        return Admin::grid(Announcement::class, function (Grid $grid) {
           // global $grid;

            $grid->disableExport();   
            $grid->actions(function ($actions) {
                $actions->append('<a href="'.url('/').'/admin/announcement/view/'.$actions->getKey().'"><i class="fa fa-eye"></i></a>');
                $actions->disableDelete();
            });

            $grid->tools(function ($tools) {
                $tools->batch(function ($batch) {
                    $batch->disableDelete();
                });
            });

           
            $grid->Title('News Title')->sortable();
          
            $grid->column('Status','Status')->display(function () {
                return ($this->Status==1)?'Active':'In Active';
            });   
            $grid->filter(function($filter){
                $filter->disableIdFilter();
                $filter->ilike('Title', 'Title');
               // $filter->ilike('district.DistrictName', 'District Name');
               // $filter->ilike('constituency.ConstituencyName', 'Constituency Name');
                $filter->equal('Status','Status')->select([""=>'Test','0' => 'In Active','1'=>'Active']);

            });  

            Admin::script('$( document ).ready(function() { $(".Status").select2({"allowClear":true,"placeholder":"Please Select Status"}); $(":reset").css("display", "none"); $("td .icheckbox_minimal-blue").hide(); });');
        });
    }

    public function deleteimage(Request $request)
    {
       $test = "1";
       return json_encode($test);
    }
}
