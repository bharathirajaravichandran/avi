-- Adminer 4.1.0 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vendor_id` int(11) NOT NULL,
  `accountName` varchar(150) NOT NULL,
  `companyIdNo` varchar(100) DEFAULT NULL,
  `fax` varchar(50) NOT NULL,
  `industry_id` int(11) NOT NULL,
  `website` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `companyCode` varchar(50) DEFAULT NULL,
  `bankAccountNo` varchar(50) DEFAULT NULL,
  `parentAccount_id` int(11) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `accountType_id` int(11) DEFAULT NULL,
  `annualRevenue` int(11) NOT NULL,
  `NoOfEmployees` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `leads_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `industry_id` (`industry_id`),
  KEY `user_id` (`user_id`),
  KEY `accountType_id` (`accountType_id`),
  KEY `leads_id` (`leads_id`),
  KEY `vendor_id` (`vendor_id`),
  CONSTRAINT `accounts_ibfk_1` FOREIGN KEY (`industry_id`) REFERENCES `industry` (`id`),
  CONSTRAINT `accounts_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `accounts_ibfk_3` FOREIGN KEY (`accountType_id`) REFERENCES `accountTypes` (`id`),
  CONSTRAINT `accounts_ibfk_4` FOREIGN KEY (`leads_id`) REFERENCES `leads` (`id`),
  CONSTRAINT `accounts_ibfk_5` FOREIGN KEY (`vendor_id`) REFERENCES `vendors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `accounts` (`id`, `vendor_id`, `accountName`, `companyIdNo`, `fax`, `industry_id`, `website`, `user_id`, `companyCode`, `bankAccountNo`, `parentAccount_id`, `phone`, `accountType_id`, `annualRevenue`, `NoOfEmployees`, `status`, `created_at`, `updated_at`, `leads_id`) VALUES
(1,	8,	'dsf',	NULL,	'045224654657',	1,	'http://testsite.com',	32,	NULL,	NULL,	NULL,	'8122337553',	NULL,	1000000,	NULL,	1,	'2019-01-18 14:18:35',	'2019-01-18 14:18:35',	8),
(2,	10,	'SM technology',	NULL,	'514545684',	1,	'http://www.smtechnologies.com/',	35,	NULL,	NULL,	NULL,	'566845154454',	NULL,	20005,	NULL,	1,	'2019-01-25 06:10:36',	'2019-01-25 06:10:36',	14);

-- 2019-01-25 09:10:11
