@include('header')
@include('sidebar')
  <style>
.error {
      color: red;
      
   }
</style>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                                @if($pid=="")
                               Add City
                               @else
                               Edit City
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/city')}}">City</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                    <form method="POST" action="{{url('city/savecity')}}" id="countryforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                              {{ csrf_field() }}
                              <div class="card-body">
                              <div class="row">
                              <div class="col-sm-6 input_field_sections">

                                <h5>Country</h5>
                                      <select class="form-control Country" style="width: 100%;" name="Country" id="Country" onchange="statelist(this.value);" >
                                  <option value="">Please select Country</option>
                                     @foreach ($country as $k=>$v)
                                          <option value="{{ $k }}"{{ (isset($city->country_id) && $city->country_id==$k)  ? 'selected': '' }}>{{ $v }}</option>
                                      @endforeach
                                 </select>
                              </div>
                     <div class="col-sm-6 input_field_sections">
                        <h5>State</h5>
                           <select class="form-control State" style="width: 100%;" name="State" id="State" onchange="citylist(this.value);">
                              <option value="">Please select State</option>
                                @if (($pid) != '')
                                @foreach ($states as $k=>$v)
                                    <option value="{{ $k }}" {{ (isset($city->state_id) && $city->state_id==$k)  ? 'selected': '' }}>{{ $v }}</option>
                                @endforeach
                              @endif
                            
                           </select>
                     </div>
                              </div>
                                      <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                      <h5>City Name</h5>
                                          <input class="form-control input-height" name="name" type="text" id="name" value="{{ old('name', isset($city->name) ? $city->name : null) }}" minlength="1" maxlength="30" required="true" placeholder="Enter City Name..">
                                      
                                         </div> 
                                            <div class="col-sm-6 input_field_sections">
                                          <h5>Status</h5>
                                             <select class="form-control status" style="width: 100%;" name="status" id="status"  >
                                             <option value="1">Active</option>
                                             <option value="0" {{ (isset($city->status) && $city->status==0)  ? 'selected': '' }}>InActive</option>
                                             </select>
                                        </div>
                                       </div> 
                               
                  
                                                 
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit">
                              <a  href="/city">  <input type="button" class="btn btn-default" value="Cancel"></a>
                            </div>
                        </div>
                    </div>
                   </form>
                   
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>
   

   @include('footer')


<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"
   type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script language="javascript">

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");
$('input, text').blur(function() {
        var value = $.trim( $(this).val() );
        $(this).val( value );
    });

$(function() {
  
   $( "#countryforms" ).validate({
       rules: {
                Country: {
            required: true,
              },
        name: {
            required: true,
            minlength:3,
            maxlength:30
              }, 
       State: {
            required: true,
          
              },  
        },
       messages: {
        Country: {
            required: " Please Select Country Name"
                 },
        name: {
            required: "Please Enter City Name"
                 },     
        State: {
            required: "Please Select State Name"
          
                 },
        
      },
      submitHandler: function(form){
        $('form input[type=submit]').prop('disabled', true);
        form.submit();
      },
   });  
});


</script>
<script>
   function statelist(vals)
    {
        url = "/getstatelist?q="+vals;
        $.ajax({
            type : "GET",
            url : url,
            beforeSend : function() {

            },
            success : function(data) {
                $('#State').html(data);
                $("#State").select2({"allowClear":false});            
            },
            error : function(xhr, ajaxOptions, thrownError) {
            },
        });
    }


</script>
