@include('header')
<!-- /#left -->
<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                     Add Food Type
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/foodtype"> Food Type</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <form action="{{url('foodtype/savefoodtype')}}" method="post" id="myforms" name="myform">
         <div class="inner bg-container forms">
            <div class="row">
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Food Type Name</h5>
                              <input type="text" class="form-control" name="foodtype" id="foodtype" value="{{old('name', isset($foodtypedetails->name) ? $foodtypedetails->name : null) }}" minlength="1" maxlength="30" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Status</h5>
                              <select class="form-control" id="status" name="status">
                                 <option  value="">Please Select Status</option>
                                 <option value="1" {{ old('status', isset($foodtypedetails->status) ? $foodtypedetails->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                  <option value="0" {{ old('status', isset($foodtypedetails->status) ? $foodtypedetails->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                              </select> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.row -->
            <div class=" m-t-35">
               <div class="form-actions form-group row">
                  <div class="col-xl-12 text-center">
                     <input type="hidden" name="id" id="pid" value="{{$id}}">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                     <input type="submit" class="btn btn-primary" value="Submit">
                     <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                  </div>
               </div>
            </div>
         </div>
         </form>
         <!-- /.outer -->
         <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
            aria-hidden="true">
            <form>
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span class="float-right" aria-hidden="true">&times;</span>
                     </button>
                     <div class="input-group search_bar_small">
                        <input type="text" class="form-control" placeholder="Search..." name="search">
                        <span class="input-group-btn">
                        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->

<style type="text/css">
   body
   {
   font-family: Arial, Sans-serif;
   }
   .error
   {
   color:red;
   font-family:verdana, Helvetica;
   }
</style>
@include('footer')

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
   $(document).ready(function(){
       $.validator.addMethod("lettersonly", function(value, element) {
   return this.optional(element) || /^[a-zA-Z\s& ]+$/i.test(value);
   }, "Alpha Numeric characters only allowed."); 
   });
</script>
<script type="text/javascript">
   $(function()
   {
   $("#myforms").validate(
     {
        rules:{
           foodtype:{
               required:true,
               lettersonly:true,
               remote:{
                url : "{{ url('/') }}/foodtypeexist",
                type: 'GET',
                data :{ 
                  id: $('#pid').val(),
                  foodtype: this.value,
               },
                 complete: function(data) {
                        //console.log(data);
                    }
               }
           },
           status:{
               required:true,
           },
        },
        messages:{
         foodtype:{
              required:"Food Type Name Field is required",
              lettersonly:"Food Type Name Field is invalid",
              remote:"Food Type Name Field is already exist",
         },
         status:{
              required:"Status Field is required",
              
         },              
        },
        submitHandler: function(form){
        $('form input[type=submit]').prop('disabled', true);
        form.submit();
      },
    });   
});

   $("#cancelform").click(function() {
       window.location.href = "{{url('/foodtype')}}";
   }); 
</script>