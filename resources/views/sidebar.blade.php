<!-- start sidebar menu -->
<?php
if(Auth::check())
{
    $allowed = 0;
     $modulesid = array();
    $userdetails =  App\Models\Users::where('id',Auth::user()->id)->first(); 
    if(is_object($userdetails))
    {
        if($userdetails->user_type_id ==1)
        $allowed = 1;
        if($userdetails->user_type_id ==2)
        {
            $permissionmodules = App\Models\Userpermissions::where('user_id',$userdetails->id)->get();
            if(count($permissionmodules) > 0) 
            {
                foreach ($permissionmodules as $permission_modules) {
                    $modulesid[] = $permission_modules->module_id;
                }
            }
        }
    }
    $profileimage = ($userdetails->profile_img!=null) ? $userdetails->profile_img : "defaultprofile.jpg";
    $name = ($userdetails->first_name!="") ? $userdetails->first_name : "User";
    $role = ($userdetails->user_type_id=="1") ? "Administrator" : "Sub Admin";
}
?>
@if(Auth::check())
<div id="left">
   <div class="menu_scroll">
      <div class="left_media">
         <div class="media user-media">
            <div class="user-media-toggleHover">
               <span class="fa fa-user"></span>
            </div>
            <div class="user-wrapper" style="width:100%">
               <a class="user-link" href="{{url('/')}}/editprofile/{{$userdetails->id}}">
                  <img class="media-object img-thumbnail user-img rounded-circle admin_img3" alt="User Picture"
                     src="{{url('/')}}/uploads/userimage/{{$profileimage}}">
                  <p class="user-info menu_hide" style="display: inline-block;width: 164px;white-space: nowrap;overflow: hidden !important;text-overflow: ellipsis;">Welcome {{$name}}</p>
                  <div class="roal_abso">
                  <p class="user-info role">{{$role}}</p>
                  </div>
               </a>
            </div>
         </div>
         <hr/>
      </div>
      <ul id="menu">
          @if($allowed ==1)
         <li>
            <a href="{{url('/')}}/admin">
            <i class="fa fa-user-secret"></i>
            <span class="link-title menu_hide">&nbsp;Admins</span>
            </a>
         </li>
         @endif

         @if($allowed ==1 || in_array(11,$modulesid))
         <li>
            <a href="{{url('/')}}/subadmin">
            <i class="fa fa-user-secret"></i>
            <span class="link-title menu_hide">&nbsp;Sub Admins</span>
            </a>
         </li>
         @endif

        @if($allowed ==1 || in_array(1,$modulesid))
         <li>
            <a href="{{url('/')}}/users">
            <i class="fa fa-users"></i>
            <span class="link-title menu_hide">&nbsp;User Management </span>
            </a>
         </li>
        @endif
        @if($allowed ==1 || in_array(2,$modulesid))
         <li>
            <a href="{{url('/')}}/airport">
            <i class="fa fa-plane"></i>
            <span class="link-title menu_hide">&nbsp;Airport Management</span>
            </a>
         </li> 
        @endif
           @if($allowed ==1 || in_array(6,$modulesid))
         <li>
            <a href="{{url('/')}}/terminal">
            <i class="fa fa-road"></i>
            <span class="link-title menu_hide">&nbsp;Terminal Management</span>
            </a>
         </li>
         @endif

         @if($allowed ==1 || in_array(7,$modulesid) || in_array(3,$modulesid) || in_array(4,$modulesid))
            <li class="dropdown_menu">
            <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span class="link-title menu_hide">&nbsp; Service  Management</span>
            <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              @if($allowed ==1 || in_array(7,$modulesid))
              <li>
                  <a href="{{url('/')}}/servicetype">
                  <i class="fa fa-angle-right"></i>
                  &nbsp;Service Type
                  </a>
               </li>
                    @endif
               @if($allowed ==1 || in_array(3,$modulesid))
               <li>
                   <a href="{{url('/')}}/restaurants">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Restaurant
                  </a>
               </li>
               @endif
                 @if($allowed ==1 )
               <li>
                   <a href="{{url('/')}}/retailtype">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Shop Type
                  </a>
               </li>
               @endif
                   @if($allowed ==1 )
               <li>
                   <a href="{{url('/')}}/retailshop">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Shop
                  </a>
               </li>
               @endif  
               @if($allowed ==1 )
               <li>
                   <a href="{{url('/')}}/transport">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Transport
                  </a>
               </li>
               @endif     
               @if($allowed ==1 )
               <li>
                   <a href="{{url('/')}}/vehicle">
                  <i class="fa fa-angle-right"></i>
                  &nbsp;Vehicle
                  </a>
               </li>
               @endif
               @if($allowed ==1 || in_array(4,$modulesid))
                 <li>
                    <a href="{{url('/')}}/lounge">
                    <i class="fa fa-angle-right"></i>
                    &nbsp;Lounge Management
                    </a>
                 </li> 
               @endif
                 
              @if($allowed ==1 || in_array(3,$modulesid))
              <li>
                  <a href="{{url('/')}}/restaurantmenucategory">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title menu_hide">&nbsp;Restaurant Menu Category Management</span>
                  </a>
               </li> 
              @endif

              @if($allowed ==1 || in_array(3,$modulesid))
              <li>
                  <a href="{{url('/')}}/restaurantmenu">
                  <i class="fa fa-angle-right"></i>
                  <span class="link-title menu_hide">&nbsp;Restaurant Menu Management</span>
                  </a>
               </li> 
              @endif
            </ul>
         </li>
         @endif

      
        @if($allowed ==1 || in_array(12,$modulesid))
        <li>
            <a href="{{url('/')}}/foodtype">
            <i class="fa fa-snowflake-o"></i>
            <span class="link-title menu_hide">&nbsp;Food Type Management</span>
            </a>
         </li> 
        @endif
         @if($allowed ==1 || in_array(10,$modulesid))
        <li>
            <a href="{{url('/')}}/facility">
            <i class="fa fa-bookmark"></i>
            <span class="link-title menu_hide">&nbsp;Facility Management</span>
            </a>
         </li> 
        @endif

        @if($allowed ==1 || in_array(5,$modulesid))
        <li class="dropdown_menu">
            <a href="#">
           <i class="fa fa-gift"></i>
            <span class="link-title menu_hide">&nbsp; Offer</span>
            <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
              @if($allowed ==1)
               <li>
                   <a href="{{url('/')}}/offertype">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Offer Type
                  </a>
               </li>
               @endif
               <li>
                  <a href="{{url('/')}}/offer">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Offer
                  </a>
               </li>
            </ul>
         </li>
         @endif
         @if($allowed ==1)
         <li>
            <a href="#">
            <i class="fa fa-credit-card"></i>
            <span class="link-title menu_hide">&nbsp;Payment Reports</span>
            </a>
         </li>
         @endif
       
       
         @if($allowed ==1 || in_array(9,$modulesid))
         <li class="dropdown_menu">
            <a href="#">
            <i class="fa fa-newspaper-o"></i>
            <span class="link-title menu_hide">&nbsp; CMS</span>
            <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
               <li>
                   <a href="{{url('/')}}/advertisement">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Advertising
                  </a>
               </li>
               <li>
                  <a href="{{url('/')}}/news">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; News List
                  </a>
               </li>
               <li>
                 <a href="{{url('/cms')}}">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Content Management
                  </a>
               </li>
               <li>
                 <a href="{{url('/tips')}}">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Tips Management
                  </a>
               </li>
            </ul>
         </li>
          @endif
        @if($allowed ==1)
          <li class="dropdown_menu">
            <a href="#">
            <i class="fa fa-tasks"></i>
            <span class="link-title menu_hide">&nbsp; General</span>
            <span class="fa arrow menu_hide"></span>
            </a>
            <ul>
               <li>
                   <a href="{{url('/')}}/country">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Country
                  </a>
               </li>
               <li>
                  <a href="{{url('/')}}/state">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; State
                  </a>
               </li>
               <li>
                 <a href="{{url('/city')}}">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; City
                  </a>
               </li>   
                <li>
                 <a href="{{url('/currency')}}">
                  <i class="fa fa-angle-right"></i>
                  &nbsp; Currency
                  </a>
               </li>
            </ul>
         </li>
        @endif 
        
        
      </ul>
      <!-- /#menu -->
   </div>
</div>
<!-- end sidebar menu -->
@endif