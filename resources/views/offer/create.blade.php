@include('header')
@include('sidebar')

<!-- /#left -->
<!-- /#left -->
<style>
.error {
      color: red;
      
   }
   .fileinput.fileinput-new { margin-bottom: 0px;  }
.fileinput.fileinput-new .btn.btn-file {     overflow: visible;  width: 185px; position: relative; top:-15px; }
.fileinput.fileinput-new .m-t-20  { margin-top: 0px;   }
.fileinput.fileinput-new #profile-img-error {     position: absolute;
    top: 40px;
    left: 0; }
</style>
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                               @if($pid=="")
                               Add Offer
                               @else
                               Edit Offer
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="#">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="/offer">Offers</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                              @if($pid=="")
                                <form method="POST" action="{{url('offer/saveoffer')}}" id="offerforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                  @else
                                 <form method="POST" action="{{url('offer/saveoffer')}}" id="editofferforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                  @endif
                                          {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-4 input_field_sections">
                                            <h5>Promo Code </h5>
                                           <input type="text" class="form-control" name="promocode" id="promocode" value="{{ old('offer_code', isset($offer->offer_code) ? $offer->offer_code : null) }}"/>
                                       </div>
                                        <div class="col-sm-4 input_field_sections">
                                            <h5>Offer Type</h5>
                                          <select class="form-control" name="offertype" id="offertype" value="">
                                            <option value="">Select Offer Type</option>
                                            @foreach($offertype as $k=>$v)
                                            <option value="{{$k}}" {{ (isset($offer->offer_type_id) && $offer->offer_type_id==$k)  ? 'selected': '' }}>{{$v}}</option>
                                            @endforeach
                                      
                                          </select> 
                                         </div>
                                         <div class="col-sm-4 input_field_sections">
                                            <h5>Minimum Purchase Value</h5>
                                          <input type="text" class="form-control"  name="min_pur_value" id="min_pur_value" value="{{ old('min_purchase_value', isset($offer->min_purchase_value) ? $offer->min_purchase_value : null) }}" />
                                         </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-sm-4 input_field_sections">
                                            <h5>Discount(%)</h5>
                                           <input type="text" class="form-control" name="discount" id="discount" value="{{ old('discount_percentage', isset($offer->discount_percentage) ? $offer->discount_percentage : 0) }}"  />
                                       </div>
                                          <div class="col-sm-4 input_field_sections">
                                            <h5>Flat Price</h5>
                                           <input type="text" class="form-control"  name="flat_rate" id="flat_rate" value="{{ old('flat_rate', isset($offer->flat_rate) ? $offer->flat_rate : 0) }}" />  
                                         </div>
                                        <div class="col-sm-4 input_field_sections">
                                            <h5>Max Amount</h5>
                                           <input type="text" class="form-control"  name="max_amt" id="max_amt" value="{{ old('max_discount', isset($offer->max_discount) ? $offer->max_discount : null) }}" />  
                                         </div>
                                      </div>
                                       <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Start Date</h5>
                                             <input type="text" class="form-control startdate" name="startdate" id="startdate" value="{{old('offer_start_date', isset($offer->offer_start_date) ? date('m/d/Y',strtotime($offer->offer_start_date)) : null) }}"
                                               />
                                       </div>
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>End Date</h5>
                                             <input type="text" class="form-control enddate" name="enddate" id="enddate" value="{{old('offer_end_date', isset($offer->offer_end_date) ? date('m/d/Y',strtotime($offer->offer_end_date)) : null) }}" />
                                       </div>
                                      </div>
                                     
                                     <div class="row">
                                        <div class="col-sm-12 input_field_sections">
                                            <h5>Conditions</h5>
                                         
                                            <textarea class="form-control" rows="5" name="condition" id="condition" value="" >{{ (isset($offer->conditions)) ? $offer->conditions: '' }}</textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-sm-12">
                                       
                                      </div>
                                        <div class="col-sm-4 input_field_sections">
                                         
                                          <div class="row">
                                            <div class="col-sm-5 col-xs-12 text-center"> 
                                                <h5 style="">Notify to App</h5>
                                              <label class="custom-control custom-checkbox ">

                                            <input type="checkbox" class="custom-control-input"  name="notify" id="notify" value="1"{{ (isset($offer->notify_to_app) && $offer->notify_to_app==1)  ? 'checked': '' }}>
                                            <span class="custom-control-indicator"></span>
                                            </label>
                                          </div>
                                            <div class="col-sm-7   col-xs-12">
                                              <h5>Title</h5>
                                        <select class="form-control"  name="service_types" id="service_types" value="" >
                                        <option value="">Select Type</option>
                                              @foreach($service_types as $k=>$v)
                                            <option value="{{$k}}" {{ (isset($offer->service_types) && $offer->service_types==$k)  ? 'selected': '' }}>{{$v}}</option>
                                            @endforeach

                                      </select>
                                            </div>
                                          </div>
                                            
                                           
                                           

                                        </div>
                                        <div class="col-sm-4 input_field_sections">
                                            <h5>Title</h5>
                                         
                                            <input type="text" class="form-control" name="title" id="title" value="{{ old('title ', isset($offer->title) ? $offer->title : null) }}" />
                                        </div>
                                       <div class="col-sm-4 input_field_sections">
                                                     
                                                        <div class="col-12 col-lg-12 text-center text-lg-left">
                                                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                <div class="fileinput-new img-thumbnail text-center" style="padding-top: 20px; border:0px;">
                                                                        @if(isset($offer) && $offer->image!="" && $offer->image!=null)
                                                                        <img src="{{url('/')}}/uploads/offerimage/{{$offer->image}}" id="profile-img-tag" width="180px" height="140px"  name="profile_imaged" style="margin-top:15px;"/>
                                                                    @else
                                                                       <img src="{{url('/')}}/uploads/dummy/offer.png" id="profile-img-tag" width="180px" height="140px" name="profile_imaged" style="display:none;"/>
                                                                      @endif
                                                                  
                                                                </div>
                                                                <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                                                                <div class="m-t-20 text-center">
                                                            <span class="btn btn-primary btn-file">
                                                            <span>Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="file" name="profile_img" id="profile-img" class="fileinput-new" onchange="readURL(this);"  value="" accept="image/x-png,image/jpeg">
                                                            </span>
                                                                    <a href="#" class="btn btn-warning fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                    </div>
                                   
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   
                     </form>
                </div>

            </div>
        </div>
        <!-- /#content -->
    </div>

<!-- startsec End -->
  @include('footer')

<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"
   type="text/javascript"></script> -->
<!--   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
  <script type="text/javascript">
        $(function () {
            $("#startdate").datepicker({
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() + 1);
                    $("#txtTo").datepicker("option", "minDate", dt);
                }
            });
            $("#enddate").datepicker({
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var dt = new Date(selected);
                    dt.setDate(dt.getDate() - 1);
                    $("#txtFrom").datepicker("option", "maxDate", dt);
                }
            });
        });
    </script>
<script language="javascript">
   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                 $('#profile-img-tag').show();
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");

$.validator.addMethod("greaterThan",
    function (value, element, param) {
          var $otherElement = $(param);
          return parseInt(value, 10) > parseInt($otherElement.val(), 10);
    });





$('input, text').blur(function() {
        var value = $.trim( $(this).val() );
        $(this).val( value );
    });

$(function() {
  
   $( "#offerforms" ).validate({
       rules: {
               
        promocode: {
            required: true,
            minlength:3,
            maxlength:30
              },  
        offertype: {
            required: true,
          
              },  
      min_pur_value: {
            required: true,
            number:true,
           maxlength:10
              },   
      discount: {
            required: true,
            number:true,
             maxlength:5,
             max: 100

          
              },
     max_amt: {
            required: true,
            number:true,
          greaterThan: "#min_pur_value",
            maxlength:10
              },  
    startdate: {
            required: true,       
              },  
    enddate: {
            required: true,       
              },    
     condition: {
            required: true,
            maxlength:15000       
              },
    service_types: {
            required: true,       
              }, 
       title: {
            required: true,    
               maxlength:30   
              }, 
   profile_img: {
                required: true,
                          accept: "image/*"
              }, 
    flat_rate: {
                required: true,
                 number:true,
             maxlength:5,
            
                        
              }
       
        },
       messages: {
             
        promocode: {
            required: "Please Enter Promocode"
                 }, 
      offertype: {
            required: "Please Select Offertype"
                 },
      min_pur_value: {
            required: "Please Enter Minimum Purchase Value",
                 },
      discount: {
            required: "Please Enter Discount Percentage",
                 },   
      max_amt: {
            required: "Please Enter Maximum Amount",
            greaterThan:"Must be greater than Minimum Purchase Value"
                 },    
      startdate: {
            required: "Please Select Start Date",
                 },    
      enddate: {
            required: "Please Select End Date",
                 }, 
        condition: {
            required: "Please Enter Condition",
                 },   
     service_types: {
            required: "Please Select Type",
                 },  
       title: {
            required: "Please Enter Title",
                 },
       profile_img: {
                            required: "Please upload file.",
                            accept: "Please upload file in these format only (jpg, jpeg, png)."
                        } ,
         flat_rate: {
            required: "Please Enter Flat Price",
                 },    
       
                   },
      submitHandler: function(form){ 
        $('form input[type=submit]').prop('disabled', true);
        form.submit();
      },
   });  
});
$(function() {
  
   $( "#editofferforms" ).validate({
       rules: {
               
        promocode: {
            required: true,
            minlength:3,
            maxlength:30
              },  
        offertype: {
            required: true,
          
              },  
      min_pur_value: {
            required: true,
            number:true,
           maxlength:10
              },   
      discount: {
            required: true,
            number:true,
             maxlength:5,
             max: 100

          
              },
     max_amt: {
            required: true,
            number:true,
          greaterThan: "#min_pur_value",
            maxlength:10
              },  
    startdate: {
            required: true,       
              },  
    enddate: {
            required: true,       
              },    
     condition: {
            required: true,
            maxlength:15000       
              },
    service_types: {
            required: true,       
              }, 
       title: {
            required: true,    
               maxlength:30   
              }, 
   profile_img: {
               
                          accept: "image/*"
              },
   flat_rate: {
                required: true,
                 number:true,
             maxlength:5,
            
                        
              }
       
        },
       messages: {
             
        promocode: {
            required: "Please Enter Promocode"
                 }, 
      offertype: {
            required: "Please Select Offertype"
                 },
      min_pur_value: {
            required: "Please Enter Minimum Purchase Value",
                 },
      discount: {
            required: "Please Enter Discount Percentage",
                 },   
      max_amt: {
            required: "Please Enter Maximum Amount",
            greaterThan:"Must be greater than Minimum Purchase Value"
                 },    
      startdate: {
            required: "Please Select Start Date",
                 },    
      enddate: {
            required: "Please Select End Date",
                 }, 
        condition: {
            required: "Please Enter Condition",
                 },   
     service_types: {
            required: "Please Select Type",
                 },  
       title: {
            required: "Please Enter Title",
                 },
       profile_img: {
                           
                            accept: "Please upload file in these format only (jpg, jpeg, png)."
                        },
        flat_rate: {
            required: "Please Enter Flat Price",
                 },    
            
       
                   },
      submitHandler: function(form){ 
        $('form input[type=submit]').prop('disabled', true);
        form.submit();
      },
   });  
});


$("#cancelform").click(function() {
   window.location.href = "{{url('/offer')}}";
});




</script>
