@include('header')
@include('sidebar')

<style>

@media (max-width 480px)

{

.col-sm-3.input_field_sections {

    margin-top: 0 !important;
}

}


</style>
<!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-eye"></i>
                               View Offer
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/offer')}}">Offers</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                    <div class="row">
                        <div class="col-sm-12" style="margin-bottom: 15px;">
                          <a href="{{url('/')}}/offer/edit/{{$id}}" style="background-color: #ed7626;padding: 5px 20px;border-radius: 20px !IMPORTANT;display: inline-block;border: 1px solid #ed7626;color: #fff;" type="button" class="btn float-right">Edit</a>
                        </div>
                        <div class="col">
                            <div class="card">
                                
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 input_field_sections">
                                            <h5>Promo Code </h5>
                                           <strong>{{$offer->offer_code}}</strong>
                                             
                                       </div>
                                        <div class="col-sm-3 input_field_sections">
                                            <h5>Offer Type</h5>
                                            @if($offer->offer_type->name!="")
                                         <strong>{{$offer->offer_type->name}}</strong>
                                         @else
                                         <strong>Not Defined</strong>
                                         @endif
                                         </div>
                                         <div class="col-sm-3 input_field_sections">
                                            <h5>Discount(%)</h5>
                                              <strong>{{$offer->discount_percentage}}%</strong>
                                           </div>
                                             <div class="col-sm-3 input_field_sections">
                                            <h5>Flat Price</h5>
                                         <strong>{{$offer->flat_rate}} Rs</strong>
                                         </div>

                                      </div>

                                        <div class="row">
                                           <div class="col-sm-3 input_field_sections">
                                            <h5>Minimum Purchase Value</h5>
                                         <strong>{{$offer->min_purchase_value}}</strong>
                                         </div>
                                                <div class="col-sm-3 input_field_sections">
                                            <h5>Max Amount</h5>
                                               <strong>{{$offer->max_discount}}</strong>
                                           </div>
                                            <div class="col-sm-3 input_field_sections">
                                            <h5>Start Date</h5>
                                              <strong>{{$offer->offer_start_date}}</strong>
                                           </div>
                                         <div class="col-sm-3 input_field_sections">
                                            <h5>End Date</h5>
                                               <strong>{{$offer->offer_end_date}}</strong>
                                           </div>
                                              <div class="col-sm-3 input_field_sections">
                                            <h5>Image</h5>
                                            @if($offer->image!="")
                                               <strong> <img src="{{url('/')}}/uploads/offerimage/{{$offer->image}}" id="profile-img-tag" width="180px" height="140px"  name="profile_imaged" style="margin-top:15px;"/>  </strong>
                                              @else
                                              <strong>  <img src="{{url('/')}}/uploads/dummy/offer.png" id="profile-img-tag" width="180px" height="140px" name="profile_imaged" /></strong>
                                              @endif
                                           </div>
                                    </div>
 
                                    <div class="row" id="last_div" >
                                        
                                         <div class="col-sm-3 input_field_sections" style="margin-top: -8%;">
                                            <h5>Conditions</h5>
                                               <strong>{{$offer->conditions}}  </strong>
                                           </div> 
                                             <div class="col-sm-3 input_field_sections" style="margin-top: -8%;">
                                            <h5>title</h5>
                                               <strong>{{$offer->title}}  </strong>
                                           </div> 
                                              <div class="col-sm-3 input_field_sections" style="margin-top: -8%;">
                                            <h5>Notify To Me</h5>
                                               <strong>{{isset($offer->notify_to_app)?'Yes':'No'}}  </strong>
                                           </div>
                                        
                                    </div>
                                   
                                   
                                </div>
                            </div>
                        </div>
                    </div>

                  
                   
                   
                </div>
                <!-- /.outer -->
                <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
                     aria-hidden="true">
                    <form>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span class="float-right" aria-hidden="true">&times;</span>
                                </button>
                                <div class="input-group search_bar_small">
                                    <input type="text" class="form-control" placeholder="Search..." name="search">
                                    <span class="input-group-btn">
        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
      </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- /#content -->
    </div>

<!-- startsec End -->
@include('footer')