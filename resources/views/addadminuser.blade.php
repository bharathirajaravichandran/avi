@include('header')
<!-- /#left -->
<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                      @if($id==0)
                     Add Admin User
                     @else
                     Update Admin User
                     @endif
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                         <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/admin"> Admin Users</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <form action="{{url('admin/saveadmin')}}" method="post" id="myforms" name="myform">
         <div class="inner bg-container forms">
            <div class="row">
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Username</h5>
                               <input type="text" class="form-control" name="first_name" id="first_name" value="{{old('first_name', isset($adminuserdetail->first_name) ? $adminuserdetail->first_name : null) }}" minlength="1" maxlength="30" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Email</h5>
                              @if($id==0)
                               <input type="text" class="form-control" name="email" id="email" value="{{old('email', isset($adminuserdetail->email) ? $adminuserdetail->email : null) }}"  maxlength="40" />
                               @else
                               <input type="text" class="form-control" name="email" id="email" value="{{$adminuserdetail->email}}" disabled maxlength="40" />
                               @endif
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Phone</h5>
                              <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone', isset($adminuserdetail->phone) ? $adminuserdetail->phone : null) }}" minlength="1" maxlength="16" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>DOB</h5>
                              <input type="text" class="form-control dob" id="userdob" name="dob" value="{{old('dob', isset($adminuserdetail->dob) ? date('m/d/Y',strtotime($adminuserdetail->dob)) : null) }}" readonly />  
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Status</h5>
                              <select class="form-control" id="status" name="status">
                                 <option  value="">Please Select Status</option>
                                 <option value="1" {{ old('status', isset($adminuserdetail->status) ? $adminuserdetail->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                  <option value="0" {{ old('status', isset($adminuserdetail->status) ? $adminuserdetail->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                              </select> 
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.row -->
            <div class=" m-t-35">
               <div class="form-actions form-group row">
                  <div class="col-xl-12 text-center">
                     <input type="hidden" name="id" id="pid" value="{{$id}}">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                     <input type="submit" class="btn btn-primary" value="Submit">
                     <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                  </div>
               </div>
            </div>
         </div>
         <!-- /.outer -->
         <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
            aria-hidden="true">
            <form>
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span class="float-right" aria-hidden="true">&times;</span>
                     </button>
                     <div class="input-group search_bar_small">
                        <input type="text" class="form-control" placeholder="Search..." name="search">
                        <span class="input-group-btn">
                        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>
@include('footer')
<style type="text/css">
   body
   {
   font-family: Arial, Sans-serif;
   }
   .error
   {
   color:red;
   font-family:verdana, Helvetica;
   }
</style>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){

   $.validator.addMethod("lettersonly", function(value, element) {
     return this.optional(element) || /^[a-zA-Z\s ]+$/i.test(value);
   }, "Alpha Numeric characters only allowed."); 

   $.validator.addMethod("phonenumber", function(value, element) {
      return this.optional(element) || /^[0-9-_]+$/i.test(value);
   }, "VAlid Phone number only allowed."); 

   $.validator.addMethod("nowhitespace", function(value, element) {
    return this.optional(element) || value.trim()!="";
   }, "No white space please");
 
});
</script>
<script type="text/javascript">
$(function()
{
  $("#myforms").validate(
  {
      onfocusout: false,
      invalidHandler: function(form, validator) {
      var errors = validator.numberOfInvalids();
      if (errors) {
           validator.errorList[0].element.focus();
      }
    },
    rules:{
      first_name: {
        required:true,
        lettersonly:true,
        nowhitespace:true,
      },
      phone: {
        required:true,
        phonenumber:true,
           minlength: 8,
         maxlength:16
      },
      email: {
        required:true,
        email:true,
        remote:{
            url : "{{ url('/') }}/adminuserexist",
            type: 'GET',
            data :{ 
             id: $('#pid').val(),
             email: this.value,
            },
            complete: function(data) {
                   //console.log(data);
            }
         }
      },
      dob: {
        required:true,
        date:true,
      },
      status: {
        required:true,
      },
    },
    messages:{
      first_name:{
        required:"User Name Field is required",
        lettersonly:"User Name Field is invalid",
        nowhitespace:"User Name Field is invalid",
      },
      phone:{
        required:"Phone Number Field is required",
        phonenumber:"Phone Number Field is invalid",
      
      },
      email:{
        required:"Email Field is required",
        email:"Email Field is invalid",
        remote:"This email is already exist",

      },
      dob:{
        required:"Dob Field is required",
        date:"Dob Field is invalid",
      }, 
      status:{
        required:"Status Field is required",
      },        
    },

    submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    },
    
  });   

});

$("#cancelform").click(function() {
   window.location.href = "{{url('/admin')}}";
}); 

var dt = new Date();
dt.setFullYear(new Date().getFullYear()-18);

$('#userdob').datepicker({
  viewMode: "years",
   endDate : dt,
});

</script>