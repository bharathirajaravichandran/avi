@include('header')
@include('sidebar')

<!-- /#left -->
<style>
.error {
      color: red;
      
   }
</style>
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                               @if($pid=="")
                               Add State
                               @else
                               Edit State
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/state')}}">State</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                  <form method="POST" action="{{url('state/savestate')}}" id="countryforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                
                                          {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                       <div class="col-sm-6 input_field_sections">
                                        <h5>Country</h5>
                                      
                                            <select name="country_id" id="country_id" class="form-control select2 input-height">
                                                <option value="">Select Country</option>
                                                @foreach($country as $k=>$val)
                                                <option value="{{$k}}" {{ (isset($states->country_id) && $states->country_id==$k)  ? 'selected': '' }} >{{$val}}</option>
                                                @endforeach
                                            </select>
                                    </div>
                                       <div class="col-sm-6 input_field_sections">
                                            <h5>State Name</h5>
                                           <input type="text" class="form-control" name="name" id="name" value="{{ ( isset($states->name) ? $states->name : null) }}" placeholder="Enter State Name here.." minlength="1" maxlength="30"/>  
                                              {!! $errors->first('Country name', '<p class="help-block">:message</p>') !!}
                                         </div>
                                        <div class="col-sm-6 input_field_sections">
                                          <h5>Status</h5>
                                             <select class="form-control status" style="width: 100%;" name="status" id="status"  >
                                             <option value="1">Active</option>
                                             <option value="0" {{ (isset($states->status) && $states->status==0)  ? 'selected': '' }}>InActive</option>
                                             </select>
                                        </div>
                                     </div>                            
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   </form>
                   
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>
   
     <style type="text/css">
     body
     {
     font-family: Arial, Sans-serif;
     }
     .error
     {
     color:red;
     font-family:verdana, Helvetica;
     }
     #terminal-error {
        position: absolute;
        bottom: -11px;
     }
    </style>

   @include('footer')


<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"
   type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script language="javascript">

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");
$('input, text').blur(function() {
        var value = $.trim( $(this).val() );
        $(this).val( value );
    });


$(function() {
  
   $( "#countryforms" ).validate({
       rules: {
                
        name: {
            required: true,
            lettersonly: true,
            minlength:3,
            maxlength:35
              }, 
               country_id: {
      required: true,
     },
    
        },
     messages: {
        name: {
            required: "Please enter State Name"
                 }, 
        country_id:{
          required:"Please Select Country Name"
        }    
        
      },

      submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    }, 
                   
   });  
});

$("#cancelform").click(function() {
   window.location.href = "{{url('/state')}}";
});
</script>