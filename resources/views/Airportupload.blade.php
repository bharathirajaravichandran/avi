@include('header')
@include('sidebar')
<style type="text/css">
    .error
     {
     color:red;
     font-family:verdana, Helvetica;
     }
</style>
<!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                                
                              Upload Airport
                               
                           </h4>
                       </div>
                        
<div class="col-md-12 row" style="margin-top:15px;">
         <div class="form-group col-md-5 offset-md-1 pull-left">
             <label>Download Airport's Template</label>
             <div>  <a href="{{ url('download', 'airport.csv') }}" class="btn btn-success">Download Airport template</a></div>
                </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="#">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/airport')}}">Airport</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                     <form method="POST" id="uploadform" action="{{ url('/airport/uploadcsv') }}"  class="col-md-12" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                          {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="col-md-12">
                                       <div class="row input_field_sections">
                                            <h5 class="col-sm-4">Airport</h5>
                                        <div class="col-sm-6">
                                          <input  type="file" name="csvfile" style="color: #777;box-shadow: none;outline: none;" id="ProfileImage" />
                                        </div>
                                       </div>
                                                                    
                                    </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class="col-xl-12 m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->
 @include('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>
<script language="javascript">

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");

$(function() {
  
   $( "#uploadform" ).validate({
       rules: {
                csvfile: {
            required: true,
             accept: "csv"
              },
         
        },
       messages: {
                csvfile: {
            required: " Please Upload Valid File",
             accept: "Please upload file in these format only (csvfile)."
                 },        
      },

      submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    },
    
   });  
});
$("#cancelform").click(function() {
   window.location.href = "{{url('/airport')}}";
});

</script>