@include('header')
@include('sidebar')
<!-- /#left -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>

<style type="text/css">
.fileinput-remove {display:none;}
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash:before {  position: absolute;  content: '\f1f8' !important;
    top: -14px;   left: -6px;   font-size: 18px;   font-family: fontawesome;    font-style: normal; color:#a94442; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in   {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in:before {  position: absolute;  content: '\f00e' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: #444; }
.file-upload-indicator .glyphicon-exclamation-sign   {  position: relative; }   
.file-upload-indicator .glyphicon-exclamation-sign:before {  position: absolute;  content: '\f06a' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: red; }
.file-thumbnail-footer .file-footer-buttons .kv-file-remove:hover { background: #f4f4f4;  }
.file-thumbnail-footer .file-footer-buttons .kv-file-zoom:hover { background: #f4f4f4;  }
.error{color: red;}
.Err, .Err1,.Err2 { display:none; color:red!important; }
</style>
<div id="content" class="bg-container">
   <header class="head">
      <div class="main-bar">
         <div class="row no-gutters">
            <div class="col-sm-5 col-lg-6 skin_txt">
               <h4 class="nav_top_align">
                  <i class="fa fa-plus"></i>
                   @if($pid=="")
                     Add Shop
                     @else
                     Update Shop
                     @endif
               </h4>
            </div>
            <div class="col-sm-7 col-lg-6">
               <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                  <li class="breadcrumb-item">
                     <a href="{{url('/')}}">
                     <i class="fa fa-home" data-pack="default" data-tags=""></i>
                     Dashboard
                     </a>
                  </li>
                  <li class="breadcrumb-item">
                     <a href="/retailshop">Shop</a>
                  </li>
               </ol>
            </div>
         </div>
      </div>
   </header>
   <div class="outer">
      <div class="inner bg-container forms">
         <div class="row">
            <div class="col">
               <div class="card">
                  <form action="{{url('retailshop/saveretail')}}" method="post" id="rest_form" enctype="multipart/form-data">
                     <input type="hidden" name="_token" value="{{csrf_token()}}"> 
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="name_inner">
                              <h5>Name</h5>
                              <input type="text" class="form-control" name="name" id="name" value="{{ old('name', isset($rest->name) ? $rest->name : null) }}" minlength=2 maxlength="50"/>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Shop Name field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="phone_inner">
                              <h5>Phone No</h5>
                              <input type="text" class="form-control" name="phone" id="phone"  minlength="8" maxlength="16" value="{{ old('phone', isset($rest->phone) ? $rest->phone : null) }}"  /> 
                                 <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The PhoneNo field is required.</span> 
                                 <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> PhoneNumber accepts only number</span> 
                                 <span class="control-label Err2" for="inputError"><i class="fa fa-times-circle-o"></i> PhoneNumber accepts only minimum 8 and maximum 16 number only</span> 
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="email_inner">
                              <h5>Email</h5>
                              <input type="text" class="form-control" name="email" id="email" value="{{ old('email', isset($rest->email) ? $rest->email : null) }}"  maxlength="30"/>
                                  <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is required.</span>
                              <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is invalid.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="airportid_inner">
                              <h5>Airport</h5>
                              <select class="form-control" id="airports"
                                 name="airports"  onchange="terminal_name(this.value);">
                                 <option value="">Select The Airport</option>
                                 @foreach($airports as $k=>$v)
                                 <option value="{{$k}}"{{ (isset($rest->airport_id) && $rest->airport_id==$k)  ? 'selected': '' }}>{{$v}}</option>
                                 @endforeach
                              </select>
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Airport field is required.</span>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="shop_inner">
                              <h5>Shop No</h5>
                              <input type="text" class="form-control"  name="shop_no" id="shop_no" value="{{ old('shop_no', isset($rest->shop_no) ? $rest->shop_no : null) }}" maxlength="10" />  
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Shopno field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Terminal</h5>
                              <select class="form-control" id="terminal" name="terminal" value=""> 
                              @if($pid!="")
                              @foreach($airportterminals as $k=>$v)
                              <option value="{{$k}}"{{ (isset($rest->airport_terminal_id) && $rest->airport_terminal_id==$k)  ? 'selected': '' }}>{{$v}}</option>
                              @endforeach
                              @endif
                              </select>  
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="address_inner">
                              <h5>Address</h5>
                              <input type="text" class="form-control" name="address" id="address" value="{{ old('address', isset($rest->address) ? $rest->address : null) }}"  maxlength="100"/> 
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Address field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="type_inner">
                              <h5>Type</h5>
                              <select class="form-control" name="type" id="type" value="" >
                                 <option value="">Select Type</option>
                                 @foreach($retail_types as $k=>$v)
                                 <option value="{{$k}}"  {{ (isset($rest->retail_type_id) && $rest->retail_type_id==$k)  ? 'selected': '' }}>{{$v}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Type field is required.</span>
                           </div>
                        </div>
                        <div class="row">
                    
                           <div class="col-sm-6 input_field_sections" id="landmark_inner">
                              <h5>Landmark</h5>
                              <input type="text" class="form-control" name="landmark" id="landmark" value="{{ old('landmark', isset($rest->landmark) ? $rest->landmark : null) }}" maxlength="30" /> 
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Landmark field is required.</span>
                           </div>
                             <div class="col-sm-6 input_field_sections" id="status_inner">
                              <h5>Status</h5>
                              <select class="form-control" id="status" name="status">
                                 <option value="1" {{ old('status', isset($rest->status) ? $rest->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                  <option value="0" {{ old('status', isset($rest->status) ? $rest->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                              </select> 
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Status field is required.</span>
                           </div>
                        </div>
                       
                        <div class="row">
                              <div class="col-sm-12 input_field_sections" >
                                <h5>Working Days</h5>
                              </div>  
                              
                        </div>      
                         <div class="row">
                              <div class="col-sm-2" >
                               
                              </div>  
                              <div class="col-sm-3" >
                                <label>From Time</label>
                              </div>  
                              <div class="col-sm-3" >
                                <label>To Time</label>
                              </div>  
                        </div>  
                      
                          <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Sunday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                              
                                  <div class='input-group date' >

                                    <input type="text" id="sun_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="sun_from" value="{{  isset($array['sunday']['from']) ? $array['sunday']['from'] : null }}" onkeydown="return false;">
                                   
                              </div>
                               <div id="sun_from_inner">
                                        <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                        <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                  </div>
                            </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date' id="" >
                                    <input type="text" id="sun_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="sun_to" value="{{  isset($array['sunday']['to']) ? $array['sunday']['to'] : null }}" onkeydown="return false;">
                                      
                                 </div>
                                 <div id="sun_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>   
                          <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Monday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="mon_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="mon_from" value="{{  isset($array['monday']['from']) ? $array['monday']['from'] : null }}" onkeydown="return false;">
                                       
                                  </div>
                                  <div id="mon_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="mon_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="mon_to" value="{{  isset($array['monday']['to']) ? $array['monday']['to'] : null }}" onkeydown="return false;">
                                     
                                 </div>
                                  <div id="mon_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>
                         <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Tuesday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="tue_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="tue_from" value="{{  isset($array['tuesday']['from']) ? $array['tuesday']['from'] : null }}" onkeydown="return false;">
                                   
                                  </div>
                                    <div id="tue_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="tue_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="tue_to" value="{{  isset($array['tuesday']['to']) ? $array['tuesday']['to'] : null }}" onkeydown="return false;">
                                   
                                 </div>
                                  <div id="tue_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>   
                          <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Wednesday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="wed_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="wed_from" value="{{  isset($array['Wednesday']['from']) ? $array['Wednesday']['from'] : null }}" onkeydown="return false;">
                                    
                                  </div>
                                  <div id="wed_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="wed_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="wed_to" value="{{  isset($array['Wednesday']['to']) ? $array['Wednesday']['to'] : null }}" onkeydown="return false;">
                                  
                                 </div>
                                   <div id="wed_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>    
                          <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Thursday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="thu_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="thu_from" value="{{  isset($array['Thursday']['from']) ? $array['Thursday']['from'] : null }}" onkeydown="return false;">
                                      
                                  </div>
                                    <div id="thu_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="thu_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="thu_to" value="{{  isset($array['Thursday']['to']) ? $array['Thursday']['to'] : null }}" onkeydown="return false;">
                                    
                                 </div>
                                 <div id="thu_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>    
                          <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Friday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="fr_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="fr_from" value="{{  isset($array['Friday']['from']) ? $array['Friday']['from'] : null }}" onkeydown="return false;">
                                      
                                  </div>
                                  <div id="fr_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="fr_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="fr_to" value="{{  isset($array['Friday']['to']) ? $array['Friday']['to'] : null }}" onkeydown="return false;">
                                 </div>
                                 <div id="fr_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>   
                           <div class="row" style="margin-bottom: 15px;">
                              <div class="col-sm-2" >
                                 <label><span style="position: relative;bottom: 4px;">Saturday</span></label>
                              </div>   
                              <div class="col-sm-3" >
                                  <div class='input-group date'>
                                    <input type="text" id="sat_from" class="form-control floating-label" placeholder="From Time" data-dtp="dtp_gB3tn" class="time" name="sat_from" value="{{  isset($array['saturday']['from']) ? $array['saturday']['from'] : null }}" onkeydown="return false;">
                                  </div>
                                    <div id="sat_from_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>From Time is required.</span>
                                           <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i>Enter Valid Time.</span>
                                      </div>
                              </div>
                              <div class="col-sm-3" >    
                                  <div class='input-group date'  >
                                    <input type="text" id="sat_to" class="form-control floating-label" placeholder="To Time" data-dtp="dtp_gB3tn" class="time" name="sat_to" value="{{  isset($array['saturday']['to']) ? $array['saturday']['to'] : null }}" onkeydown="return false;">
                                   
                                 </div>
                                  <div id="sat_to_inner">
                                          <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>To Time is required.</span>
                                      </div>
                              </div>
                          </div>
                       
                        <div id="testing" style="display:none;">  </div>
                    
                     </div>
               </div>
            </div>
         </div>
         <!-- /.row -->
         <div class=" m-t-35">
         <div class="form-actions form-group row">
         <div class="col-xl-12 text-center">
         <input type="hidden" name="pid" value="{{$pid}}">
         <input type="button" class="btn btn-primary" value="Submit" onclick="return validation();">
           <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
         </div>
         </div>
         </div>
         </form>
      </div>
      <!-- /.outer -->
   </div>
</div>
<!-- /#content -->
</div>
</div>


<!-- startsec End -->
@include('footer')
  <script type="text/javascript" src="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="https://jonthornton.github.io/jquery-timepicker/jquery.timepicker.css" />

  <script type="text/javascript" src="https://jonthornton.github.io/jquery-timepicker/lib/bootstrap-datepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="https://jonthornton.github.io/jquery-timepicker/lib/bootstrap-datepicker.css" />



  <script>
  $('#sun_from').timepicker
      ({
       'step':'15',
        'scrollDefault': 'now',
        'timeFormat': 'H:i a'
       
      });
     $('#sun_to').timepicker
      ({
       'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
      $('#mon_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#mon_to').timepicker
      ({
       'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });    
      $('#tue_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#tue_to').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });    
      $('#wed_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#wed_to').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
    $('#thu_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#thu_to').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
    $('#fr_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#fr_to').timepicker
      ({
       'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });  
       $('#sat_from').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });
     $('#sat_to').timepicker
      ({
        'step':'15',
        'scrollDefault': 'now',
         'timeFormat': 'H:i a'
      });


         // $('#time').bootstrapMaterialDatePicker({ date: false });
         $('#time').timepicker({ 'scrollDefault': 'now' });





   $("#cancelform").click(function() {
       window.location.href = "{{url('/retailshop')}}";
   }); 

   </script>

  <script type="text/javascript">
   
   $(function() { 
   $("#working").select2({"allowClear":false});     
   $("#airports").select2({"allowClear":false});    
   });
   function terminal_name(vals)
   {
       if(vals=="")
       {
         $('#terminal').empty();
       }
      if(vals!="")
      {
            $('#terminal').html('');
      $("#terminal").select2({"allowClear":true,"placeholder":"Please select Terminal"});
      url = "{{ url('/') }}/getterminal?q="+vals;
      $.ajax({
          type : "GET",
          url : url,
          beforeSend : function() {
   
          },
          success : function(data) {
              $('#terminal').html(data);
              $("#terminal").select2({"allowClear":false});            
          },
          error : function(xhr, ajaxOptions, thrownError) {
          },
      });
    }
   }
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script language="javascript">
   
   $("#cancelform").click(function() {
   window.location.href = "{{url('/retailshop')}}";
});
function validation()
{
   $(':input[type="button"]').prop('disabled', true);
    var res_name = $("#name").val();
    var phone = $("#phone").val();
    var email = $("#email").val();
    var airport = $("#airports").val();
    var shop_no = $("#shop_no").val();
    var address = $("#address").val();
    var type = $("#type").val();

    var landmark = $("#landmark").val();

       var sun_from = $("#sun_from").val();
       var sun_to = $("#sun_to").val();   
       var mon_from = $("#mon_from").val();
       var mon_to = $("#mon_to").val(); 
       var tue_from = $("#tue_from").val();
       var tue_to = $("#tue_to").val(); 
         var wed_from = $("#wed_from").val();
       var wed_to = $("#wed_to").val();
       var thu_from = $("#thu_from").val();
       var thu_to = $("#thu_to").val(); 
       var fr_from = $("#fr_from").val();
       var fr_to = $("#fr_to").val(); 
       var sat_from = $("#sat_from").val();
       var sat_to = $("#sat_to").val();

    var phoneregex = /^[0-9\+]{1,}[0-9\-]{3,15}$/;
    var alphanumeric =/^[a-z0-9 ]+$/i;
       var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; 
    var err="";
    $('.Err').hide();
   $('.Err1').hide();
   $('.Err2').hide();
    if((res_name=='' || res_name==null))
   {
     $('#name').addClass('has-error');
     $('#name_inner .Err').show();
     if(err=='')
     {
         $('#name').focus();
         err='set';
     }
   } 


  
   if(phone!="")
   {
     if(isNaN(phone))
     {
        $('#phone').addClass('has-error');
        $('#phone_inner .Err1').show();
        if(err=='')
        {
            $('#phone').focus();
            err='set';
        }
      }
      if(!isNaN(phone))
      {
        if(phone.length>16 || phone.length<8)
        {
              $('#phone').addClass('has-error');
            $('#phone_inner .Err2').show();
            if(err=='')
            {
                $('#phone').focus();
                err='set';
            }
        }
      }
   } 
 

    if(email!="" && !emailregex.test(email))
    {
        $('#email').addClass('has-error');
        $('#email_inner .Err1').show();
        if(err=='')
        {
            $('#email').focus();
            err='set';
        }
    }
     if(airport=='' || airport==null)
   {
       
        $('#airportid').addClass('has-error');
        $('#airportid_inner .Err').show();
        if(err=='')
        {
            $('#airportid').focus();
            err='set';
        }
   }  
    if(shop_no=='' || shop_no==null)
   {
       
        $('#shop_no').addClass('has-error');
        $('#shop_inner .Err').show();
        if(err=='')
        {
            $('#shop_no').focus();
            err='set';
        }
   }   
   if(address=='' || address==null)
   {
        $('#address').addClass('has-error');
        $('#address_inner .Err').show();
        if(err=='')
        {
            $('#address').focus();
            err='set';
        }
   }   
   if(type=='' || type==null)
   {
        $('#type').addClass('has-error');
        $('#type_inner .Err').show();
        if(err=='')
        {
            $('#type').focus();
            err='set';
        }
   }    
  
   if(landmark=='' || landmark==null)
   {
        $('#landmark').addClass('has-error');
        $('#landmark_inner .Err').show();
        if(err=='')
        {
            $('#landmark').focus();
            err='set';
        }
   } 
     
   if(sun_from!="" && sun_to!="" )
   {
       if(sun_from>sun_to)
       {
         $('#sun_from').addClass('has-error');
          $('#sun_from_inner .Err1').show();
          if(err=='')
          {
              $('#sun_from').focus();
              err='set';
          }
       }  
   } 
    if(mon_from!="" && mon_to!="")
   {
       if(mon_from>mon_to)
       {
         $('#mon_from').addClass('has-error');
          $('#mon_from_inner .Err1').show();
          if(err=='')
          {
              $('#mon_from').focus();
              err='set';
          }
       }  
   }   
    if(tue_from!="" && tue_to!="")
   {
       if(tue_from>tue_to)
       {
         $('#tue_from').addClass('has-error');
          $('#tue_from_inner .Err1').show();
          if(err=='')
          {
              $('#tue_from').focus();
              err='set';
          }
       }  
   }
 
    if(wed_from!="" && wed_to!="")
   {
       if(wed_from>wed_to)
       {
         $('#wed_from').addClass('has-error');
          $('#wed_from_inner .Err1').show();
          if(err=='')
          {
              $('#wed_from').focus();
              err='set';
          }
       }  
   }  
      if(thu_from!="" && thu_to!="")
   {
       if(thu_from>thu_to)
       {
         $('#thu_from').addClass('has-error');
          $('#thu_from_inner .Err1').show();
          if(err=='')
          {
              $('#thu_from').focus();
              err='set';
          }
       }  
   }  
   if(fr_from!="" && fr_to!="")
   {
       if(fr_from>fr_to)
       {
         $('#fr_from').addClass('has-error');
          $('#fr_from_inner .Err1').show();
          if(err=='')
          {
              $('#fr_from').focus();
              err='set';
          }
       }  
   }   
   if(sat_from!="" && sat_to!="")
   {
       if(sat_from>sat_to)
       {
         $('#sat_from').addClass('has-error');
          $('#sat_from_inner .Err1').show();
          if(err=='')
          {
              $('#sat_from').focus();
              err='set';
          }
       }  
   }

   
    
   
   if(err!='')
   {
   
 
     $(':input[type="button"]').prop('disabled', false);
     return false;
   }
   else
   {   
    
      $('#rest_form').submit();
   }

   
}
   
</script>