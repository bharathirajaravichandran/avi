@include('header')
@include('sidebar')


<!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-eye"></i>
                               View Shop
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="/retailshop">Shop</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                    <div class="row">
                      <div class="col-sm-12" style="margin-bottom: 15px;">
                        <a href="{{url('/')}}/retailshop/edit/{{$id}}" style="background-color: #ed7626;padding: 5px 20px;border-radius: 20px !IMPORTANT;display: inline-block;border: 1px solid #ed7626;color: #fff;" type="button" class="btn float-right">Edit</a>
                      </div>
                        <div class="col">
                            <div class="card">                                
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Name</h5>
                                           <strong>{{$rest->name}}</strong>
                                             
                                       </div>
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Phone No</h5>
                                         <strong>{{$rest->phone}}</strong>
                                         </div>
                                      </div>
                                        <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Email</h5>
                                              <strong>{{$rest->email}}</strong>
                                           </div>
                                         <div class="col-sm-6 input_field_sections">
                                            <h5>Airport</h5>
                                             @if($rest->airports->name!="")
                                               <strong>{{$rest->airports->name}}</strong>
                                            @else
                                               <strong></strong>
                                             @endif  
                                           </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Shop No</h5>
                                              <strong>{{$rest->shop_no}}</strong>
                                           </div>
                                         <div class="col-sm-6 input_field_sections">
                                            <h5>Terminal</h5>
                                            @if($rest->terminal->name!="")
                                               <strong>{{$rest->terminal->name}}</strong>
                                            @else
                                               <strong></strong>
                                             @endif  
                                           </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Type</h5>
                                              @if($rest->retailtype->name!="")
                                               <strong>{{$rest->retailtype->name}}</strong>
                                            @else
                                               <strong></strong>
                                             @endif 
                                           </div>
                                            <h5>Landmark</h5>
                                              <strong>{{$rest->landmark}}</strong>
                                           </div>
                                   
                                    
                                     <div class="row">
                                       
                                           
                                            <div class="col-sm-6 input_field_sections">
                                            <h5>Address</h5>
                                              <strong>{{$rest->address}}</strong>
                                           </div>
                                            <div class="col-sm-6 input_field_sections">
                                            <h5>Status</h5>
                                              <strong>{{$rest->status==1?'Active':'Inactive'}}</strong>
                                           </div>
                                         
                                    </div>   

                                    <div class="row">
                                        <div class="col-sm-12 input_field_sections">
                                            <h5>Working Days</h5>
											
										</div>
										</div>
										
									<table border="1" cellpadding="7" cellspacing="0" width="600px"  style="border-collapse: collapse; border:1px solid #ddd !Important;text-align:center; "  >		
									<tr>
									   <td></td>
									   <td><label>From Time</label></td>  
										<td><label>To Time</label></td>  
									</tr>
									<tr>
									   <td><strong>Sunday:</strong></td>
									   <td><label><strong>{{isset($array['sunday']['from']) ? $array['sunday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['sunday']['to']) ? $array['sunday']['to'] : null }}</strong></label></td>  
									</tr>	
									<tr>
									   <td><strong>Monday:</strong></td>
									   <td><label><strong>{{isset($array['monday']['from']) ? $array['monday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['monday']['to']) ? $array['monday']['to'] : null }}</strong></label></td>  
									</tr>		
									<tr>
									   <td><strong>Tuesday:</strong></td>
									   <td><label><strong>{{isset($array['tuesday']['from']) ? $array['tuesday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['tuesday']['to']) ? $array['tuesday']['to'] : null }}</strong></label></td>  
									</tr>	
									<tr>
									   <td><strong>Wednesday:</strong></td>
									   <td><label><strong>{{isset($array['Wednesday']['from']) ? $array['Wednesday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['Wednesday']['to']) ? $array['Wednesday']['to'] : null }}</strong></label></td>  
									</tr>
									<tr>
									   <td><strong>Thursday:</strong></td>
									   <td><label><strong>{{isset($array['Thursday']['from']) ? $array['Thursday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['Thursday']['to']) ? $array['Thursday']['to'] : null }}</strong></label></td>  
									</tr>
									<tr>
									   <td><strong>Friday:</strong></td>
									   <td><label><strong>{{isset($array['Friday']['from']) ? $array['Friday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['Friday']['to']) ? $array['Friday']['to'] : null }}</strong></label></td>  
									</tr>
									<tr>
									   <td><strong>Saturday:</strong></td>
									   <td><label><strong>{{isset($array['saturday']['from']) ? $array['saturday']['from'] : null }}</strong></label></td>  
										<td><label><strong>{{  isset($array['saturday']['to']) ? $array['saturday']['to'] : null }}</strong></label></td>  
									</tr>
                                  
                                    </table>          
                                                                                            
                                </div>
                                          
                                    </div>
                                      
                                   
                                </div>
                            </div>
                        </div>
                    </div>

                  
                   
                </div>   
                </div>
                <!-- /.outer -->
                <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
                     aria-hidden="true">
                    <form>
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span class="float-right" aria-hidden="true">&times;</span>
                                </button>
                                <div class="input-group search_bar_small">
                                    <input type="text" class="form-control" placeholder="Search..." name="search">
                                    <span class="input-group-btn">
        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
      </span>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
        <!-- /#content -->
    </div>

<!-- startsec End -->
@include('footer')