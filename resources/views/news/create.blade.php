@include('header')
@include('sidebar')

<!-- /#left -->
 <style>
.error {
      color: red;
      
   }
   .fileinput.fileinput-new { margin-bottom: 0px;  }
.fileinput.fileinput-new .btn.btn-file {     overflow: visible;  width: 185px; position: relative; top:-15px; }
.fileinput.fileinput-new .m-t-20  { margin-top: 0px;   }
.fileinput.fileinput-new #profile-img-error {     position: absolute;
    top: 30px;
    left: 0; }
</style>
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                               @if($pid=="")
                               Add News
                               @else
                               Edit News
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/news')}}">News</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                   @if($pid=="")
                    <form method="POST" action="{{url('news/savenews')}}" id="adforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                      @else
                    <form method="POST" action="{{url('news/savenews')}}" id="editadforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    @endif
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 input_field_sections">
                                            <h5>Title</h5>
                                        </div>    
                                       <div class="col-sm-9 input_field_sections">                                       
                                           <input type="text" class="form-control" name="name" id="name" value="{{ ( isset($news->title) ? $news->title : null) }}" placeholder="Enter Title here.." minlength="1" maxlength="50"/>  
                                             
                                         </div>
                                      </div>
                                     <div class="row">
                                      <div class="col-sm-3 input_field_sections">
                                            <h5>Description</h5>
                                        </div> 
                                       <div class="col-sm-9 input_field_sections">                                        
                                           <textarea name="description" id="description">{{ ( isset($news->description) ? $news->description : null) }}</textarea>
                                        </div>
                                        
                                  </div>  

                                  <div class="row">
                                    <div class="col-sm-12 input_field_sections">
                                      <div class=" col-lg-12 text-center text-lg-left">
                                          <div class="col-sm-12 fileinput fileinput-new" data-provides="fileinput" style="width:100%;padding: 0px !Important;">
                                              <div class="row">
                                                  <div class="col-sm-3 ">
                                                      <h5>Image</h5>
                                                  </div> 
                                                  <div class="col-sm-9 ">   
                                                      <div class="fileinput-new img-thumbnail text-center" style="padding-top: 20px; border-radius:3px;">
                                                          @if(isset($news) && $news->image!="" && $news->image!=null)
                                                            <img src="{{url('/')}}/uploads/newsimage/{{$news->image}}" id="profile-img-tag" width="180px" height="140px"  name="profile_imaged" style="margin-top:15px;"/>
                                                          @else
                                                            <img src="{{url('/')}}/uploads/dummy/newslist.jpeg" id="profile-img-tag" width="180px" height="140px" name="profile_imaged" style="display:none;" />
                                                          @endif
                                                      </div>
                                                      <div class="fileinput-preview fileinput-exists img-thumbnail"></div>
                                                        <div class="m-t-20 text-center" style="position: relative;top: -100px;">
                                                            <span class="btn btn-primary btn-file">
                                                            <span>Select image</span>
                                                            <span class="fileinput-exists">Change</span>
                                                            <input type="file" name="profile_img" id="profile-img" class="fileinput-new" onchange="readURL(this);"  value="" accept="image/x-png,image/jpeg">
                                                            </span>
                                                                    <a href="#" class="btn btn-warning fileinput-exists" data-dismiss="fileinput">Remove</a>
                                                        </div>
																                  </div>
                                              </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div> 
                                     <div class="row"> 
                                       <div class="col-sm-3 input_field_sections">
                                          <h5>Status</h5>
                                        </div>
                                        <div class="col-sm-9 input_field_sections">
                                             <select class="form-control status" style="width: 100%;" name="status" id="status"  >
                                             <option value="1">Active</option>
                                             <option value="0" {{ (isset($news->status) && $news->status==0)  ? 'selected': '' }}>InActive</option>
                                             </select>
                                        </div>
                                      </div>                             
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit">
                              <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   </form>
                   
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->

<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
   @include('footer')

     <script>
 
  CKEDITOR.replace( 'description');
</script>  
 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.js"></script>


<script language="javascript">

   function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
               $('#profile-img-tag').show();
                $('#profile-img-tag').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
$( document ).ready(function() {
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");+
$.validator.addMethod("ckeditor", function(value, element) {
      //  var desc = CKEDITOR.instances.description.getData();
        return value=="";
      }, "Description field required");
      }); 
$('input, text').blur(function() {
        var value = $.trim( $(this).val() );
        $(this).val( value );
    });
 $('input, textarea').blur(function() {
        var value = $.trim( $(this).val() );
        $(this).val( value );
    });
$(function() {
  
   $( "#adforms" ).validate({
               ignore: [],
              debug: false,
       rules: {
               
        name: {
            required: true,
            minlength:3,
            maxlength:50,
            
              },
        description:{
             required: function() 
                {
                 CKEDITOR.instances.description.updateElement();
                },
           },
            profile_img: {
                      required: true,
                          accept: "image/*"
                    } 
       
        },
       messages: {
             
        name: {
            required: "Please Enter Title",
            
                 }, 
        description: {
            required: "Please Enter Description",
                 }, 
          profile_img: {
                            required: "Please upload file.",
                            accept: "Please upload file in these format only (jpg, jpeg, png)."
                        } 
       
                   },
        submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    },
   });  
});
$(function() {
  
   $( "#editadforms" ).validate({
               ignore: [],
              debug: false,
       rules: {
               
        name: {
            required: true,
            minlength:3,
            maxlength:50,
            
              },
        description:{
             required: function() 
                {
                 CKEDITOR.instances.description.updateElement();
                },
              
           },
            profile_img: {
                          accept: "image/*"
                    } 
       
        },
       messages: {
             
        name: {
            required: "Please Enter Title",
            
                 }, 
        description: {
            required: "Please Enter Description",
                 }, 
          profile_img: {
                            
                            accept: "Please upload file in these format only (jpg, jpeg, png)."
                        } 
       
                   }
   });  
});
$("#cancelform").click(function() {
   window.location.href = "{{url('/news')}}";
});

</script>