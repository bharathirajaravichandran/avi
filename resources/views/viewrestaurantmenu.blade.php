@include('header')
<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-lg-6 col-md-4 col-sm-4">
                  <h4 class="nav_top_align">
                     <i class="fa fa-th"></i>
                     View Restaurant Menu
                  </h4>
               </div>
               <div class="col-lg-6 col-md-8 col-sm-8">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/restaurantmenu"> View Restaurant Menu</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <div class="inner bg-container forms">
            <div class="row">
                <div class="col-sm-12" style="margin-bottom: 15px;">
                        <a href="{{url('/')}}/restaurantmenu/edit/{{$id}}" style="background-color: #ed7626;padding: 5px 20px;border-radius: 20px !IMPORTANT;display: inline-block;border: 1px solid #ed7626;color: #fff;" type="button" class="btn float-right">Edit</a>
                      </div>
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Menu Name</h5>
                              <strong>{{$restaurantmenudetails->name}}</strong>
                           </div>
                            <div class="col-sm-6 input_field_sections">
                              <h5>Restaurant Name</h5>
                              <strong>{{$restaurantmenudetails->restaurantname->name}}</strong>
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Menu Category</h5>
                              <strong>{{$restaurantmenudetails->restaurantmenucategory->name}}</strong>
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Food Type</h5>
                              <strong>{{$restaurantmenudetails->foodtype->name}}</strong>
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Description</h5>
                              @if($restaurantmenudetails->description!="")
                              <strong>{{$restaurantmenudetails->description}}</strong>
                              @else
                              <strong>Not Defined</strong>
                              @endif
                           </div>
                            <div class="col-sm-6 input_field_sections">
                              <h5>Status</h5>
                              <strong>
                                @if($restaurantmenudetails->status ==1)
                                Active
                                @else
                                Inactive
                                @endif
                              </strong>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Created Date</h5>
                              <strong>{{ \Carbon\Carbon::parse($restaurantmenudetails->created_at)->format('d M Y')}}</strong>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12 input_field_sections">
                           <h5>Images</h5>
                           <?php 
                              $imagelist = explode(",", $restaurantmenudetails->images);
                              if($restaurantmenudetails->images!="")
                              {
                                 if(count($imagelist) > 0)
                                 {
                                    foreach($imagelist as $k=>$v)
                                    { ?>

                                       </div>
                                       <div class="row">                        
                                    <div class="col-sm-4 input_field_sections">
                                       <img src="{{url('/')}}/uploads/restaurantmenu/{{$v}}" width="250px" height="250px">
                                    </div>
                                       <?php }
                                 }
                              }
                              else
                              echo "No Image";
                           ?>
                    
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- /.outer -->
         <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
            aria-hidden="true">
            <form>
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span class="float-right" aria-hidden="true">&times;</span>
                     </button>
                     <div class="input-group search_bar_small">
                        <input type="text" class="form-control" placeholder="Search..." name="search">
                        <span class="input-group-btn">
                        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->
@include('footer')