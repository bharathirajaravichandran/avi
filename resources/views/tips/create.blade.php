@include('header')
@include('sidebar')

<!-- /#left -->
  <style>
.error {
      color: red;
      
   }
</style>
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                               @if($pid=="")
                               Add Tips
                               @else
                               Edit Tips
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="{{url('/')}}">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/tips')}}">Tips</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                    <form method="POST" action="{{url('tips/savetips')}}" id="tipsforms" name="edit_tips" class="form-horizontal">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 input_field_sections">
                                            <h5>Airport</h5>
                                        </div> 
                                          <div class="col-sm-9 input_field_sections">   
                                     <select name="airport_id" id="airport_id" class="form-control select2 input-height">
                                                <option value="">Select Airport</option>
                                                @foreach($airports as $k=>$val)
                                                <option value="{{$val->id}}" {{ (isset($tips->airport_id) && $tips->airport_id==$val->id)  ? 'selected': '' }} >{{$val->name.'-'.$val->code}}</option>
                                                @endforeach
                                            </select>
                                          </div>
                                      </div>
                                     <div class="row">
                                      <div class="col-sm-3 input_field_sections">
                                            <h5>Description</h5>
                                        </div> 
                                       <div class="col-sm-9 input_field_sections">                                        
                                           <textarea name="description" id="description" class="form-control">{{ ( isset($tips->description) ? $tips->description : null) }}</textarea>
                                        </div>
                                        
                                  </div>  
                                     <div class="row"> 
                                       <div class="col-sm-3 input_field_sections">
                                          <h5>Status</h5>
                                        </div>
                                        <div class="col-sm-9 input_field_sections">
                                             <select class="form-control status" style="width: 100%;" name="status" id="status"  >
                                             <option value="1">Active</option>
                                             <option value="0" {{ (isset($tips->status) && $tips->status==0)  ? 'selected': '' }}>InActive</option>
                                             </select>
                                        </div>
                                    </div> 

                                                           
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit" id="submit" >
                                 <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   </form>
                   
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->
   @include('footer')
<script src="https://cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>
<script>
  CKEDITOR.replace( 'description');
</script> 

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">
$(function() {
  
   $( "#tipsforms" ).validate({
      ignore: [],
      rules: {    
        airport_id: {
            required: true,
           
              },
        description:{
           required: function() 
              {
               CKEDITOR.instances.description.updateElement();
              }, 
          },
      },
       messages: {
                    airport_id: {
                          required: "Please Select Airport"
                               }, 
                    description: {
                          required: "Please Enter Description"
                               },     
                },
                submitHandler: function(form){
                          $('form input[type=submit]').prop('disabled', true);
                          form.submit();
                },
    });  
});

$("#cancelform").click(function() {
   window.location.href = "{{url('/tips')}}";
});

</script>