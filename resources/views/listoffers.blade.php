
@include('header')
<!-- /#left -->
<div class="wrapper">
   @include('sidebar')
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-lg-6 col-md-4 col-sm-4">
                  <h4 class="nav_top_align">
                     <i class="fa fa-th"></i>
                     Offers
                  </h4>
               </div>
               <div class="col-lg-6 col-md-8 col-sm-8">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="index.php">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i> Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="#">Offers</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <div class="inner bg-container">
            <div class="row">
               <div class="col-12 data_tables">
                  <!-- BEGIN EXAMPLE2 TABLE PORTLET-->
                  <div class="card">
                     <div class="card-body m-t-35">
                        <div class="row">
                           <div class="col-sm-3">
                              <div class="btn-group show-hide">
                                 <a data-toggle="tooltip" data-placement="top" title="Add"  class="btn btn-primary"  href="add_offer.php" > <i class="fa fa-plus"></i> </a>
                                 <a data-toggle="tooltip" data-placement="top" title="Export" class="btn btn-primary" href="#" > <i class="fa fa-upload" aria-hidden="true"></i> </a>
                                 <a data-toggle="tooltip" data-placement="top" title="Import" class="btn btn-primary" href="#" > <i class="fa fa-download" aria-hidden="true"></i> </a>
                              </div>
                           </div>
                           <div class="col-sm-3"><input type="text" class="form-control startdate" placeholder="Start Date" /></div>
                           <div class="col-sm-3"><input type="text" class="form-control enddate" placeholder="End Date" /></div>
                           <div class="col-sm-3">
                              <select class="form-control">
                                 <option value="">Select Status</option>
                              </select>
                           </div>
                        </div>
                        <div class=" m-t-15">
                           <table class="table table-striped table-bordered table_res toggle_class" id="sample_6">
                              <thead>
                                 <tr>
                                    <th>Promo Code </th>
                                    <th>Offer Type</th>
                                    <th>Discount</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td>FLAT 50</td>
                                    <td>One time</td>
                                    <td>50%</td>
                                    <td>05-05-2018 to 06-05-2018</td>
                                    <td>Active</td>
                                    <td class="nowrap">
                                       <a data-toggle="tooltip" data-placement="top" title="Edit" href="edit_offer.php" class="btn btn-primary btn-xs">
                                       <i class="fa fa-pencil"></i>
                                       </a>
                                       <a data-toggle="tooltip" data-placement="top" title="View" href="view_offer.php" class="btn btn-info btn-xs">
                                       <i class="fa fa-eye"></i>
                                       </a>
                                       <button class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Delete">
                                       <i class="fa fa-trash-o "></i>
                                       </button>
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                  <!-- END EXAMPLE2 TABLE PORTLET-->
               </div>
            </div>
         </div>
         <!-- /.inner -->
      </div>
      <!-- /.outer -->
      <!-- Modal -->
      <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
         aria-hidden="true">
         <form>
            <div class="modal-dialog" role="document">
               <div class="modal-content">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span class="float-right" aria-hidden="true">&times;</span>
                  </button>
                  <div class="input-group search_bar_small">
                     <input type="text" class="form-control" placeholder="Search..." name="search">
                     <span class="input-group-btn">
                     <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
                     </span>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
   <!-- startsec End -->
</div>
@include('footer')