@include('header')
<div class="wrapper">
   <style type="text/css">
      .btn-file h5{
      margin: 5px 0px !important;
      color: #fff !important;
      }
      .customfile_btntypenew .btn {
      width: 100%;
      }
      .Err, .Err1, .Err2 { display:none; color:red!important; }
   </style>
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                     Add / Edit Floor Plan
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/airport">Airports</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <form action="{{url('airport/savefloorplan')}}" method="post" id="myforms" name="myform" enctype="multipart/form-data">
            <div class="inner bg-container forms">
               <div class="row">
                  <div class="col">
                     <div class="card">
                        <div class="card-body">
                           <div class="row">
                              <div class="col-sm-12 input_field_sections">
                                 <h5>Airport</h5>
                                 <select class="form-control" disabled>
                                    <option value="1" selected="selected">{{$airportdetails->name}}</option>
                                 </select>
                              </div>
                           </div>
                           <div class="floormore-sec">
                              <?php
                                 $i=1;
                                 if(count($floormap) > 0)
                                 {
                                   foreach($floormap as $floor_map)
                                   { ?>
                              <div class="row removesec" id="floormapid<?php echo $i; ?>">
                                 <div class="col-sm-5 input_field_sections" id="floormaptitle_inner<?php echo $i; ?>">
                                    <h5>Title</h5>
                                    <input type="text" class="form-control" name="floormaptitle<?php echo $i; ?>" id="floormaptitle<?php echo $i; ?>" value="{{old('storey', isset($floor_map->storey) ? $floor_map->storey : null) }}" maxlength="30" /> 
                                    <span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Title Field is required</span>
                                 </div>
                                 <div class="col-sm-5 input_field_sections" id="profile-img-inner<?php echo $i; ?>">
                                    <h5>Image</h5>
                                    <div data-provides="fileinput">
                                       <img src="{{url('/')}}/uploads/floorplan/{{$floor_map->floormap}}" id="profile-img-tag<?php echo $i; ?>" width="100%" height="50%" name="profile_imaged<?php echo $i; ?>" />
                                       <div class="customfile_btntypenew">
                                          <div class="btn btn-primary btn-file">
                                             <h5>Change</h5>
                                             <input type="file" name="profile_img<?php echo $i; ?>" id="profile-img<?php echo $i; ?>" class="fileinput-new" onchange="readURL(this,<?php echo $i; ?>);"  value="{{$floor_map->floormap}}" accept="image/x-png,image/jpeg,image/jpg">
                                          </div>
                                       </div>
                                    </div>
                                    <span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please upload valid image</span>
                                 </div>
                                 <div class="col-sm-2 input_field_sections" id="{{$floor_map->id}}">
                                    <h5>Remove</h5>
                                    <a href="javascript:;" class="remove-link remove-floor"><i class="fa fa-close"></i></a>
                                 </div>
                              </div>
                              <input type="hidden" id="existingid" name="existingid<?php echo $i; ?>" value="{{$floor_map->id}}" style="display: none;">
                              <?php 
                                 $i++;
                                 }
                                 } ?>
                              <div class="row" id="floormapid<?php echo $i; ?>">
                                 <div class="col-sm-5 input_field_sections" id="floormaptitle_inner<?php echo $i; ?>">
                                    <h5>Title</h5>
                                    <input type="text" class="form-control" name="floormaptitle<?php echo $i; ?>" id="floormaptitle<?php echo $i; ?>" maxlength="30" /> 
                                    <span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Title Field is required</span>
                                 </div>
                                 <div class="col-sm-5 input_field_sections" id="profile-img-inner<?php echo $i; ?>">
                                    <h5>Image</h5>
                                    <div data-provides="fileinput">
                                       <img src="#" id="profile-img-tag<?php echo $i; ?>" width="100%" height="50%" name="profile_imaged<?php echo $i; ?>" style="display:none;" />
                                       <div class="customfile_btntypenew">
                                          <div class="btn btn-primary btn-file">
                                             <h5>Browse</h5>
                                             <input type="file" name="profile_img<?php echo $i; ?>" id="profile-img<?php echo $i; ?>" class="fileinput-new" onchange="readURL(this,<?php echo $i; ?>);"  value="" accept="image/x-png,image/jpeg">
                                          </div>
                                       </div>
                                    </div>
                                    <span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please upload valid image</span>
                                 </div>
                                 <div class="col-sm-2 input_field_sections">
                                    <h5>Add More</h5>
                                    <button class="btn btn-info btn-addmorefloor" type="button"><i class="fa fa-plus"></i></button>
                                 </div>
                              </div>
                              <input type="hidden" id="currentvalue" name="currentvalue" value="<?php echo $i; ?>" />
                              <input type="hidden" id="lastvalue" name="lastvalue" value="<?php echo $i; ?>" />
                              <input type="hidden" id="removedfloor" name="removedfloor" value="" style="display: none;">
                              <input type="hidden" id="existingid" name="existingid<?php echo $i; ?>" value="0" style="display: none;">
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.row -->
               <div class=" m-t-35">
                  <div class="form-actions form-group row">
                     <div class="col-xl-12 text-center">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <input type="hidden" name="airport_id" value="{{$id}}">
                        <input type="button" class="btn btn-primary" value="Submit" onclick="return validation();">
                        <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                     </div>
                  </div>
               </div>
         </form>
         </div>
         <!-- /.outer -->
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->
@include('footer')
<script type="text/javascript">
   $(document).on('click', '.btn-addmorefloor', function(){
     var currentvalue = parseInt($('#currentvalue').val()) +1;
     var lastvalue = parseInt($('#lastvalue').val()) +1;
    $('.floormore-sec').append('<div class="row removesec" id="floormapid'+currentvalue+'"> <div class="col-sm-5 input_field_sections" id="floormaptitle_inner'+currentvalue+'"> <h5>Title</h5><input type="text" class="form-control" name="floormaptitle'+currentvalue+'" id="floormaptitle'+currentvalue+'" maxlength="30" /><span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Title Field is required</span></div><div class="col-sm-5 input_field_sections" id="profile-img-inner'+currentvalue+'"><h5>Image</h5> <div data-provides="fileinput"><img src="" id="profile-img-tag'+currentvalue+'" width="100%" height="50%" name="profile_imaged'+currentvalue+'" style="display:none;" /><div class="customfile_btntypenew"><div class="btn btn-primary btn-file"><h5>Browse</h5><input type="file" name="profile_img'+currentvalue+'" id="profile-img'+currentvalue+'" class="fileinput-new" onchange="readURL(this,'+currentvalue+');"  value="" accept="image/x-png,image/jpeg"></div></div></div><span class="Err" for="inputError"><i class="fa fa-times-circle-o"></i> Please upload valid image</span></div><div class="col-sm-2 input_field_sections"><h5>Remove</h5><a href="javascript:;" class="remove-link removelastvalue"><i class="fa fa-close"></i></a></div></div><input type="hidden" id="existingid" name="existingid'+currentvalue+'" value="0" style="display: none;">');
    $('#currentvalue').val(currentvalue);
    $('#lastvalue').val(lastvalue);
   
   });

    $(document).on('click', '.removelastvalue', function(){
    var lastvalue = parseInt($('#lastvalue').val()) -1;
    $('#lastvalue').val(lastvalue);
   });
   
    function readURL(input,value) {
         if (input.files && input.files[0]) {
           var file = input.files[0];
           var fileType = file["type"];
           var validImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg"];
           if ($.inArray(fileType, validImageTypes) != -1) {
             var reader = new FileReader();
             reader.onload = function (e) {
               $('#profile-img-tag'+value).show();
               $('#profile-img-tag'+value).attr('src', e.target.result);
             }
             reader.readAsDataURL(input.files[0]);
           }
           else
           $('#profile-img-tag'+value).hide();
         }
     }
   
   $(document).on('click', '.remove-floor', function(){
       var floorid = $(this).parent().attr('id');
       var list = $("#removedfloor").val();
       if(list!="")
         list = list+","+floorid;
       else
         list = floorid;
       $("#removedfloor").val(list);
   });
   
   function validation()
   {
    $(':input[type="button"]').prop('disabled', true);
     var err='';
     $('.Err').hide();
     $('.Err1').hide();
     var currentvalue =$('#lastvalue').val();
     for(var i=1; i<currentvalue; i++)
     {
       var title = $("#floormaptitle"+i).val();
       var image = $("#profile-img"+i).val();
       if(typeof image !="undefined")
       var ext = $("#profile-img"+i).val().split('.').pop().toLowerCase();
       else
       var ext ="";

      var imgsrc = $("#profile-img-tag"+i).attr('src');
   
       if((title=='' || title==null) && typeof title!="undefined")
       {
         $('#floormaptitle'+i).addClass('has-error');
         $('#floormaptitle_inner'+i+' .Err').show();
         if(err=='')
         {
           $('#floormaptitle'+i).focus();
           err='set';
         }
       }
       if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1 && ext!="")
       {
         $('#profile-img'+i).addClass('has-error');
         $('#profile-img-inner'+i+' .Err').show();
         if(err=='')
         {
           $('#profile-img'+i).focus();
           err='set';
         }
       }

        if(title!="" && ext=="" && (imgsrc=="" || imgsrc=="#"))
       {
          $('#profile-img'+i).addClass('has-error');
         $('#profile-img-inner'+i+' .Err').show();
         if(err=='')
         {
           $('#profile-img'+i).focus();
           err='set';
         }
       }
     }
      
    if(i==currentvalue)
    {
      var title = $("#floormaptitle"+i).val();
      var image = $("#profile-img"+i).val();
      if(typeof image !="undefined")
      var ext = $("#profile-img"+i).val().split('.').pop().toLowerCase();
      else
      var ext ="";
    
      var title = $("#floormaptitle"+i).val();

      if((title=='' || title==null) && ext!="")
      {
        $('#floormaptitle'+i).addClass('has-error');
        $('#floormaptitle_inner'+i+' .Err').show();
        if(err=='')
        {
          $('#floormaptitle'+i).focus();
          err='set';
        }
      }

      if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1 && ext!="")
       {
         $('#profile-img'+i).addClass('has-error');
         $('#profile-img-inner'+i+' .Err').show();
         if(err=='')
         {
           $('#profile-img'+i).focus();
           err='set';
         }
       }

       if(title!="" && ext=="")
       {
          $('#profile-img'+i).addClass('has-error');
         $('#profile-img-inner'+i+' .Err').show();
         if(err=='')
         {
           $('#profile-img'+i).focus();
           err='set';
         }
       }
   }

     if(i==1 && currentvalue==1)
     {
       var title = $("#floormaptitle"+i).val();
       if((title=='' || title==null))
       {
         $('#floormaptitle'+i).addClass('has-error');
         $('#floormaptitle_inner'+i+' .Err').show();
         if(err=='')
         {
           $('#floormaptitle'+i).focus();
           err='set';
         }
       }
     }
     if(err!='')
     {
         $(':input[type="button"]').prop('disabled', false);
         return false;
     }
     else
     {
         $('#myforms').submit();
     }
   }

    $("#cancelform").click(function() {
       window.location.href = "{{url('/airport')}}";
   }); 
</script>