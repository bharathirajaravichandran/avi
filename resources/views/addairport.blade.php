@include('header')
<!-- /#left -->
<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                     @if($id==0)
                     Add Airport
                     @else
                     Update Airport
                     @endif
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/airport"> Airport</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <div class="outer">
         <form action="{{url('airport/saveairport')}}" method="post" id="myforms" name="myform">
         <div class="inner bg-container forms">
            <div class="row">
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Name</h5>
                              <input type="text" class="form-control" name="name" id="name" value="{{old('name', isset($airportdetails->name) ? $airportdetails->name : null) }}" minlength="1" maxlength="50" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Phone No</h5>
                              <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone', isset($airportdetails->phone) ? $airportdetails->phone : null) }}" maxlength="16"/>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Email</h5>
                              <input type="text" class="form-control" name="email" id="email" value="{{old('email', isset($airportdetails->email) ? $airportdetails->email : null) }}" maxlength="40" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Airport ID</h5>
                              <input type="text" class="form-control" name="code" id="code" value="{{old('code', isset($airportdetails->code) ? $airportdetails->code : null) }}" minlength="1" maxlength="20" />
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Address</h5>
                              <input type="text" class="form-control" name="address" id="address" value="{{old('address', isset($airportdetails->address) ? $airportdetails->address : null) }}" />
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Country</h5>
                              <select class="form-control" name="country_id" id="country_id" >
                               <option value="">Select Country</option>
                                 @foreach($country as $key=>$val)
                                 <option value="{{$key}}" {{old('country_id', isset($airportdetails->country_id) ? $airportdetails->country_id : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                              </select>
                            </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>State</h5>
                              <select class="form-control" name="state_id" id="state_id">
                                 <option value="">Select State</option>
                                 @if($id!="0")
                                 @foreach($states as $k=>$val)
                                 <option value="{{$k}}"{{old('state_id', isset($airportdetails->state_id) ? $airportdetails->state_id : '') == $k ? 'selected' : '' }} >{{$val}}</option>
                                 @endforeach
                                 @endif
                              </select>
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>City</h5>
                               <select class="form-control" name="city_id" id="city_id">
                                 <option value="">Select City</option>
                                 @if($id!="0")
                                 @foreach($cities as $citykey=>$val)
                                 <option value="{{$citykey}}"{{old('city_id', isset($airportdetails->city_id) ? $airportdetails->city_id : '') == $citykey ? 'selected' : '' }} >{{$val}}</option>
                                 @endforeach
                                 @endif
                              </select>
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Latitude</h5>
                              <input type="text" class="form-control" name="latitude" id="latitude" value="{{old('latitude', isset($airportdetails->latitude) ? $airportdetails->latitude : null) }}" maxlength="20"/>
                           </div>
                           <div class="col-sm-6 input_field_sections">
                              <h5>Longitude</h5>
                              <input type="text" class="form-control" name="longitude" id="longitude" value="{{old('longitude', isset($airportdetails->longitude) ? $airportdetails->longitude : null) }}" maxlength="20" />
                           </div>
                        </div>
                         <div class="row">
                           <div class="col-sm-6 input_field_sections">
                              <h5>Transport Availability</h5>
                             <select class="form-control transport" id="transport" name="transport[]" multiple>
                                 <option  value="">Please Select Transport</option>
                                 @foreach($vehicle as $k=>$v)
                                 <option value="{{$k}}" {{ isset($transport) && (in_array($k, $transport)) ? 'selected': '' }}>{{$v}}</option>
                                 @endforeach
                              </select> 
                           </div>
                            <div class="col-sm-6 input_field_sections">
                              <h5>Terminals</h5>
                              <select class="form-control terminal" id="terminal" name="terminal[]" multiple>
                                 <option  value="">Please Select Terminal</option>
                                 @foreach($terminals as $k=>$v)
                                 <option value="{{$k}}">{{$v}}</option>
                                 @endforeach
                              </select> 
                           </div>
                        </div>
                             <div class="row">
                                 <div class="col-sm-6 input_field_sections">
                                    <h5>Status</h5>
                                    <select class="form-control" id="status" name="status">
                                       <option  value="">Please Select Status</option>
                                       <option value="1" {{ old('status', isset($airportdetails->status) ? $airportdetails->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                        <option value="0" {{ old('status', isset($airportdetails->status) ? $airportdetails->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                                    </select> 
                                 </div>
                         </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- /.row -->
            <div class=" m-t-35">
               <div class="form-actions form-group row">
                  <div class="col-xl-12 text-center">
                     <input type="hidden" name="id" id="pid" value="{{$id}}">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                     <input type="submit" class="btn btn-primary" value="Submit">
                     <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                  </div>
               </div>
            </div>
         </div>
         </form>
         <!-- /.outer -->
         <div class="modal fade" id="search_modal" tabindex="-1" role="dialog"
            aria-hidden="true">
            <form>
               <div class="modal-dialog" role="document">
                  <div class="modal-content">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                     <span class="float-right" aria-hidden="true">&times;</span>
                     </button>
                     <div class="input-group search_bar_small">
                        <input type="text" class="form-control" placeholder="Search..." name="search">
                        <span class="input-group-btn">
                        <button class="btn btn-light" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->

<style type="text/css">
   body
   {
   font-family: Arial, Sans-serif;
   }
   .error
   {
   color:red;
   font-family:verdana, Helvetica;
   }
   #terminal-error {
      position: absolute;
      bottom: -11px;
   }
</style>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>

@include('footer')

<script type="text/javascript">
    $(document).ready(function(){
     $("#terminal").select2({"allowClear":true,"placeholder":"Please select terminal"}); 
     $("#transport").select2({"allowClear":true,"placeholder":"Please select transport"}); 
        });
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

      $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\s0-9 ]+$/i.test(value);
      }, "Alpha Numeric characters only allowed."); 

      $.validator.addMethod("airportidvalidation", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\s0-9]+$/i.test(value);
      }, "Alpha Numeric characters only allowed."); 

      $.validator.addMethod("phonenumber", function(value, element) {
        return this.optional(element) || /^[0-9-_]+$/i.test(value);
      }, "VAlid Phone number only allowed."); 

      $.validator.addMethod("addresscode", function(value, element) {
        return this.optional(element) || /^[0-9.]+$/i.test(value);
      }, "Address code should be only numbers."); 

      $.validator.addMethod("nowhitespace", function(value, element) {
      	return this.optional(element) || value.trim()!="";
      }, "No white space please");

   });
</script>
<script type="text/javascript">
$(function()
{
  $("#myforms").validate(
  {
    onfocusout: false,
    invalidHandler: function(form, validator) {
       var errors = validator.numberOfInvalids();
       if (errors) {
           validator.errorList[0].element.focus();
       }
    },
    rules:{
      name: {
        required:true,
        lettersonly:true,
         nowhitespace:true,
      },
      phone: {
        required:true,
        phonenumber:true,
        minlength: 8,
        maxlength:16
      },
      email: {
        required:true,
        email:true,
      },
      code: {
        required:true,
        airportidvalidation:true,
        remote:{
        url : "{{ url('/') }}/airportexist",
        type: 'GET',
        data :{ 
          id: $('#pid').val(),
          code: this.value,
        },
         complete: function(data) {
                //console.log(data);
          }
        }
      },
      address: {
        required:true,
         nowhitespace:true,
      },
      country_id: {
        required:true,
      },
      state_id: {
        required:true,
      },
      city_id: {
        required:true,
      }, 
      latitude: {
        required:true,
        addresscode:true,
      },
      longitude: {
        required:true,
        addresscode:true,
      },
      status: {
        required:true,
      },
      "transport[]": {
        required:true,
      },
      "terminal[]": {
        required:true,
      }
    },
    messages:{
      name:{
        required:"Name Field is required",
        lettersonly:"Name Field is invalid",
        nowhitespace:"Name Field is invalid",
      },
      phone:{
        required:"Phone Number Field is required",
        phonenumber:"Phone Number Field is invalid",
      },
      email:{
        required:"Email Field is required",
        email:"Email Field is invalid",
      },
      code:{
        required:"Airport ID Field is required",
        remote:"This Airport ID is already exist",
      },   
      address:{
        required:"Address Field is required",
        nowhitespace:"Address Field is invalid",
      }, 
      country_id:{
        required:"Country Field is required",
      },     
      state_id:{
        required:"State Field is required",
      },     
      city_id:{
        required:"City Field is required",
      },   
      latitude:{
        required:"Latitude Field is required",
        addresscode:"Latitude Field is invalid",
      },  
      longitude:{
        required:"Longitude Field is required",
        addresscode:"Latitude Field is invalid",
      }, 
      status:{
        required:"Status Field is required",
      }, 
      "transport[]":{
        required:"transport Field is required",
      },
      "terminal[]":{
        required:"Terminal Field is required",
      },             
    },
    submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    },
  });   

});

$("#cancelform").click(function() {
   window.location.href = "{{url('/airport')}}";
}); 
</script>

<script type="text/javascript">
   $('#country_id').change(function(){
   var countryID = $(this).val();    
   if(countryID){
       $.ajax({
          type:"GET",
          url:"{{url('get_state_list')}}?country_id="+countryID,
          success:function(res){   
          
           if(res){
               $("#state_id").empty();
               $("#state_id").append('<option value="">Select State</option>');
               $.each(res,function(key,value){
                   $("#state_id").append('<option value="'+key+'">'+value+'</option>');
               });
               $("#city_id").empty();
               $("#city_id").append('<option value="">Select City</option>');
            
           }else{
              $("#state_id").empty();
              $("#city_id").empty();
              $("#city_id").append('<option value="">Select City</option>');
           }
          }
       });
   }else{
       $("#state_id").empty();
       $("#city_id").empty();
   }      
   });

   $('#state_id').on('change',function(){ 
   var stateID = $(this).val();    
      if(stateID){
       $.ajax({
          type:"GET",
          url:"{{url('get_city_list')}}?state_id="+stateID,
          success:function(res){  
          if(res){
               $("#city_id").empty();
               $("#city_id").append('<option value="">Select City</option>');
               $.each(res,function(key,value){
                   $("#city_id").append('<option value="'+key+'">'+value+'</option>');
               });
          
           }else{
              $("#city_id").empty();
           }
          }
       });
   }else{
       $("#city_id").empty();
   }
   });
</script>

@if (isset($selectedterminal))

<script type="text/javascript">
$(function () {
    $("#terminal").val({{$selectedterminal}}).select2({"allowClear":true,"placeholder":"Please select Terminal"});
  });
</script>
 @endif