@include('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link href="http://dmkportal.com/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/> -->


<style type="text/css">
.fileinput-remove {display:none;}
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash:before {  position: absolute;  content: '\f1f8' !important;
    top: -14px;   left: -6px;   font-size: 18px;   font-family: fontawesome;    font-style: normal; color:#a94442; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in   {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in:before {  position: absolute;  content: '\f00e' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: #444; }
.file-upload-indicator .glyphicon-exclamation-sign   {  position: relative; }   
.file-upload-indicator .glyphicon-exclamation-sign:before {  position: absolute;  content: '\f06a' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: red; }
.file-thumbnail-footer .file-footer-buttons .kv-file-remove:hover { background: #f4f4f4;  }
.file-thumbnail-footer .file-footer-buttons .kv-file-zoom:hover { background: #f4f4f4;  }
.Err, .Err1 { display:none; color:red!important; }
</style>

<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                     @if($id==0)
                     Add Restaurant Menu
                     @else
                     Update Restaurant Menu
                     @endif
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/restaurantmenu">Restaurant Menus</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <style type="text/css">
      .select2-selection__clear {display: none;}
      </style>
      <div class="outer">
         <form action="{{url('restaurantmenu/saverestaurantmenu')}}" method="post" id="myforms" name="myform" enctype="multipart/form-data">
         <div class="inner bg-container forms">
            <div class="row">
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="restaurant_id_inner">
                              <h5>Restaurant</h5>
                              <select class="form-control" name="restaurant_id" id="restaurant_id">
                                 <option value="">Select Restaurant</option>
                                 @foreach($restaurantlist as $key=>$val)
                                 <option value="{{$key}}" {{old('restaurant_id', isset($restaurantmenudetails->restaurant_id) ? $restaurantmenudetails->restaurant_id : '') == $key ? 'selected' : '' }} @if($resid!="" && $resid==$key) selected @endif >{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Restaurant field is required.</span>
                            </div>
                           <div class="col-sm-6 input_field_sections" id="name_inner">
                              <h5>Menu Name</h5>
                              <input type="text" class="form-control" name="name" id="name" value="{{old('name', isset($restaurantmenudetails->name) ? $restaurantmenudetails->name : null) }}"  minlength="1" maxlength="30" />
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Menu Name field is required.</span>
                           </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 input_field_sections" id="currency_id_inner">
                              <h5>Currency</h5>
                              <select class="form-control" name="currency_id" id="currency_id">
                                 <option value="">Select Currency</option>
                                 @foreach($currentlist as $key=>$val)
                                 <option value="{{$key}}" {{old('currency_id', isset($restaurantmenudetails->currency_id) ? $restaurantmenudetails->currency_id : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Currency field is required.</span>
                            </div>
                             <div class="col-sm-6 input_field_sections" id="price_inner">
                                <h5>Price</h5>
                                <input type="text" class="form-control" name="price" id="price" value="{{old('price', isset($restaurantmenudetails->price) ? $restaurantmenudetails->price : null) }}" maxlength="8" />
                                <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Price field is required.</span>
                                <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Price field is invalid.</span>
                             </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="food_type_id_inner">
                              <h5>Food Type</h5>
                              <select class="form-control" name="food_type_id" id="food_type_id">
                                 <option value="">Select Currency</option>
                                 @foreach($foodtypelist as $key=>$val)
                                 <option value="{{$key}}" {{old('food_type_id', isset($restaurantmenudetails->food_type_id) ? $restaurantmenudetails->food_type_id : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Food Type field is required.</span>
                            </div>
                             <div class="col-sm-6 input_field_sections" id="restaurant_menu_category_id_inner">
                              <h5>Menu Category</h5>
                              <select class="form-control" name="restaurant_menu_category_id" id="restaurant_menu_category_id">
                                 <option value="">Select Menu Category</option>
                                 @foreach($restaurantmenucategory as $key=>$val)
                                 <option value="{{$key}}" {{old('restaurant_menu_category_id', isset($restaurantmenudetails->restaurant_menu_category_id) ? $restaurantmenudetails->restaurant_menu_category_id : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Menu Category field is required.</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 input_field_sections" id="restaurantmenuid_inner">
                              <h5>Description</h5>
                              <textarea class="form-control" rows="5" name="description">{{old('description', isset($restaurantmenudetails->description) ? $restaurantmenudetails->description : null) }}</textarea>
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Description field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="status_inner">
                              <h5>Status</h5>
                              <select class="form-control" id="status" name="status">
                                 <option  value="">Please Select Status</option>
                                 <option value="1" {{ old('status', isset($restaurantmenudetails->status) ? $restaurantmenudetails->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                  <option value="0" {{ old('status', isset($restaurantmenudetails->status) ? $restaurantmenudetails->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                              </select> 
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Status field is required.</span>
                           </div>
                        </div>
                        <div class="row" id="timeline_mainimage">
                           <div class="col-sm-12 input_field_sections" id="timeline_mainimage_inner">
                              <h5>Restaurant Menu Images</h5>
                               @if (($main_image) != '')
                                <input id="timeline_images" type="file"  class="MediaFilesPath" id="MediaFilesPath" name="MediaFilesPath[]" multiple="1" /> 
                                <input type="hidden" name="hidden_image" id="hidden_image" value="{{$main_image}}">
                                <textarea name="deleted_hidden_image" id="deleted_hidden_image" value="" style="display:none"></textarea>

                                @else
                                <input id="timeline_images" type="file" class="MediaFilesPath" name="MediaFilesPath[]" multiple="1" />
                                <input type="hidden" name="hidden_image" id="hidden_image" value="">
                               <textarea name="deleted_hidden_image" id="deleted_hidden_image" value="" style="display:none"></textarea>
                                @endif 
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>Please provide valid image.</span>
                           </div>
                        </div>
                        <div id="testing" style="display:none;">  </div>
                           <input type="hidden" id="hiddencount" name="hiddencount" value="1">
                     </div>
                  </div>   
               </div>
            </div>
            <!-- /.row -->
            <div class=" m-t-35">
               <div class="form-actions form-group row">
                  <div class="col-xl-12 text-center">
                     <input type="hidden" name="id" id="pid" value="{{$id}}">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                     <input type="button" class="btn btn-primary" value="Submit" onclick="return validation();">
                     <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->


@include('footer')   



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script type="text/javascript">
   $('#airportid').change(function(){
      var airportID = $(this).val();    
      if(airportID){
         $.ajax({
            type:"GET",
            url:"{{url('get_terminal')}}?airportid="+airportID,
            success:function(res){   
               if(res){
                  $("#terminalid").empty();
                  $("#terminalid").append('<option value="">Please Select Terminal</option>');
                  $.each(res,function(key,value){
                      $("#terminalid").append('<option value="'+key+'">'+value+'</option>');
                  });
               
               }else{
                 $("#terminalid").empty();
                 $("#terminalid").append('<option value="">Please Select Terminal</option>');
               }
            }
         });
      }else{
          $("#terminalid").empty();
         $("#terminalid").append('<option value="">Please Select Terminal</option>');
      }      
   });

   $(document).ready(function(){
     $("#facilities").select2({"allowClear":true,"placeholder":"Please select facility"}); 
   });
</script>

  <script>
    $(function () {
                
        @if(count($image_array)>0)

          $("#timeline_images").fileinput({
            "initialPreview":[@foreach($image_array as $k=>$v)"{{ url('/') }}/uploads/restaurantmenu/{{$v}}",@endforeach],
            "initialPreviewConfig":[@foreach($image_array as $k=>$v)
            {
               "caption":"{{$v}}",
               "url":"{{ url('/') }}/restaurantmenu/deleteimage",
               "key":"{{$k}}"
            },
            @endforeach],
            "overwriteInitial":false,
            "initialPreviewAsData":true,
            "browseLabel":"Browse",
            "showRemove":false,
            "showUpload":false,
            "uploadUrl":"#" , 
            "fileActionSettings": {
              "showUpload": false,
              },
             "initialPreviewDelete":true, 
             "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
            "deleteExtraData":{"MediaFilesPath[multiple_image]":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"GET"},
            "deleteUrl":"#",
            dropZoneEnabled: false,
            maxFileSize: 4000,
            maxFileCount: 5,
          });

        @else

          $("#timeline_images").fileinput({
            "overwriteInitial":false,
            "initialPreviewAsData":true,
            "browseLabel":"Browse",
            "showRemove":false,
            "showUpload":false,
            "uploadUrl":"#" , 
            "fileActionSettings": {
              "showUpload": false,
              },
            "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
            "deleteExtraData":{"MediaFilesPath":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},

            "uploadExtraData": function(previewId, index) {
              return {key: index};
            },
            "deleteUrl":"#",
            dropZoneEnabled: false,
            maxFileSize: 4000,
            maxFileCount: 5,
          });

        @endif   
    });

    $(document).ready(function(){
        $('.MediaFilesPath').change(function(e){ 
             var hdcnt = $('#hiddencount').val();
             var $this = $(this), $clone = $this.clone(true);
             if(hdcnt == 1)
             {
               $this.after($clone).prop('id', 'MediaFilesPathnew1');
               $this.after($clone).prop('class', 'MediaFilesPathnew1');
               $this.after($clone).prop('name', 'MediaFilesPathnew1[]');
               $this.after($clone).prop('readonly', 'readonly');
               $this.after($clone).appendTo('#testing');
               hdcnt++;
               $('#hiddencount').val(hdcnt);
             }
        });

          $('#timeline_images').change(function(e){ 
            var i;
            var resultset = $('#hidden_image').val();
            for (i = 0; i < e.target.files.length; ++i) {
              var fileName = e.target.files[i].name;
              if(resultset!="")
              resultset = resultset+","+fileName;
              else
              resultset = fileName;
              var formData = new FormData();
              formData.append('#timeline_images', fileName, "Test");
            }
            
            //$('#hidden_image').val(resultset);
             var hdcnt = $('#hiddencount').val();
             var $this = $(this), $clone = $this.clone(true);

             $this.after($clone).prop('id', 'MediaFilesPathnew'+hdcnt);
             $this.after($clone).prop('class', 'MediaFilesPathnew'+hdcnt);
             $this.after($clone).prop('name', 'MediaFilesPathnew'+hdcnt+'[]');
             $this.after($clone).prop('readonly', 'readonly');
             $this.after($clone).appendTo('#testing');
             hdcnt++;

           
            $('#hiddencount').val(hdcnt);
        });


   $('body').on('click', '.kv-file-remove', function(e) {
          var value = $(this).parent().parent().parent().children().prop('title');
          var list = $("#deleted_hidden_image").val();

          if(list!="")
            list = list+","+value;
          else
            list = value;

          $("#deleted_hidden_image").html(list);
          return false;
        });
    });

   $("#cancelform").click(function() {
       window.location.href = "{{url('/restaurantmenu')}}";
   }); 

   </script>

@if (isset($selectedfacility))

<script type="text/javascript">
$(function () {
    $("#facilities").val({{$selectedfacility}}).select2({"allowClear":true,"placeholder":"Please select Facility"});
  });
</script>
 @endif

 <script type="text/javascript">
   
function validation()
{

   $(':input[type="button"]').prop('disabled', true);

   var restaurant = $("#restaurant_id").val();
   var menuname = $("#name").val();
   var currency = $("#currency_id").val();
   var price = $("#price").val();
   var foodtype = $("#food_type_id").val();
   var menucategory = $("#restaurant_menu_category_id").val();
   var description = $("#description").val();
   var status = $("#status").val();

   var nameregex = /^[A-Za-z0-9 _]*$/;

   var err='';
   var checkeror = 0;
   $('.Err').hide();
   $('.Err1').hide();

   if((restaurant=='' || restaurant==null))
   {
     $('#restaurant_id').addClass('has-error');
     $('#restaurant_id_inner .Err').show();
     if(err=='')
     {
         $('#restaurant_id').focus();
         err='set';
     }
   }

   if((menuname=='' || menuname==null))
   {
        $('#name').addClass('has-error');
        $('#name_inner .Err').show();
        if(err=='')
        {
            $('#name').focus();
            err='set';
        }
   }
   
   if(currency=='' || currency==null)
   {
        $('#currency_id').addClass('has-error');
        $('#currency_id_inner .Err').show();
        if(err=='')
        {
            $('#currency_id').focus();
            err='set';
        }
   }  

   if(price=='' || price==null)
   {
        $('#price').addClass('has-error');
        $('#price_inner .Err').show();
        if(err=='')
        {
            $('#price').focus();
            err='set';
        }
   } 

   if(foodtype=='' || foodtype==null)
   {
        $('#food_type_id').addClass('has-error');
        $('#food_type_id_inner .Err').show();
        if(err=='')
        {
            $('#food_type_id').focus();
            err='set';
        }
   }

   if(menucategory=='' || menucategory==null)
   {
        $('#restaurant_menu_category_id').addClass('has-error');
        $('#restaurant_menu_category_id_inner .Err').show();
        if(err=='')
        {
            $('#restaurant_menu_category_id').focus();
            err='set';
        }
   }

/*   if(description=='' || description==null)
   {
        $('#description').addClass('has-error');
        $('#description_inner .Err').show();
        if(err=='')
        {
            $('#description').focus();
            err='set';
        }
   } */ 

   if(status=='' || status==null)
   {
        $('#status').addClass('has-error');
        $('#status_inner .Err').show();
        if(err=='')
        {
            $('#status').focus();
            err='set';
        }
   }
   if ( $(".file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
   {
     $('#timeline_images').addClass('has-error');
     $('#timeline_mainimage_inner .Err').show();
     if(err=='')
     {
         $('#timeline_images').focus();
         err='set';
     }
   }

   if(err!='')
   {
     $(':input[type="button"]').prop('disabled', false);
     return false;
   }
   else
   {
      checkunique();
     
   }
}

  function checkunique() {
    $.ajax({
      type : "POST",
      url : "{{ url('/') }}/restaurantmenu/restaurantmenuexist",
      data : $('#myforms').serialize(),
      beforeSend : function() {
      },
      success : function(data) { 
        if(data == "failed")
        {
            $(':input[type="button"]').prop('disabled', false);
            $('#name').addClass('has-error');
            $('#rname_inner .Err1').show();
            $('#name').focus();
            return false;
        }
        else
            $('#myforms').submit();
        },
    });
  }

 </script>

 