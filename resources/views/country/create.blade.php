@include('header')
@include('sidebar')

<!-- /#left -->
        <div id="content" class="bg-container">
            <header class="head">
                <div class="main-bar">
                   <div class="row no-gutters">
                       <div class="col-sm-5 col-lg-6 skin_txt">
                           <h4 class="nav_top_align">
                               <i class="fa fa-plus"></i>
                                 @if($pid=="")
                               Add Country
                               @else
                               Edit Country
                               @endif
                           </h4>
                       </div>
                       <div class="col-sm-7 col-lg-6">
                           <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                               <li class="breadcrumb-item">
                                   <a href="#">
                                       <i class="fa fa-home" data-pack="default" data-tags=""></i>
                                       Dashboard
                                   </a>
                               </li>
                               <li class="breadcrumb-item">
                                   <a href="{{url('/country')}}">Country</a>
                               </li>
                             
                           </ol>
                       </div>
                   </div>
                </div>
            </header>
            <div class="outer">
                <div class="inner bg-container forms">
                   <form method="POST" action="{{url('country/savecountry')}}" id="countryforms" name="edit_country" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                          {{ csrf_field() }}
                                <div class="card-body">
                                    <div class="row">
                                       <div class="col-sm-6 input_field_sections">
                                            <h5>Country Code</h5>
                                             <input class="form-control" name="code" type="text" id="code" value="{{ old('sortname', isset($country->sortname) ? $country->sortname : null) }}" minlength="1" maxlength="3" required="true" placeholder="Enter Country Code here..">
                                             <div id="code_inner"></div>
                                       </div>
                                       <div class="col-sm-6 input_field_sections">
                                            <h5>Country Name</h5>
                                           <input type="text" class="form-control" name="name" id="name" value="{{ ( isset($country->name) ? $country->name : null) }}" placeholder="Enter Country Name here.." minlength="1" maxlength="30"/>  
                                              {!! $errors->first('Country name', '<p class="help-block">:message</p>') !!}
                                         </div>
                                     </div>
                                      <div class="row">
                                        <div class="col-sm-6 input_field_sections">
                                            <h5>Phone Code</h5>
                                           <input type="text" class="form-control" name="phonecode" id="phonecodes" value="{{ ( isset($country->phonecode) ? $country->phonecode : null) }}" placeholder="Enter Phone Code here.." maxlength="10" />
                                       </div> 
                                       </div>                                 
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- /.row -->
                    <div class=" m-t-35">

                        <div class="form-actions form-group row">
                            <div class="col-xl-12 text-center">
                              <input type="hidden" name="pid" value="{{$pid}}">
                               <input type="submit" class="btn btn-primary" value="Submit">
                                <input type="submit" class="btn btn-default" value="Cancel" id="cancelform">
                            </div>
                        </div>
                    </div>
                   </form>
                </div>
                <!-- /.outer -->
            </div>
        </div>
        <!-- /#content -->
    </div>
<!-- startsec End -->

  <style type="text/css">
     body
     {
     font-family: Arial, Sans-serif;
     }
     .error
     {
     color:red;
     font-family:verdana, Helvetica;
     }
     #terminal-error {
        position: absolute;
        bottom: -11px;
     }
    </style>
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"
   type="text/javascript"></script>

   @include('footer')


<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.12.0/jquery.validate.min.js"
   type="text/javascript"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
<script language="javascript">

jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-z ]+$/i.test(value);
}, "<span class='testing' style='color:red'>  Please enter only letters</span>");

$(function() {
  
   $( "#countryforms" ).validate({
       rules: {
                code: {
            required: true,
            lettersonly: true,
            maxlength:3

              },
        name: {
            required: true,
            lettersonly: true,
            minlength:3,
            maxlength:15
              }, 
       phonecode: {
            required: true,
            number:true,
            minlength:3,
            maxlength:15

              },  
        },
       messages: {
                code: {
            required: " Please enter Country Code"
                 },
        name: {
            required: "Please enter Country Name"
                 },     
        phonecode: {
            required: "Please enter Phone Code"
          
                 },
        
      },

      submitHandler: function(form){
      $('form input[type=submit]').prop('disabled', true);
      form.submit();
    },
    
   });  
});
$("#cancelform").click(function() {
   window.location.href = "{{url('/country')}}";
});

</script>