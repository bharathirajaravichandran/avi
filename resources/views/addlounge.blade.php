@include('header')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/css/fileinput.css" media="all" rel="stylesheet" type="text/css"/>
<!-- <link href="http://dmkportal.com/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css"/> -->


<style type="text/css">
.fileinput-remove {display:none;}
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-trash:before {  position: absolute;  content: '\f1f8' !important;
    top: -14px;   left: -6px;   font-size: 18px;   font-family: fontawesome;    font-style: normal; color:#a94442; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in   {  position: relative; }   
.file-thumbnail-footer .file-footer-buttons .glyphicon.glyphicon-zoom-in:before {  position: absolute;  content: '\f00e' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: #444; }
.file-upload-indicator .glyphicon-exclamation-sign   {  position: relative; }   
.file-upload-indicator .glyphicon-exclamation-sign:before {  position: absolute;  content: '\f06a' !important;
    top: -14px;   left: -8px;  font-size: 18px;  font-family: fontawesome;   font-style: normal;   color: red; }
.file-thumbnail-footer .file-footer-buttons .kv-file-remove:hover { background: #f4f4f4;  }
.file-thumbnail-footer .file-footer-buttons .kv-file-zoom:hover { background: #f4f4f4;  }
.Err, .Err1 ,.Err2{ display:none; color:red!important; }
</style>

<div class="wrapper">
   @include('sidebar')
   <!-- /#left -->
   <div id="content" class="bg-container">
      <header class="head">
         <div class="main-bar">
            <div class="row no-gutters">
               <div class="col-sm-5 col-lg-6 skin_txt">
                  <h4 class="nav_top_align">
                     <i class="fa fa-plus"></i>
                     @if($id==0)
                     Add Lounge
                     @else
                     Update Lounge
                     @endif
                  </h4>
               </div>
               <div class="col-sm-7 col-lg-6">
                  <ol class="breadcrumb float-right nav_breadcrumb_top_align">
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}">
                        <i class="fa fa-home" data-pack="default" data-tags=""></i>
                        Dashboard
                        </a>
                     </li>
                     <li class="breadcrumb-item">
                        <a href="{{url('/')}}/lounge">Lounges</a>
                     </li>
                  </ol>
               </div>
            </div>
         </div>
      </header>
      <style type="text/css">
      .select2-selection__clear {display: none;}
      </style>
      <div class="outer">
         <form action="{{url('lounge/savelounge')}}" method="post" id="myforms" name="myform" enctype="multipart/form-data">
         <div class="inner bg-container forms">
            <div class="row">
               <div class="col">
                  <div class="card">
                     <div class="card-body">
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="name_inner">
                              <h5>Lounge Name</h5>
                              <input type="text" class="form-control" name="name" id="name" value="{{old('name', isset($loungedetails->name) ? $loungedetails->name : null) }}"  minlength="1" maxlength="30" />
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Lounge Name field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="loungeid_inner">
                              <h5>Lounges ID</h5>
                              <input type="text" class="form-control" name="loungeid" id="loungeid" value="{{old('loungeid', isset($loungedetails->loungeid) ? $loungedetails->loungeid : null) }}" maxlength="30" />
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Lounge ID field is required.</span>
                               <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> This Lounge ID is already exist.</span>
                           </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6 input_field_sections" id="airportid_inner">
                              <h5>Airport</h5>
                              <select class="form-control" name="airportid" id="airportid">
                                 <option value="">Select Airport</option>
                                 @foreach($airport as $key=>$val)
                                 <option value="{{$key}}" {{old('airportid', isset($loungedetails->airportid) ? $loungedetails->airportid : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Airport field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="terminalid_inner">
                              <h5>Terminal</h5>
                              <select class="form-control" name="terminalid" id="terminalid">
                                 <option value="">Select Terminal</option>
                                 @if($id!=0)
                                 @foreach($terminal as $key=>$val)
                                  <option value="{{$key}}" {{old('terminalid', isset($loungedetails->terminalid) ? $loungedetails->terminalid : '') == $key ? 'selected' : '' }}>{{$val}}</option>
                                 @endforeach
                                 @endif
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Terminal field is required.</span>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="email_inner">
                              <h5>Email</h5>
                              <input type="text" class="form-control" name="email" id="email" value="{{old('email', isset($loungedetails->email) ? $loungedetails->email : null) }}" maxlength="40" />
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is required.</span>
                              <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Email field is invalid.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="phone_inner">
                              <h5>Phone No</h5>
                              <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone', isset($loungedetails->phone) ? $loungedetails->phone : null) }}"  minlength="8" maxlength="16"/>
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone Number field is required.</span>
                               <span class="control-label Err1" for="inputError"><i class="fa fa-times-circle-o"></i> The Phone Number field is invalid.</span>
                                 <span class="control-label Err2" for="inputError"><i class="fa fa-times-circle-o"></i> PhoneNumber accepts only minimum 8 and maximum 16 number only</span> 
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="landmark_inner">
                              <h5>Landmark</h5>
                              <input type="text" class="form-control" name="landmark" id="landmark" value="{{old('landmark', isset($loungedetails->landmark) ? $loungedetails->landmark : null) }}"  minlength="1" maxlength="30" />
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Landmark field is required.</span>
                           </div>
                           <div class="col-sm-6 input_field_sections" id="status_inner">
                              <h5>Status</h5>
                              <select class="form-control" id="status" name="status">
                                 <option  value="">Please Select Status</option>
                                 <option value="1" {{ old('status', isset($loungedetails->status) ? $loungedetails->status : '') == "1" ? 'selected' : '' }}>Active</option>
                                  <option value="0" {{ old('status', isset($loungedetails->status) ? $loungedetails->status : '') == "0" ? 'selected' : '' }}>Inactive</option>
                              </select> 
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Status field is required.</span>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-6 input_field_sections" id="facility_inner">
                              <h5>Facilities</h5>
                              <select class="form-control" name="facilities[]" id="facilities" multiple="1">
                                 <option value="">Select Facilities</option>
                                 @foreach($facility as $key=>$val)
                                 <option value="{{$key}}">{{$val}}</option>
                                 @endforeach
                              </select>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Facility field is required.</span>
                           </div>
                        </div>

                        <div class="row">
                           <div class="col-sm-12 input_field_sections" id="conditions_inner">
                              <h5>Conditions</h5>
                              <textarea class="form-control" rows="5" name="conditions">{{old('conditions', isset($loungedetails->conditions) ? $loungedetails->conditions : null) }}</textarea>
                               <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i> The Condition field is required.</span>
                           </div>
                        </div> 
                        <div class="row" id="timeline_mainimage">
                           <div class="col-sm-12 input_field_sections" id="timeline_mainimage_inner">
                              <h5>Lounge Images</h5>
                               @if (($main_image) != '')
                                <input id="timeline_images" type="file"  class="MediaFilesPath" id="MediaFilesPath" name="MediaFilesPath[]" multiple="1" /> 
                                <input type="hidden" name="hidden_image" id="hidden_image" value="{{$main_image}}">
                                <textarea name="deleted_hidden_image" id="deleted_hidden_image" value="" style="display:none"></textarea>

                                @else
                                <input id="timeline_images" type="file" class="MediaFilesPath" name="MediaFilesPath[]" multiple="1" />
                                <input type="hidden" name="hidden_image" id="hidden_image" value="">
                               <textarea name="deleted_hidden_image" id="deleted_hidden_image" value="" style="display:none"></textarea>
                                @endif 
                              <span class="control-label Err" for="inputError"><i class="fa fa-times-circle-o"></i>Please provide valid image.</span>
                           </div>
                        </div>
                        <div id="testing" style="display:none;">  </div>
                           <input type="hidden" id="hiddencount" name="hiddencount" value="1">
                     </div>
                  </div>   
               </div>
            </div>
            <!-- /.row -->
            <div class=" m-t-35">
               <div class="form-actions form-group row">
                  <div class="col-xl-12 text-center">
                     <input type="hidden" name="id" id="pid" value="{{$id}}">
                     <input type="hidden" name="_token" value="{{csrf_token()}}">
                     <input type="button" class="btn btn-primary" value="Submit" onclick="return validation();">
                     <input type="button" class="btn btn-default" value="Cancel" id="cancelform">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- /#content -->
</div>
<!-- startsec End -->


@include('footer')   



<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.4.7/js/fileinput.js" type="text/javascript"></script>

<script type="text/javascript">
   $('#airportid').change(function(){
      var airportID = $(this).val();    
      if(airportID){
         $.ajax({
            type:"GET",
            url:"{{url('get_terminal')}}?airportid="+airportID,
            success:function(res){   
               if(res){
                  $("#terminalid").empty();
                  $("#terminalid").append('<option value="">Please Select Terminal</option>');
                  $.each(res,function(key,value){
                      $("#terminalid").append('<option value="'+key+'">'+value+'</option>');
                  });
               
               }else{
                 $("#terminalid").empty();
                 $("#terminalid").append('<option value="">Please Select Terminal</option>');
               }
            }
         });
      }else{
          $("#terminalid").empty();
         $("#terminalid").append('<option value="">Please Select Terminal</option>');
      }      
   });

   $(document).ready(function(){
     $("#facilities").select2({"allowClear":true,"placeholder":"Please select facility"}); 
   });
</script>

  <script>
    $(function () {
                
        @if(count($image_array)>0)

          $("#timeline_images").fileinput({
            "initialPreview":[@foreach($image_array as $k=>$v)"{{ url('/') }}/uploads/lounge/{{$v}}",@endforeach],
            "initialPreviewConfig":[@foreach($image_array as $k=>$v)
            {
               "caption":"{{$v}}",
               "url":"{{ url('/') }}/lounge/deleteimage",
               "key":"{{$k}}"
            },
            @endforeach],
            "overwriteInitial":false,
            "initialPreviewAsData":true,
            "browseLabel":"Browse",
            "showRemove":false,
            "showUpload":false,
            "uploadUrl":"#" , 
            "fileActionSettings": {
              "showUpload": false,
              },
             "initialPreviewDelete":true, 
             "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'flv', 'wmv', 'mp4', 'avi', 'mov', 'mp3'],
            "deleteExtraData":{"MediaFilesPath[multiple_image]":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"GET"},
            "deleteUrl":"#",
            dropZoneEnabled: false,
          });

        @else

          $("#timeline_images").fileinput({
            "overwriteInitial":true,
            "initialPreviewAsData":true,
            "browseLabel":"Browse",
            "showRemove":false,
            "showUpload":false,
            "uploadUrl":"#" , 
            "fileActionSettings": {
              "showUpload": false,
              },
            "allowedFileExtensions": ['jpeg', 'jpg', 'png', 'gif'],
            "deleteExtraData":{"MediaFilesPath":"_file_del_","_file_del_":"","_token":"{{ csrf_token() }}","_method":"PUT"},

            "uploadExtraData": function(previewId, index) {
              return {key: index};
            },
            "deleteUrl":"#",
            dropZoneEnabled: false,
          });

        @endif   
    });

    $(document).ready(function(){
        $('.MediaFilesPath').change(function(e){ 
             var hdcnt = $('#hiddencount').val();
             var $this = $(this), $clone = $this.clone(true);
             if(hdcnt == 1)
             {
               $this.after($clone).prop('id', 'MediaFilesPathnew1');
               $this.after($clone).prop('class', 'MediaFilesPathnew1');
               $this.after($clone).prop('name', 'MediaFilesPathnew1[]');
               $this.after($clone).prop('readonly', 'readonly');
               $this.after($clone).appendTo('#testing');
               hdcnt++;
               $('#hiddencount').val(hdcnt);
             }
        });

          $('#timeline_images').change(function(e){ 
            var i;
            var resultset = $('#hidden_image').val();
            for (i = 0; i < e.target.files.length; ++i) {
              var fileName = e.target.files[i].name;
              if(resultset!="")
              resultset = resultset+","+fileName;
              else
              resultset = fileName;
              var formData = new FormData();
              formData.append('#timeline_images', fileName, "Test");
            }
            
            //$('#hidden_image').val(resultset);
             var hdcnt = $('#hiddencount').val();
             var $this = $(this), $clone = $this.clone(true);

             $this.after($clone).prop('id', 'MediaFilesPathnew'+hdcnt);
             $this.after($clone).prop('class', 'MediaFilesPathnew'+hdcnt);
             $this.after($clone).prop('name', 'MediaFilesPathnew'+hdcnt+'[]');
             $this.after($clone).prop('readonly', 'readonly');
             $this.after($clone).appendTo('#testing');
             hdcnt++;

           
            $('#hiddencount').val(hdcnt);
        });


   $('body').on('click', '.kv-file-remove', function(e) {
          var value = $(this).parent().parent().parent().children().prop('title');
          var list = $("#deleted_hidden_image").val();

          if(list!="")
            list = list+","+value;
          else
            list = value;

          $("#deleted_hidden_image").html(list);
          return false;
        });
    });

   $("#cancelform").click(function() {
       window.location.href = "{{url('/lounge')}}";
   }); 

   </script>

@if (isset($selectedfacility))

<script type="text/javascript">
$(function () {
    $("#facilities").val({{$selectedfacility}}).select2({"allowClear":true,"placeholder":"Please select Facility"});
  });
</script>
 @endif

 <script type="text/javascript">
   
function validation()
{
   $(':input[type="button"]').prop('disabled', true);
   var loungename = $("#name").val();
   var loungeid = $("#loungeid").val();
   var airport = $("#airportid").val();
   var terminal = $("#terminalid").val();
   var email = $("#email").val();
   var phone = $("#phone").val();
   var landmark = $("#landmark").val();
   var status = $("#status").val();
   var facilities = $("#facilities").val();
   var conditions = $("#conditions").val();

   var emailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
   var phoneregex = /^[0-9\+]{1,}[0-9\-]{3,15}$/;
   var nameregex = /^[A-Za-z0-9 _]*$/;

   var err='';
   var checkeror = 0;
   $('.Err').hide();
   $('.Err1').hide();
   $('.Err2').hide();

   if((loungename=='' || loungename==null))
   {
     $('#name').addClass('has-error');
     $('#name_inner .Err').show();
     if(err=='')
     {
         $('#name').focus();
         err='set';
     }
   }

   if((loungeid=='' || loungeid==null))
   {
        $('#loungeid').addClass('has-error');
        $('#loungeid_inner .Err').show();
        if(err=='')
        {
            $('#loungeid').focus();
            err='set';
        }
   }
   
   if(airport=='' || airport==null)
   {
        $('#airportid').addClass('has-error');
        $('#airportid_inner .Err').show();
        if(err=='')
        {
            $('#airportid').focus();
            err='set';
        }
   }  

   if(terminal=='' || terminal==null)
   {
        $('#terminalid').addClass('has-error');
        $('#terminalid_inner .Err').show();
        if(err=='')
        {
            $('#terminalid').focus();
            err='set';
        }
   } 

   if(email=='' || email==null)
   {
        $('#email').addClass('has-error');
        $('#email_inner .Err').show();
        if(err=='')
        {
            $('#email').focus();
            err='set';
        }
   }

    if(email!="" && !emailregex.test(email))
    {
        $('#email').addClass('has-error');
        $('#email_inner .Err1').show();
        if(err=='')
        {
            $('#email').focus();
            err='set';
        }
    } 

   if(phone=='' || phone==null)
   {
        $('#phone').addClass('has-error');
        $('#phone_inner .Err').show();
        if(err=='')
        {
            $('#phone').focus();
            err='set';
        }
   }

   if(phone!="" && isNaN(phone))
   {
        $('#phone').addClass('has-error');
        $('#phone_inner .Err1').show();
        if(err=='')
        {
            $('#phone').focus();
            err='set';
        }
   } 
     if(!isNaN(phone) &&  phone!="")
      {
        if(phone.length>16 || phone.length<8)
        {
              $('#phone').addClass('has-error');
            $('#phone_inner .Err2').show();
            if(err=='')
            {
                $('#phone').focus();
                err='set';
            }
        }
      }
   if(landmark=='' || landmark==null)
   {
        $('#landmark').addClass('has-error');
        $('#landmark_inner .Err').show();
        if(err=='')
        {
            $('#landmark').focus();
            err='set';
        }
   }  

   if(facilities=='' || facilities==null)
   {
        $('#facilities').addClass('has-error');
        $('#facility_inner .Err').show();
        if(err=='')
        {
            $('#facilities').focus();
            err='set';
        }
   }  

 /*  if(conditions=='' || conditions==null)
   {
        $('#conditions').addClass('has-error');
        $('#conditions_inner .Err').show();
        if(err=='')
        {
            $('#conditions').focus();
            err='set';
        }
   }*/ 

   if(status=='' || status==null)
   {
        $('#status').addClass('has-error');
        $('#status_inner .Err').show();
        if(err=='')
        {
            $('#status').focus();
            err='set';
        }
   }
   if ( $(".file-upload-indicator .glyphicon").hasClass("glyphicon-exclamation-sign") ) 
   {
     $('#timeline_images').addClass('has-error');
     $('#timeline_mainimage_inner .Err').show();
     if(err=='')
     {
         $('#timeline_images').focus();
         err='set';
     }
   }

   if(err!='')
   {
     $(':input[type="button"]').prop('disabled', false);
     return false;
   }
   else
   {
      checkunique();
     
   }
}

  function checkunique() {
    $.ajax({
      type : "POST",
      url : "{{ url('/') }}/lounge/loungeexist",
      data : $('#myforms').serialize(),
      beforeSend : function() {
      },
      success : function(data) { 
        if(data == "failed")
        {
            $(':input[type="button"]').prop('disabled', false);
            $('#loungeid').addClass('has-error');
            $('#loungeid_inner .Err1').show();
            $('#loungeid').focus();
            return false;
        }
        else
            $('#myforms').submit();
        },
    });
  }

 </script>

 