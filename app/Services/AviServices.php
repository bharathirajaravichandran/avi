<?php 
namespace App\Services; 
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;


class AviServices {

   /* error_log(print_r($request->all(),true),3,'/var/www/html/error.log');*/

  /**
   * [getAuthUser description]
   * @param  [type] $token [description]
   * @return [type]        [description]
   */
  public function getAuthUser($token){
    $user = JWTAuth::toUser($token); 
    return $user;
  }

  /**
   * Refresh the JWT token using the expired one
   * @param  Request $request [description]
   * @return [type]           [description] 
   */
  public function refreshToken(Request $request) {
    $token = $request->token;
    try {
      if (! $user = JWTAuth::parseToken()->authenticate()) {
        return response()->json([config('resource.user_not_found')],config('resource.status_404'));
      }
    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e){
      $refreshedToken = JWTAuth::refresh($token);
      $token = $refreshedToken;
      return response()->json(compact('token'));
    }
    return response()->json([config('resource.user_not_found')],config('resource.status_404'));
  }

  /**
   * this function used to get the user id from the Token
   * @param  [type] $token [description]
   * @return [type]        [description]
   */
  private function getUserId($token) {
    $user = $this->getAuthUser($token);   
  }


  public function forgotPassword(Request $request) {
    $validator = Validator::make($request->all(), [                       
            'email'                => 'required|email',                              
        ]);
        if ($validator->fails()){              
            return response()->json(['status' => 500, 'message' => 'parameter missing & invalid email'], 200);
        }  
        $user = User::where('email',$request->input('email'))->first();
        if(!$user){
           return response()->json(['status' => 500, 'message' => 'email not exists'], 200);
        }
        $newPass = $this->randomPassword();
        $data = array('name'=>$user->first_name, 'email'=>$user->email, 'password'=>$newPass);
            $mail_send = Mail::send('apisendpwmail', $data, function($message) use($user) {
                 $message->to($user->email, $user->first_name)->subject('Avi - ForgotPassword');
                 $message->from(config('mail.username'),'Admin');
            });
        if (Mail::failures()){
              return response()->json(['status' => 500, 'message' => 'Email Not send'], 200);
            }         
        $user->password = bcrypt($newPass);
        $user->save();
        return response()->json(['status' => 200, 'message' => 'Password sent to your email'], 200);          
  }


  public function register(Request $request){
        $validator = Validator::make($request->all(), [            
            'first_name'           => 'required',            
            'email'                => 'required|email',
            'mobile'               => 'required',
            'password'             => 'min:3|required_with:confirmpassword|same:confirmpassword',
            'confirmpassword'      => 'min:3'                 
        ]);
        if ($validator->fails()){              
            return response()->json(['status' => 500, 'message' => 'parameter missing & password missing'], 200);
        }  

        $EmailPhoneCheck = User::where('email',$request->input('email'))->orWhere('mobile',$request->input('mobile'))->count();
        if($EmailPhoneCheck != 0)
             return response()->json(['status' => 500, 'message' => 'email or mobile number already exists'], 200);
        
         //save data
         $user = new User();
         $exclude_inputs = array('password','confirmpassword');
         foreach($request->all() as $key=>$value){
              if(!in_array($key,$exclude_inputs))
                  $user->$key = $value;
          }           
         if($request->has('password'))
         { 
           $user->password = bcrypt($request->input('password'));
         }                           
         $user->user_type_id = 1 ;
         $user->user_type    = 1 ;
         $user->created_by   = 1 ;
         $user->status       = 1 ;
         $user->save();
         //save data       
        $token = JWTAuth::fromUser($user);         
        return response()->json(['status' => 200,'message' => 'User logged in sucessfully.','data' => $this->array_null_values_replace(User::find($user->id)->toArray()),'jwt_token'=>$token])->header('jwt_token', $token);
      
  }

  public function login(Request $request){
        $validator = Validator::make($request->all(), [            
            'email'           => 'required',
            'password'      => 'required',                    
        ]);
        if ($validator->fails()){              
            return response()->json(['status' => 500, 'message' => 'parameter missing'], 200);
        }  
        $data = User::where('mobile',$request->input('email'))->orWhere('email',$request->input('email'))
                    ->where('password',$request->input('password'))->first();

        $credentials = [];
        if (filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
            $credentials['email']             = trim($request->email);
            $credentials['password']          = $request->password;
        } else {
            $credentials['mobile']            = trim($request->email);
            $credentials['password']          = $request->password;
        }
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['status' => 500, 'message' => 'Invalid credentials']);
            }
        } catch (JWTAuthException $e) {
            return response()->json(['status' => 500, 'message' => 'could not create token']);
        }
        $currentUser = Auth::user();
        return response()->json(['status' => 200,'message' => 'User logged in sucessfully.','data' => $this->array_null_values_replace($currentUser->toArray()),'jwt_token'=>$token])->header('jwt_token', $token);
      
  }

 public function array_null_values_replace($result)
    {
          if(is_array($result))
          { 
            array_walk_recursive($result, function (&$item, $key)
             {  
               $item = null === $item ? '' : $item;
             });
            return $result;
          }                 
          else
           {  
                $r['Status']  = 'Failed';
                $r['message'] = "Failed";
                return response()->json($r);
           }
    }

 public function randomPassword()
  {
    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 8; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
  }

 public function test()
  {
    dd('testing');
  }
  
}