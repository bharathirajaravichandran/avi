<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Airportterminals;
use App\Models\Restaurant;
use App\Models\Airports;
use App\Models\Retail_Shop;
use App\Models\Retail_type;
use App\Models\Retailtime;
use App\Models\Users;
use Auth;

class RetailshopController extends Controller
{
    public function list()
    {

          $userid = Auth::user()->id;
        if($userid!="")
        {
           $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                  if($userdetails->user_type_id ==1)
                    $rest=Retail_Shop::where('status','!=',2)->orderby('id','DESC')->get();
                  if($userdetails->user_type_id ==2)
                    $rest =Retail_Shop::where('status','!=',2)->where('created_by',$userid)->orderBy('id','DESC')->get();

                  $airports=Airports::where('status',1)->pluck('name','id');
                  return view('retailshop.list',compact('rest','airports'));
            }
             else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>User List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }

        }
        else
        return redirect('/login');
    }
     public function create()
    {
    	$airports=Airports::where('status',1)->pluck('name','id');
    	$retail_types=Retail_type::where('status',1)->pluck('name','id');
    	$pid="";
     
    	return view('retailshop.create',compact('pid','airports','retail_types'));
    }
    public function saveretail(Request $request)
    {
    	//dd($request->all());

        $id=$request->pid;

        if($id==0 || $id==null)
        $ret=new Retail_Shop;

        else
        $ret=Retail_Shop::where('id',$id)->where('status','!=',2)->first();

        $ret->airport_id=$request->airports;
        $ret->airport_terminal_id=$request->terminal;
        $ret->retail_type_id=$request->type;
        $ret->shop_no=$request->shop_no;
        $ret->name=$request->name;
        $ret->phone=$request->phone;
        $ret->email=$request->email;
        $ret->landmark=$request->landmark;
        $ret->address=$request->address;
        $ret->status=$request->status;
        $ret->created_by=Auth::user()->id;
        if($ret->save()){
          if($id!="")
            Retailtime::where('shop_id',$ret->id)->delete();

            if($request->sun_from!="" && $request->sun_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'sunday'; 
              $days->from_time = $request->sun_from; 
              $days->to_time = $request->sun_to; 
              $days->save(); 
            }
            if($request->mon_from!="" && $request->mon_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'monday'; 
              $days->from_time = $request->mon_from; 
              $days->to_time = $request->mon_to; 
              $days->save(); 
            }   
            if($request->tue_from!="" && $request->tue_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'tuesday'; 
              $days->from_time = $request->tue_from; 
              $days->to_time = $request->tue_to; 
              $days->save(); 
            }   
            if($request->wed_from!="" && $request->wed_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'Wednesday'; 
              $days->from_time = $request->wed_from; 
              $days->to_time = $request->wed_to; 
              $days->save(); 
            }   
            if($request->thu_from!="" && $request->thu_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'Thursday'; 
              $days->from_time = $request->mon_from; 
              $days->to_time = $request->mon_to; 
              $days->save(); 
            }   
            if($request->fr_from!="" && $request->fr_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'Friday'; 
              $days->from_time = $request->fr_from; 
              $days->to_time = $request->fr_to; 
              $days->save(); 
            }  
            if($request->sat_from!="" && $request->sat_to!=""){
              $days = new Retailtime; 
              $days->shop_id = $ret->id; 
              $days->day = 'saturday'; 
              $days->from_time = $request->sat_from; 
              $days->to_time = $request->sat_to; 
              $days->save(); 
            }
        }
        if($id=="")
         return redirect('/retailshop')->witherrors('Shop Added successfully');
         else
         return redirect('/retailshop')->witherrors('Shop Updated successfully');
    }
       public function edit($id)
    {
        //dd($id);
        $rest=Retail_Shop::find($id);
  	$airports=Airports::where('status',1)->pluck('name','id');
    	$retail_types=Retail_type::where('status',1)->pluck('name','id');
        $pid=$id;
        $airportterminals =DB::table('terminals')->where('status',1)->pluck('name','id');
        $days =Retailtime::where('shop_id',$id)->get();
        if($days->count()>0){
        $array=array();
          foreach($days as $d){
            if($d->day=='sunday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='monday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='tuesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Wednesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Thursday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Friday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='saturday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } 
          }
        }
        //dd($array['sunday']['from']);
        return view('retailshop.create',compact('pid','airports','retail_types','rest','airportterminals','days','array'));
    }
     public function retaildestroy($id)
    {
        $rest= Retail_Shop::find($id);
        $rest->status=2;
        $rest->save();
       Retailtime::where('shop_id',$id)->delete();
          return redirect('/retailshop')->with('message','Shop Deleted Successfully');

    }
     public function view($id)
    {
         $rest= Retail_Shop::with('airports','retailtype','terminal')->find($id);
           $days =Retailtime::where('shop_id',$id)->get();
           $id=$id;
        if($days->count()>0){
        $array=array();
          foreach($days as $d){
            if($d->day=='sunday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='monday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='tuesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Wednesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Thursday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Friday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='saturday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } 
          }

    }
    else
      $array="";
    
      return view('retailshop.view',compact('rest','workdays','array','id'));

  }
  public function filter_list($id)
  {
     $userid = Auth::user()->id;
        if($userid!="")
        {
           $userdetails = Users::where('id',$userid)->first();
           $shops = Airports::where('id',$id)->where('status','!=',2)->first();
           
            if(is_object($shops))
            {
                  if($userdetails->user_type_id ==1)
                    $rest=Retail_Shop::where('airport_id',$id)->where('status','!=',2)->orderby('id','DESC')->get();
                  if($userdetails->user_type_id ==2)
                    $rest =Retail_Shop::where('airport_id',$id)->where('status','!=',2)->where('created_by',$userid)->orderBy('id','DESC')->get();

                  $airports=Airports::where('status',1)->pluck('name','id');
                  $airport_id=$id;
                  return view('retailshop.list',compact('rest','airports','airport_id'));
            }
             else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Shop List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }

        }
        else
        return redirect('/login');

  }
   public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Retail_Shop::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
    
}
