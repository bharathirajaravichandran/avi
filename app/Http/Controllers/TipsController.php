<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tips;
use App\Models\Airports;
use Auth;

class TipsController extends Controller
{
     public function list()
    {
    	$tips=Tips::where('status','!=',2)->with('airport')->get();
    	return view('tips.list', compact('tips'));
    }
      public function create()
    {
    	$pid="";
       $airports=Airports::where('status',1)->get();
    	return view('tips.create', compact('pid','airports'));
    }
     public function savetips(Request $request)
    {
    	//dd($request->all());
    	$id=$request->pid;
    	if($id=="")
    	 $tips = new Tips;
    	else
    	  $tips =Tips::find($id);
    	$tips->airport_id=$request->airport_id;
    	$tips->description=$request->description;
      $tips->status=$request->status;
    	$tips->created_by=Auth::user()->id;

    	
         $tips->save();
         if($id=="")
         return redirect('/tips')->witherrors('Tips Added successfully');
         else
         return redirect('/tips')->witherrors('Tips Updated successfully');
    }
    public function view($id)
    {
    	 $tips=Tips::where('id',$id)->with('airport')->first();
       $id=$id;
       
    	return view('tips.view', compact('tips','id'));
    }
      public function edit($id)
    {
    	 $tips=Tips::find($id);
    	 $pid=$id;
    	  $airports=Airports::where('status',1)->get();
    	return view('tips.create', compact('pid','airports','tips'));
    }
    public function tipsdestory($id)
    {
    	    	$tips=Tips::find($id);
    	$tips->status=2;
    	   $tips->save();
            return redirect()->back()->with('message','Tips Deleted successfully');

    }
    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Tips::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }   
  
}
