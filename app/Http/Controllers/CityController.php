<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\States;
use App\Models\City;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class CityController extends Controller
{
     public function index()
    {
        $city =City::with('country','state')->where('status','!=',2)->get();     
        return view('city.list_city', compact('city'));
    }

    public function listing(Request $request)
    {
        $data=array();

        if($request->order[0]['column'] == 0)
        {
          $ordervalue = "cnt.name";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->order[0]['column'] == 1)
        {
          $ordervalue = "states.name";
          $ordertype = $request->order[0]['dir'];
        }

         if($request->order[0]['column'] == 2)
        {
          $ordervalue = "cities.name";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->order[0]['column'] == 3)
        {
          $ordervalue = "cities.status";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->search['value']=="")
        {
          $totalcity =DB::table('cities')->select('cities.id','cities.name as cityname','cities.status as citystatus','states.name as statename','cnt.name as countryname')->join('states','cities.state_id','=','states.id')->join('countries as cnt','states.country_id','=','cnt.id')->where('cities.status','!=',2)->count() ;

          $city =DB::table('cities')->select('cities.id','cities.name as cityname','cities.status as citystatus','states.name as statename','cnt.name as countryname')->join('states','cities.state_id','=','states.id')->join('countries as cnt','states.country_id','=','cnt.id')->where('cities.status','!=',2)->orderBy($ordervalue,$ordertype)->limit($request->length)->offset($request->start)->get() ;
        }
        else
        {
          $searchval = $request->search['value'];

          $totalcitylist  = DB::table('cities')->select('cities.id','cities.name as cityname','cities.status as citystatus','states.name as statename','cnt.name as countryname')->join('states','cities.state_id','=','states.id')->join('countries as cnt','states.country_id','=','cnt.id')->where('cities.status','!=',2);
          $totalcity = $totalcitylist->where(function ($query) use ($searchval) {
            $query->orWhere('states.name', 'like', '%' . $searchval . '%')
            ->orWhere('cnt.name', 'like', '%' . $searchval . '%')
            ->orWhere('cities.name', 'like', '%' . $searchval . '%');
          })->count();

          
          $citylist =DB::table('cities')->select('cities.id','cities.name as cityname','cities.status as citystatus','states.name as statename','cnt.name as countryname')->join('states','cities.state_id','=','states.id')->join('countries as cnt','states.country_id','=','cnt.id')->where('cities.status','!=',2);

          $city = $citylist->where(function ($query) use ($searchval) {
          $query->orWhere('states.name', 'like', '%' . $searchval . '%')
            ->orWhere('cnt.name', 'like', '%' . $searchval . '%')
            ->orWhere('cities.name', 'like', '%' . $searchval . '%');
          })->orderBy($ordervalue,$ordertype)->limit(10)->offset(0)->get();

        }

        foreach($city as $citydata)
        {
            if($citydata->citystatus ==1)
            $set = "checked";
            else
            $set = "";
            $resultarray = array();
            $resultarray[] = $citydata->countryname;
            $resultarray[] = $citydata->statename;
            $resultarray[] = $citydata->cityname;
            $resultarray[] = '<input type="checkbox" data-toggle="toggle" data-on="Active" data-off="In Active" data-onstyle="success" data-offstyle="danger" data-size="mini" class="statuschange" id="'.$citydata->id.'"'.$set.'>';
            $resultarray[] = '<a data-toggle="tooltip" data-placement="top" title="Edit" href="'.url('/').'/cityedit/'.$citydata->id.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                            <a data-toggle="tooltip" data-placement="top" title="View" href="'.url('/').'/cityview/'.$citydata->id.'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i>
                            </a>
                            <a href="#" class="btn btn-danger btn-xs waves-effect waves-light remove-record" data-toggle="modal" data-url="'.url('/').'/citydestroy/'.$citydata->id.'" data-id="'.$citydata->id.'" data-target="&#35;custom-width-modal" title="Delete" data-placement="top"><i class="fa fa-trash-o remove-record"></i></a>';
            $data[] = $resultarray;
        }

        $json_data = array(
                "draw"            => intval( $request->draw),
                "recordsTotal"    => intval( $totalcity ),
                "recordsFiltered" => intval( $totalcity ),
                "data"            => $data
            );

        echo json_encode($json_data);
    }

    public function create()
    {
    	$country=Country::where('status','!=',2)->pluck('name','id');
    	 $pid="";

        return view('city.create',compact('pid','country','state'));
    }
    public function getstatelist(Request $request)
    {
        $ParentID = $request->get('q');
        //dd($ParentID);
        $statelist =States::where('country_id', $ParentID)->where('status',1)->pluck('name', 'id');
        $result = "<option value=''>Please select State</option>";
        foreach($statelist as $k=>$v)
        {
            $result .="<option value='".$k."'>".$v."</option>";
        }
     
        return $result;
    } 

    public function savecity(Request $request)
    {
    	  $c_name = strtolower($request->name);
            $name=ucfirst($c_name);
            $id=$request->pid;
            //dd($id);
            if($id=="")
            $city=new City;
            else
            $city=City::find($id);
            

            $city->country_id=$request->Country;
            $city->state_id=$request->State;
            $city->name=$name;
            $city->status=$request->status;
            $city->save();
  /*          dd($countrynew);*/
            //Country::create($data);
             if($id=="")
            return redirect('/city')
                             ->witherrors('City Added successfully');
              else
            return redirect('/city')
                             ->witherrors('City Updated successfully');

    }
    public function edit($id)
    {

    	  $city=City::find($id);
    	  $country=Country::where('status','!=',2)->pluck('name','id');
    	  $states=States::where('status','!=',2)->pluck('name','id');
    	  $pid=$id;

    	  return view('city.create',compact('pid','country','states','city'));
    }
     public function delete($id)
    {
          
         $city= City::find($id);
         $city->status=2;
            $city->save();
             //dd($country);
            return redirect()->back()->with('message','City Deleted successfully');
        
    }
    public function view($id)
    {
    	   $city =City::with('state','state.country')->find($id);
         $id=$id;
    	   return view('city.view_city', compact('city','id'));
    }
     public function statusupdate(Request $request)
    {

        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = City::where('id',$id)->first();
        //dd($request->id);
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
