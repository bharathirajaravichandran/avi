<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\States;
use App\Models\Country;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class StateController extends Controller
{
   public function index()
    {
        $states =States::with('country')->where('status','!=',2)->get() ; 
        //dd($states[0]->country->name);      
        return view('state.list_state', compact('states'));
    }

    public function listing(Request $request)
    {
        $data=array();

        if($request->order[0]['column'] == 0)
        {
          $ordervalue = "cnt.name";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->order[0]['column'] == 1)
        {
          $ordervalue = "states.name";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->search['value']=="")
        {
          $totalstate =DB::table('states')->select('states.id','states.name as statename','states.status','cnt.name as countryname')->join('countries as cnt','states.country_id','=','cnt.id')->where('states.status','!=',2)->count() ;
          $state =DB::table('states')->select('states.id','states.name as statename','states.status','cnt.name as countryname')->join('countries as cnt','states.country_id','=','cnt.id')->where('states.status','!=',2)->orderBy($ordervalue,$ordertype)->limit($request->length)->offset($request->start)->get() ;
        }
        else
        {
          $searchval = $request->search['value'];

          $totalstatelist  =DB::table('states')->select('states.id','states.name as statename','states.status','cnt.name as countryname')->join('countries as cnt','states.country_id','=','cnt.id')->where('states.status','!=',2);
          $totalstate = $totalstatelist->where(function ($query) use ($searchval) {
            $query->orWhere('states.name', 'like', '%' . $searchval . '%')
            ->orWhere('cnt.name', 'like', '%' . $searchval . '%');
          })->count();

          
          $statelist =DB::table('states')->select('states.id','states.name as statename','states.status','cnt.name as countryname')->join('countries as cnt','states.country_id','=','cnt.id')->where('states.status','!=',2);

          $state = $statelist->where(function ($query) use ($searchval) {
          $query->orWhere('states.name', 'like', '%' . $searchval . '%')
            ->orWhere('cnt.name', 'like', '%' . $searchval . '%');
          })->orderBy($ordervalue,$ordertype)->limit(10)->offset(0)->get();

        }

        foreach($state as $statedata)
        {
            if($statedata->status==1)
            $set = "checked";
            else 
            $set = "";
            $resultarray = array();
            $resultarray[] = $statedata->countryname;
            $resultarray[] = $statedata->statename;
            $resultarray[] = '<input type="checkbox" data-toggle="toggle" data-on="Active" data-off="In Active" data-onstyle="success" data-offstyle="danger" data-size="mini" class="statuschange" id="'.$statedata->id.'"'.$set.'>';
            $resultarray[] = '<a data-toggle="tooltip" data-placement="top" title="Edit" href="'.url('/').'/stateedit/'.$statedata->id.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                            <a data-toggle="tooltip" data-placement="top" title="View" href="'.url('/').'/stateview/'.$statedata->id.'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i>
                            </a>
                            <a href="#" class="btn btn-danger btn-xs waves-effect waves-light remove-record" data-toggle="modal" data-url="'.url('/').'/statedestroy/'.$statedata->id.'" data-id="'.$statedata->id.'" data-target="&#35;custom-width-modal" title="Delete" data-placement="top"><i class="fa fa-trash-o remove-record"></i></a>';
            $data[] = $resultarray;
        }

        $json_data = array(
                "draw"            => intval( $request->draw),
                "recordsTotal"    => intval( $totalstate ),
                "recordsFiltered" => intval( $totalstate ),
                "data"            => $data
            );

        echo json_encode($json_data);
    }

     public function create()
    {
    	   $pid="";
    	$country=Country::where('status',1)->pluck('name','id');
     
        return view('state.create',compact('pid','country'));
    }
    public function savestate(Request $request)
    {
    	 $c_name = strtolower($request->name);
            $name=ucfirst($c_name);
      
           
            $id=$request->pid;
            //dd($id);
            if($id=="")
            $state=new States;
            else
            $state=States::find($id);
            

            $state->name=$name;
            $state->country_id=$request->country_id;
            $state->status=$request->status;
            $state->save();
  /*          dd($countrynew);*/
            //Country::create($data);
             if($id=="")
            return redirect('/state')
                             ->witherrors('State Added successfully');
              else
            return redirect('/state')
                             ->witherrors('State Updated successfully');

    }
    public function edit($id)
    {
    	 $country=Country::where('status',1)->pluck('name','id');
        $pid=$id;
        $states =States::find($id);
        //dd($states);
        return view('state.create', compact('country','pid','states'));
    }
      public function delete($id)
    {
         // dd($id);
         $state= States::find($id);
         $state->status=2;
            $state->save();
             //dd($country);
            return redirect('/state')->with('message','State Deleted successfully');
        
    }
    public function view($id)
    {
         $states =States::with('country')->find($id);
         $id=$id;
         return view('state.view_state', compact('states','id'));
    }
    public function statusupdate(Request $request)
    {

        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = States::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
    
}
