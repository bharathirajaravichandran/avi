<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Airports;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Terminals;
use App\Models\Airportterminals;
use App\Models\Userpermissions;
use App\Models\Floormaps;
use App\Models\Vehicle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Contracts\Filesystem\Filesystem;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class AirportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
	        $userdetails= Auth::user();
	        $allowed =0;

	        if($userdetails->user_type_id ==1)
	        $allowed = 1;
	    	if($userdetails->user_type_id ==2)
	    	{
	    		$permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',2)->first();
	    		if(is_object($permissioncheck))
	    		$allowed =1;
	    	}

	        if($allowed == 1)
	        return $next($request);
	    	else
	    	return redirect('/');
    	});
    }

    /*
    * 
    * name: airportlist
    * desc: to list the Airport
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 08.02.2019
    * 
    */
    public function airportlist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                if($userdetails->user_type_id ==1)
                $airportlist = Airports::where('status','!=',2)->orderBy('id','DESC')->get(); 
                if($userdetails->user_type_id ==2)
                $airportlist = Airports::where('status','!=',2)->where('created_by',$userid)->orderBy('id','DESC')->get(); 

                return view('airportlist',['airportlist'=>$airportlist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Airport List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addairport
    * desc: to add the Airports
    * @param: none
    * method: none  
    * @return: return the Airport create form or redirect to falilue page based on condition.
    * Created by JK on 08.02.2019
    * 
    */
    public function addairport()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first(); 
            if(is_object($userdetails))
            {
                $country = Country::where('status','1')->pluck('name','id');
                $terminals = Terminals::where('status','1')->pluck('name','id');
                $vehicle = Vehicle::where('status','1')->pluck('name','id');
                return view('addairport',['id'=>"0",'country'=>$country,'terminals'=>$terminals,'vehicle'=>$vehicle]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Airport Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editairport
    * desc: to edit the Airport Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 08.02.2019
    * 
    */
    public function editairport(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                $airportdetails = Airports::where('id',$id)->where('status','!=',2)->first();
                if(is_object($airportdetails))
                {
                    if($userdetails->user_type_id ==1 || $airportdetails->created_by == $userid)
                    {
                    	$getairportterminal = Airportterminals::where('airport_id',$id)->where('status',1)->get();

                    	$selectedterminal = "[";
                    	if(count($getairportterminal) > 0)
                    	{
                    		foreach($getairportterminal as $getairport_terminal)
                    		{
                    			$selectedterminal .= $getairport_terminal->terminal_id.",";
                    		}
                    	}
                    	$selectedterminal .= "]";
                    	$terminals = Terminals::where('status','1')->pluck('name','id');
                        $country = Country::where('status','1')->pluck('name','id');
                        $states =State::where('country_id',$airportdetails->country_id)->where('status',1)->pluck('name','id');
                        $cities = City::where('state_id',$airportdetails->state_id)->where('status',1)->pluck('name','id');
                        $vehicle = Vehicle::where('status','1')->pluck('name','id');
                        $transport=explode(',',$airportdetails->transport_id);
                       // dd($transport);
                        return view('addairport',['id'=>$id,'airportdetails'=>$airportdetails,'country'=> $country,'states'=>$states,'cities'=>$cities,'terminals'=>$terminals,'selectedterminal'=>$selectedterminal,'vehicle'=>$vehicle,'transport'=>$transport]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Airport failed</h3><p>You dont have credentials to update this airport.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Airport failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Airport failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: saveairport
    * desc: to save the Airport Records
    * @param: airport,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 08.02.2019
    * 
    */
    public function saveairport(Request $request)
    {
       // dd($request->all());
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $airport = new Airports;
                $airport->created_at = Carbon::now()->toDateTimeString();
                $airport->created_by=$loginid;
                $check = 1;
                $message = "Airport Created Successfully";
            }
            else
            {
                $airport = Airports::where('id',$id)->where('status','!=',2)->first();
                if(is_object($airport))
                {
                    $airport->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Airport Updated Successfully";
                }
            }
            if($check==1)
            {
              

                $airport->name=trim($request->name);
                $airport->phone=trim($request->phone);
                $airport->email=trim($request->email);
                $airport->code=trim($request->code);
                $airport->address=trim($request->address);
                $airport->country_id=trim($request->country_id);
                $airport->state_id=trim($request->state_id);
                $airport->city_id=trim($request->city_id);
                $airport->latitude=trim($request->latitude);
                $airport->longitude=trim($request->longitude);
                $transport=$request->transport;
                $airport->transport_id=implode(',',$transport);
                $airport->status=$request->status;
                $airport->save();

                $airportid[] = $airport->id;
                $airportterminals = $request->terminal;

                if($request->id!=0)
                DB::table('airport_terminals')->whereIn('airport_id', $airportid)->delete();
    
                if(count($airportterminals)>0)
                {
                    foreach($airportterminals as $k=>$v)
                    {
                        $airportterminalupdate = new Airportterminals;
                        $airportterminalupdate->terminal_id = $v;
                        $airportterminalupdate->airport_id = $airport->id;
                        $airportterminalupdate->created_at = Carbon::now()->toDateTimeString();
                        $airportterminalupdate->status = 1;
                        $airportterminalupdate->save();
                    }
                }
                return redirect('/airport')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Airport Update failed</h3><p>You dont have credentials to update this airport.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewairport
    * desc: to view the Airport Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 08.02.2019
    * 
    */

    public function viewairport(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $airportdetails = Airports::with(['country','state','city'])->where('status','!=',2)->where('id',$id)->first();
        if(is_object($airportdetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $airportdetails->created_by == $loginid)
            {
            	$airportterminals = Airportterminals::with('terminal')->where('airport_id',$id)->get();
            	if(count($airportterminals))
            	{
            		$airportterminal="";
            		foreach($airportterminals as $airport_terminals)
            		{
            			$airportterminal .= ($airportterminal!="") ? ",".$airport_terminals->terminal->name : $airport_terminals->terminal->name;
            		}
            	}
               
                        $transport=explode(',',$airportdetails->transport_id);
                           $vehicle = Vehicle::whereIn('id',$transport)->pluck('name');

                           $tran_vehicle="";
                           foreach ($vehicle as $k => $v) {
                                if($tran_vehicle!="")
                                    $tran_vehicle=$tran_vehicle.','.$v;
                                else
                                    $tran_vehicle = $v;

                           }
                           $id=$id;

                return view('viewairport',['airportdetails'=>$airportdetails,'airportterminal'=>$airportterminal,'tran_vehicle'=>$tran_vehicle,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Airport failed</h3><p>You dont have credentials to view this compliance type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Airport failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deleteairport
    * desc: to delete the Airport Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 08.02.2019
    * 
    */

    public function deleteairport(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $airportdetails = Airports::where('status','!=',2)->where('id',$id)->first();
            if(is_object($airportdetails))
            {
                if($userdetails->user_type_id ==1 || $airportdetails->created_by == $loginid)
                {
                    $airportdetails = Airports::where('status','!=',2)->where('id',$id)->first();
                    $airportdetails->status = 2;
                    $airportdetails->save();
                    return redirect('/airport')->with('message','Airport Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Airport failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Airport failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: airportexist
    * desc: to check whether the record is already exist or not.
    * @param: airport,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 08.02.2019
    * 
    */

    public function airportexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $airportcode = $request->post('code');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Airports::where('code','=',trim($airportcode))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    } 

    public function getStateList(Request $request)
    {
        $states = State::where("country_id",$request->country_id)->where('status','1')->pluck("name","id");
        return response()->json($states);
    }

    public function getCityList(Request $request)
    {
        $cities =City::where("state_id",$request->state_id)->where('status','1')->pluck("name","id");     
        return response()->json($cities);
    }  

    public function addfloormap(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first(); 
            if(is_object($userdetails))
            {
                $floormap = Floormaps::where('airport_id',$id)->where('status',1)->get();
                $airportdetails = Airports::where('status','!=',2)->where('id',$id)->first();
                if(is_object($airportdetails))
                return view('addfloorplan',['id'=>$id,'airportdetails'=>$airportdetails,'floormap'=>$floormap]);
                else
                return redirect('/airport')->with('message','Invalid Airport Details');
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Airport Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    } 

    public function savefloorplan(Request $request)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first(); 
            if(is_object($userdetails))
            {
                $currentvalue = $request->currentvalue;
                $removedid = explode(",",$request->removedfloor);

                if(count($removedid) > 0)
                DB::table('floormaps')->whereIn('id', $removedid)->delete();

                for($i=1;$i<=$currentvalue;$i++)
                {
                    $files_single = $request->file('profile_img'.$i);
                    $picture_single="";
                     if(count($files_single)>0)
                    {
                        $extension = $files_single->getClientOriginalExtension();
                        $picture_single = date('YmdHis').$this->generateRandomString(4).".".$extension;
                        $destinationPath = base_path() . '/public/uploads/floorplan/';
                        $files_single->move($destinationPath, $picture_single);
                    }

                    $floorplanid = $request->input('existingid'.$i);
                    if(($floorplanid == 0 && $request->input('floormaptitle'.$i)=="" && count($files_single)==0) || in_array($floorplanid,$removedid))
                    {
                    }
                    else
                    {
                        if($floorplanid ==0) 
                        $floormap = new Floormaps;
                        else
                        $floormap = Floormaps::where('id',$floorplanid)->first();

                        $floormap->airport_id=trim($request->airport_id);
                        $floormap->floormap=($picture_single=="" && $floorplanid!=0) ? $floormap->floormap : $picture_single;
                        $floormap->storey=$request->input('floormaptitle'.$i);
                        $floormap->created_at=Carbon::now()->toDateTimeString();
                        $floormap->status=1;
                        $floormap->created_by=$userid;
                        $floormap->save();
                    }
                }
                return redirect('/airport')->witherrors("Floormaps Updated Successfully");
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Airport Floor Map Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    } 

    public function generateRandomString($length=4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function exportFile(Request $request){
        $type="xls";
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first(); 

        $lounge = Airports::with(['country','state','city']);
        if($request->has('startdate') && $request->startdate!=null)
        {
            $startdate = date("Y-m-d",strtotime($request->startdate));
            $lounge->where('created_at','>=',$startdate);
        }
        if($request->has('enddate') && $request->enddate!=null)
        {
            $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
            $lounge->where('created_at','<=',$enddate);
        }
        if($request->has('status'))
        {
            $status = $request->status;
            $lounge->where('status','=',$status);
        }
        if($userdetails->user_type_id ==1)
            $products = $lounge->get();
        if($userdetails->user_type_id ==2)
            $products = $lounge->where('created_by',$userid)->get();

      $i=0;
      if(count($products)>0)
      {
        foreach($products as $product)
        {
          $result[$i]['Name'] = $product->name;
          $result[$i]['Airport ID'] = $product->code;
          $result[$i]['Phone'] = $product->phone;
          $result[$i]['Email'] = $product->email;
          $result[$i]['Address'] = $product->address;
          $result[$i]['Country'] = $product->country->name;
          $result[$i]['State'] = $product->state->name;
          $result[$i]['City'] = $product->city->name;
          $result[$i]['Latitude'] = $product->latitude;
          $result[$i]['Longitude'] = $product->longitude;
          $i++;
        }
      }
      else
      {
        $result[$i]['Name'] = "";
        $result[$i]['Airport ID'] = "";
        $result[$i]['Phone'] = "";
        $result[$i]['Email'] = "";
        $result[$i]['Address'] = "";
        $result[$i]['Country'] = "";
        $result[$i]['State'] = "";
        $result[$i]['City'] = "";
        $result[$i]['Latitude'] = "";
        $result[$i]['Longitude'] = "";
      }
      return \Excel::create('Airportreport', function($excel) use ($result) {
      $excel->sheet('sheet name', function($sheet) use ($result)
      {
          $sheet->fromArray($result);
      });
      })->download($type);
    } 


    public function exportexist(Request $request){
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first(); 
        $lounge = Airports::with(['country','state','city']);
        if($request->has('startdate') && $request->startdate!=null)
        {
            $startdate = date("Y-m-d",strtotime($request->startdate));
            $lounge->where('created_at','>=',$startdate);
        }
        if($request->has('enddate') && $request->enddate!=null)
        {
            $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
            $lounge->where('created_at','<=',$enddate);
        }
        if($request->has('status'))
        {
            $status = $request->status;
            $lounge->where('status','=',$status);
        }
        
        if($userdetails->user_type_id ==1)
            $products = $lounge->get();
        if($userdetails->user_type_id ==2)
            $products = $lounge->where('created_by',$userid)->get();
        $i=0;
        if(count($products)>0)
        return "1";
        else
        return "0";
    }
    public function aupload()
    {
        //dd("gdfg");
        return view('Airportupload');
    }
    public function uploadcsv(Request $request)
    {
        $file = $request->file('csvfile');
        if($file && $file->getClientOriginalExtension() == 'csv'){

                $destinationPath = 'uploads/csv';
                $csvfile = time().'-csv.'.$file->getClientOriginalExtension();
                $file->move($destinationPath,$csvfile);
                $file_content = fopen("uploads/csv/".$csvfile,"r");
                $lined = 1;  //for first line not upload

          while($line = fgetcsv($file_content))      //getting file content
          {
            $rowcount=count($file);
            if($rowcount<=1)
            {
                  unlink($destinationPath.'/'.$csvfile);
                                     return redirect('airport')
                                         ->with('message','File is Empty');

            }
            
            if(!empty($line[0]) && !empty($line[1]) &&  !empty($line[2]) && !empty($line[3]) && !empty($line[4]) && !empty($line[5]) && !empty($line[6])  )
              {
                $linecount=count($line);
                if($linecount==7)
                {    
                        $length1=strlen($line[0]);       //getting the column value size
                        $length2=strlen($line[1]);
                        $length3=strlen($line[2]);
                        $length4=strlen($line[3]);
                        $length5=strlen($line[5]);
                        $length6=strlen($line[6]);
                        if($lined==1)
                            {
                                $name='Name';
                                $phone='Phone';
                                $email='Email';
                                $airportid='Airportid';
                                $address='Address';
                                $latitude='Latitude';
                                $longitude='Longitude';
                                if($name!=ucfirst(strtolower($line[0])) || $phone!=ucfirst(strtolower($line[1])) || $email!=ucfirst(strtolower($line[2])) || $airportid!=ucfirst(strtolower($line[3]))|| $address!=ucfirst(strtolower($line[4]))|| $latitude!=ucfirst(strtolower($line[5]))|| $longitude!=ucfirst(strtolower($line[6])))
                                {
                                     unlink($destinationPath.'/'.$csvfile);
                                     return redirect('airport')
                                         ->with('message','Please upload valid CSV file');
                                }
                            }
                }
                else
                {
                    unlink($destinationPath.'/'.$csvfile);
                    return redirect('airport')->with('message','Please upload valid CSV file');
                }
 
              
                if($lined!=1)
                {

                    if($length1<=255 && $length2<=15 &&$length2>=8 && $length3<=200 && $length4<=20)           //length of column value should be less or equal 255
                    {
                        $airport=Airports::where('status','!=',2)->where('code',$line[3])->count();
                        if($airport==0)
                        {
                            $airports= new Airports;
                            $airports->name =$line[0];
                            $airports->phone =$line[1];
                            $airports->email =$line[2];
                            $airports->code =$line[3];
                            $airports->address =$line[4];
                            $airports->latitude =$line[5];
                            $airports->longitude =$line[6];
                            $airports->status =0;
                            $airports->created_by=Auth::user()->id;
                            $airports->save();
                        }
                         else
                        {
                            $data[]=$lined;
                        }

                    }
                     else
                    {
                     $data[]=$lined;
                    }

                }

              }

              else
              {
                 $data[]=$lined;
              }

                $lined++;
          }
            fclose($file_content);
            unlink($destinationPath.'/'.$csvfile);
            if(empty($data))
                return redirect('airport')->withErrors('Airports Uploaded successfully');
            else
            {
                $datas=implode(',',$data);
                return redirect('airport')->with('message','Lines'.' '.$datas.' '.'are not uploaded');
            }

        }
        else
        {
         return redirect('airport')->with('message','Please upload CSV file');
        }
        
    }
    public function download($file_name) {
        $file_path = 'uploads/format/'.$file_name;
        return response()->download($file_path);
    }

    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $airportcheck = Airports::where('id',$id)->first();
        if(is_object($airportcheck))
        {
            $airportcheck->status = $status;
            $airportcheck->save();
            return "success";
        }
        else
        return "failed";
    }
}
