<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Models\Airportterminals;
use App\Models\Restaurant;
use App\Models\Airports;
use App\Models\Servicetype;
use App\Models\Rest_time;
use App\Models\Users;
use Auth;
use App\Models\Userpermissions;

class RestaurantController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
          $userdetails= Auth::user();
          $allowed =0;

          if($userdetails->user_type_id ==1)
          $allowed = 1;
        if($userdetails->user_type_id ==2)
        {
          $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',3)->first();
          if(is_object($permissioncheck))
          $allowed =1;
        }

          if($allowed == 1)
          return $next($request);
        else
        return redirect('/');
      });
    }

    public function list()
    {
          if(Auth::user()->user_type_id ==1)
            $rest=Restaurant::with('airports')->where('status','!=',2)->orderby('id','DESC')->get();
          if(Auth::user()->user_type_id ==2)
            $rest =Restaurant::with('airports')->where('status','!=',2)->where('created_by',$userid)->orderBy('id','DESC')->get();
        //  dd($rest);
            $airports=Airports::where('status',1)->pluck('name','id');
              return view('restaurant.list',compact('rest','airports'));

    }
    public function add_restaurant()
    {
    	$airports=DB::table('airports')->where('status',1)->pluck('name','id');
    	$service_types=DB::table('service_types')->where('status',1)->pluck('name','id');
    	$pid="";
      $main_image="";
      $image_array=array();
    	return view('restaurant.add_restaurant',compact('pid','airports','service_types','main_image','image_array'));
    }
    public function getterminal(Request $request)
    {
    	 
  
  	 	$ParentID = $request->q;
      $airports =Airportterminals::with('terminal')->where('status', 1)->where('airport_id', $ParentID)->get();
      $result = "";
        foreach($airports as $k=>$v)
        {
          if($v->terminal->status ==1)
           $result .="<option value='".$v->terminal->id."'>".$v->terminal->name."</option>";

        }
    
        return $result;
  
    }
    public function saverestaurant(Request $request)
    {
        //dd($request->all());
        $picture_single="";
        $multiple_image="";

        $deleted_image = $request->has('deleted_hidden_image') ? explode(",",$request->input('deleted_hidden_image')) : array();
        $hidden_image = $request->has('hidden_image') ? explode(",",$request->input('hidden_image')) : array();
        if(count($hidden_image) > 0)
        {
            $finalresult = array_diff($hidden_image, $deleted_image);
            $multiple_image=implode(",",$finalresult);
        }

        $uploaded_count = $request->input('hiddencount');
        for($i=1;$i<$uploaded_count;$i++)
        {
          if($i==2 && count($deleted_image)>0)
          {
              //$multiple_image = $request->input('Mediatype');
          }
          else
          {
            if($request->has('MediaFilesPathnew'.$i)) 
            {
              $files = $request->file('MediaFilesPathnew'.$i);
              if(count($files)>0)
              {
                foreach($files as $files_single)
                  {
                    $filename = $files_single->getClientOriginalName();
                    if(!in_array($filename,$deleted_image))
                    {
                        $extension = $files_single->getClientOriginalExtension();
                        $picture_single = date('YmdHis').$this->generateRandomString(4).".".$extension;
                        $multiple_image .=($multiple_image!="") ? ",".$picture_single : $picture_single;
                        $destinationPath = base_path() . '/public/uploads/restaurant';
                        $files_single->move($destinationPath, $picture_single);
                    }
                    if(in_array($filename,$deleted_image)){
                        $key = array_search($filename, $deleted_image);
                        unset($deleted_image[$key]);
                    } 
                }
              }
            }
          }
        }
        
        $id=$request->pid;

        if($id==0 || $id==null)
        $restaurant=new Restaurant;

        else
        $restaurant=Restaurant::where('id',$id)->where('status','!=',2)->first();

        $restaurant->airport_id=$request->airports;
        $restaurant->airport_terminal_id=$request->terminal;
        $restaurant->service_type_id=$request->type;
        $restaurant->shop_no=$request->shop_no;
        $restaurant->name=$request->name;
        $restaurant->phone=$request->phone;
        $restaurant->shop_image=$multiple_image;
        $restaurant->email=$request->email;
        $restaurant->landmark=$request->landmark;
        $restaurant->address=$request->address;
        $restaurant->acc_no=$request->acc_no;
        $restaurant->ifsc_code=$request->ifsc_code;
        $restaurant->commission =$request->commission;
  
        $restaurant->status=$request->status;
        $restaurant->created_by=Auth::user()->id;
        if($restaurant->save()){
            if($id!="")
            Rest_time::where('rest_id',$restaurant->id)->delete(); 

            if($request->sun_from!="" && $request->sun_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'sunday'; 
              $days->from_time = $request->sun_from; 
              $days->to_time = $request->sun_to; 
              $days->save(); 
            }
            if($request->mon_from!="" && $request->mon_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'monday'; 
              $days->from_time = $request->mon_from; 
              $days->to_time = $request->mon_to; 
              $days->save(); 
            }   
            if($request->tue_from!="" && $request->tue_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'tuesday'; 
              $days->from_time = $request->tue_from; 
              $days->to_time = $request->tue_to; 
              $days->save(); 
            }   
            if($request->wed_from!="" && $request->wed_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'Wednesday'; 
              $days->from_time = $request->wed_from; 
              $days->to_time = $request->wed_to; 
              $days->save(); 
            }   
            if($request->thu_from!="" && $request->thu_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'Thursday'; 
              $days->from_time = $request->mon_from; 
              $days->to_time = $request->mon_to; 
              $days->save(); 
            }   
            if($request->fr_from!="" && $request->fr_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'Friday'; 
              $days->from_time = $request->fr_from; 
              $days->to_time = $request->fr_to; 
              $days->save(); 
            }  
            if($request->sat_from!="" && $request->sat_to!=""){
              $days = new Rest_time; 
              $days->rest_id = $restaurant->id; 
              $days->day = 'saturday'; 
              $days->from_time = $request->sat_from; 
              $days->to_time = $request->sat_to; 
              $days->save(); 
            }
        }
        if($id=="")
         return redirect('/restaurants')->witherrors('Restaurant Added successfully');
         else
         return redirect('/restaurants')->witherrors('Restaurant Updated successfully');
    }
    public function edit($id)
    {
        //dd($id);
        $rest=Restaurant::find($id);
        $airports=DB::table('airports')->where('status',1)->pluck('name','id');
        $service_types=DB::table('service_types')->where('status',1)->pluck('name','id');
        $pid=$id;
        $airportterminals =DB::table('terminals')->where('status',1)->pluck('name','id');
 

         $days =Rest_time::where('rest_id',$id)->get();
        if($days->count()>0){
        $array=array();
          foreach($days as $d){
            if($d->day=='sunday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='monday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='tuesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Wednesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Thursday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Friday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='saturday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } 
          }
        }

        if($rest->shop_image!="")
        {
            $main_image = $rest->shop_image;
            $image_array=explode(",",$rest->shop_image);
        }
        else 
        {
            $main_image = "";
            $image_array = array();
        }

    //  dd($shop_image);
        
        return view('restaurant.add_restaurant',compact('pid','airports','service_types','rest','airportterminals','workingday','shop_image','main_image','image_array','array'));
    }
    
    public function generateRandomString($length=4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function deleteimage(Request $request)
    {
       $test = "1";
       return json_encode($test);
    }
    public function view($id)
    {
         $rest= Restaurant::with('airports','servicetype','terminal')->find($id);
       $id=$id;
         $days =Rest_time::where('rest_id',$id)->get();
        if($days->count()>0){
        $array=array();
          foreach($days as $d){
            if($d->day=='sunday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='monday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='tuesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Wednesday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Thursday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='Friday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } if($d->day=='saturday'){
              $array[$d->day]['from'] = $d->from_time;
              $array[$d->day]['to'] = $d->to_time;
            } 
          }
        }
        return view('restaurant.view',compact('rest','array','id'));

    }
    public function restdestroy($id)
    {
        $rest= Restaurant::find($id);
        $rest->status=2;
        $rest->save();
          return redirect('/restaurants')->with('message','Restaurant Deleted successfully');

    }

    public function exportFile(Request $request){
      $type="xls";
      $userid = Auth::user()->id;
      $userdetails = Users::where('id',$userid)->first(); 

      $restaurant = Restaurant::with(['airports','servicetype','terminal']);
      if($request->has('startdate') && $request->startdate!=null)
      {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $restaurant->where('created_at','>=',$startdate);
      }
      if($request->has('enddate') && $request->enddate!=null)
      {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $restaurant->where('created_at','<=',$enddate);
      }
      if($request->has('status'))
      {
        $status = $request->status;
        $restaurant->where('status','=',$status);
      }
      if($userdetails->user_type_id ==1)
          $products = $restaurant->get();
      if($userdetails->user_type_id ==2)
          $products = $restaurant->where('created_by',$userid)->get();

      $i=0;
      if(count($products)>0)
      {
        foreach($products as $product)
        {
          $result[$i]['Name'] = $product->name;
          $result[$i]['Airport'] = $product->airports->name;
          $result[$i]['Service Type'] = $product->servicetype->name;
          $result[$i]['Terminal'] = $product->terminal->name;
          $result[$i]['Phone'] = $product->phone;
          $result[$i]['Email'] = $product->email;
          $result[$i]['Shop Number'] = $product->shop_no;
          $result[$i]['Landmark'] = $product->landmark;
          $result[$i]['Address'] = $product->address;
          $i++;
        }
      }
      else
      {
          $result[$i]['Name'] = "";
          $result[$i]['Airport'] = "";
          $result[$i]['Service Type'] = "";
          $result[$i]['Terminal'] = "";
          $result[$i]['Phone'] = "";
          $result[$i]['Email'] = "";
          $result[$i]['Shop Number'] = "";
          $result[$i]['Landmark'] = "";
          $result[$i]['Address'] = "";
      }
      return \Excel::create('Restaurantreport', function($excel) use ($result) {
      $excel->sheet('sheet name', function($sheet) use ($result)
      {
          $sheet->fromArray($result);
      });
      })->download($type);
    }

    public function exportexist(Request $request){
      $userid = Auth::user()->id;
      $userdetails = Users::where('id',$userid)->first(); 
      $restaurant = Restaurant::with(['airports','servicetype','terminal']);
      if($request->has('startdate') && $request->startdate!=null)
      {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $restaurant->where('created_at','>=',$startdate);
      }
      if($request->has('enddate') && $request->enddate!=null)
      {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $restaurant->where('created_at','<=',$enddate);
      }
      if($request->has('status'))
      {
        $status = $request->status;
        $restaurant->where('status','=',$status);
      }

      if($userdetails->user_type_id ==1)
          $products = $restaurant->get();
      if($userdetails->user_type_id ==2)
          $products = $restaurant->where('created_by',$userid)->get();

      $i=0;
      if(count($products)>0)
        return "1";
        else
        return "0";
    }
  public function filter_list($id)
  {
     $userid = Auth::user()->id;
        if($userid!="")
        {
           $userdetails = Users::where('id',$userid)->first();

            $rest=Airports::where('id',$id)->where('status','!=',2)->first();
          // dd($rest);
            if(is_object($userdetails) && is_object($rest) )
            {
                  if($userdetails->user_type_id ==1)
                    $rest=Restaurant::where('status','!=',2)->where('airport_id',$id)->orderby('id','DESC')->get();
                  if($userdetails->user_type_id ==2)
                    $rest =Restaurant::where('status','!=',2)->where('airport_id',$id)->where('created_by',$userid)->orderBy('id','DESC')->get();
                   $airports=Airports::where('status',1)->pluck('name','id');
                  $airport_id=$id;
                  return view('restaurant.list',compact('rest','airports','airport_id'));
            }
             else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }

        }
        else
        return redirect('/login');
  }
  public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Restaurant::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }
}
