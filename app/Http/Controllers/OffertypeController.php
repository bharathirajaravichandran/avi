<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Offertype;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class OffertypeController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

	public function list()
	{
		$offertype=Offertype::where('status','!=',2)->get(); 

		return view('offertype.list', compact('offertype'));
	}
 	public function create()
    {
        //dd("Fdsf");
        $pid="";
        return view('offertype.create',compact('pid'));
    }   
    public function saveoffertype(Request $request)
    {
    	$id=$request->pid;
    	if($id=="")
    	  $offer=new Offertype;
    	else
    	$offer=Offertype::find($id); 
    	$offer->name=$request->name;
    	$offer->status=$request->status;
    	$offer->save();
    	if($id=="")
    	return redirect('offertype')->witherrors('Offertype Added successfully');
    	else
    	return redirect('offertype')->witherrors('Offertype Updated successfully');
    }
    public function edit($id)
    {
    	$offer=Offertype::find($id); 
    	        $pid=$id;
        return view('offertype.create',compact('pid','offer'));

    }
    public function view($id)
    {
    	$offer=Offertype::find($id); 
        $id=$id;
    	return view('offertype.view', compact('offer','id'));
    }
     public function offertypedestroy($id)
    {
          
         $offer= Offertype::find($id);
         $offer->status=2;
            $offer->save();
             //dd($country);
            return redirect()->back()->with('message','Offertype Deleted successfully');
        
    }
     public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Offertype::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
 }
