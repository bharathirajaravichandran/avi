<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Lounge;
use App\Models\Loungefacility;
use App\Models\Airports;
use App\Models\Facility;
use App\Models\Terminals;
use App\Models\Airportterminals;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class LoungeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',4)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: loungelist
    * desc: to list the Facility
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function loungelist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $loungelist = Lounge::with(['airport','terminal'])->where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $loungelist = Lounge::with(['airport','terminal'])->where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                $airports=Airports::where('status',1)->pluck('name','id');
                return view('loungelist',['loungelist'=>$loungelist,'userid'=>$userid,'airports'=>$airports]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Lounge List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addfacility
    * desc: to add the Facility
    * @param: none
    * method: none  
    * @return: return the Facility create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addlounge()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                $airport = Airports::where('status',1)->pluck('name','id');
                $lounge = Facility::where('status',1)->pluck('name','id');
                return view('addlounge',['id'=>"0",'airport'=>$airport,'facility'=>$lounge,'main_image'=>"",'image_array'=>array()]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Lounge Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editfacility
    * desc: to edit the Facility Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editlounge(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $loungedetails = Lounge::where('id',$id)->where('status','!=',2)->first();
                if(is_object($loungedetails))
                {
                    if($userdetails->user_type_id ==1 || $loungedetails->created_by == $userid)
                    {
                        if($loungedetails->images!="" && $loungedetails->images!='""')
                        {
                            $main_image = $loungedetails->images;
                            $image_array=explode(",",$loungedetails->images);
                        }
                        else 
                        {
                            $main_image = "";
                            $image_array = array();
                        }
                        $airport = Airports::where('status',1)->pluck('name','id');
                        $facility = Facility::where('status',1)->pluck('name','id');

                        $terminals = Airportterminals::with('terminal')->where("airport_id",$loungedetails->airportid)->where('status','1')->get();
                        $terminal = array();

                        if(count($terminals) > 0)
                        {
                            foreach($terminals as $terminals_list)
                            {
                                $terminal[$terminals_list->terminal->id]=$terminals_list->terminal->name;
                            }
                        }

                        $getloungefacility = Loungefacility::where('loungeid',$id)->get();
                        $selectedfacility = "[";
                        if(count($getloungefacility) > 0)
                        {
                            foreach($getloungefacility as $getlounge_facility)
                            {
                                $selectedfacility .= $getlounge_facility->facilityid.",";
                            }
                        }
                        $selectedfacility .= "]";

                        return view('addlounge',['id'=>$id,'loungedetails'=>$loungedetails,'airport'=>$airport,'facility'=>$facility,'main_image'=>$main_image,'image_array'=>$image_array,'terminal'=>$terminal,'selectedfacility'=>$selectedfacility]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>You dont have credentials to update this complianctype.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: savefacility
    * desc: to save the Facility Records
    * @param: facility,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function savelounge(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $picture_single="";
            $multiple_image="";

            $deleted_image = $request->has('deleted_hidden_image') ? explode(",",$request->input('deleted_hidden_image')) : array();
            $hidden_image = $request->has('hidden_image') ? explode(",",$request->input('hidden_image')) : array();
            if(count($hidden_image) > 0)
            {
                $finalresult = array_diff($hidden_image, $deleted_image);
                $multiple_image=implode(",",$finalresult);
            }

            $uploaded_count = $request->input('hiddencount');
            for($i=1;$i<$uploaded_count;$i++)
            {
                if($i==2 && count($deleted_image)>0)
                {
                  //$multiple_image = $request->input('Mediatype');
                }
                else
                {
                    if($request->has('MediaFilesPathnew'.$i)) 
                    {
                        $files = $request->file('MediaFilesPathnew'.$i);
                        if(count($files)>0)
                        {
                            foreach($files as $files_single)
                            {
                                $filename = $files_single->getClientOriginalName();
                                if(!in_array($filename,$deleted_image))
                                {
                                    $extension = $files_single->getClientOriginalExtension();
                                    $picture_single = date('YmdHis').$this->generateRandomString(4).".".$extension;
                                    $multiple_image .=($multiple_image!="") ? ",".$picture_single : $picture_single;
                                    $destinationPath = base_path() . '/public/uploads/lounge';
                                    $files_single->move($destinationPath, $picture_single);
                                }
                                if(in_array($filename,$deleted_image)){
                                    $key = array_search($filename, $deleted_image);
                                    unset($deleted_image[$key]);
                                } 
                            }
                        }
                    }
                }
            }

            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $lounge = new Lounge;
                $lounge->created_at = Carbon::now()->toDateTimeString();
                $lounge->created_by=$loginid;
                $check = 1;
                $message = "Lounge Created Successfully";
            }
            else
            {
                $lounge = Lounge::where('id',$id)->where('status','!=',2)->first();
                if(is_object($lounge))
                {
                    $lounge->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Lounge Updated Successfully";
                }
            }
            if($check==1)
            {
                $lounge->name=$request->name;
                $lounge->airportid=$request->airportid;
                $lounge->terminalid=$request->terminalid;
                $lounge->images=$multiple_image;
                $lounge->phone=$request->phone;
                $lounge->email=$request->email;
                $lounge->loungeid=$request->loungeid;
                $lounge->landmark=$request->landmark;
                $lounge->conditions=$request->conditions;
                $lounge->status=$request->status;
                $lounge->save();

                $loungeid[] = $lounge->id;
                $loungefacilties = $request->facilities;

                if($request->id!=0)
                DB::table('loungefacilty')->whereIn('loungeid', $loungeid)->delete();
    
                if(count($loungefacilties)>0)
                {
                    foreach($loungefacilties as $k=>$v)
                    {
                        $loungefacilty = new Loungefacility;
                        $loungefacilty->facilityid = $v;
                        $loungefacilty->loungeid = $lounge->id;
                        $loungefacilty->created_at = Carbon::now()->toDateTimeString();
                        $loungefacilty->save();
                    }
                }

                return redirect('/lounge')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Lounge Update failed</h3><p>You dont have credentials to update this lounge.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewlounge
    * desc: to view the Lounge Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 19.02.2019
    * 
    */

    public function viewlounge(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $loungedetails = Lounge::with(['airport','terminal'])->where('status','!=',2)->where('id',$id)->first();
        if(is_object($loungedetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $loungedetails->created_by == $loginid) 
            {
                $loungefacilty = Loungefacility::with('facility')->where('loungeid',$id)->get();
                $loungefacilties="";
                if(count($loungefacilty))
                {
                    foreach($loungefacilty as $lounge_facilty)
                    {
                        $loungefacilties .= ($loungefacilties!="") ? " , ".$lounge_facilty->facility->name : $lounge_facilty->facility->name;
                    }
                }
                $id=$id;
                return view('viewlounge',['loungedetails'=>$loungedetails,'loungefacilties'=>$loungefacilties,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Lounge failed</h3><p>You dont have credentials to view this lounge.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Lounge failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deletelounge
    * desc: to delete the Lounge Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deletelounge(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $loungedetails = Lounge::where('status','!=',2)->where('id',$id)->first();
            if(is_object($loungedetails))
            {
                if($userdetails->user_type_id ==1 || $loungedetails->created_by == $loginid)
                {
                    $loungedetails->status = 2;
                    $loungedetails->save();
                    $loungeid[] = $id;
                    DB::table('loungefacilty')->whereIn('loungeid', $loungeid)->delete();

                    return redirect('/lounge')->with('message','Lounge Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Lounge failed</h3><p>You dont have credentials to view this lounge.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Lounge failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: facilityexist
    * desc: to check whether the record is already exist or not.
    * @param: facility,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function Loungeexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $loungeid = $request->post('loungeid');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Lounge::where('loungeid','=',trim($loungeid))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }   

    public function getTerminal(Request $request)
    {
        $terminals = Airportterminals::with('terminal')->where("airport_id",$request->airportid)->where('status','1')->get();
        $resultset = array();

        if(count($terminals) > 0)
        {
            foreach($terminals as $terminals_list)
            {
                $resultset[$terminals_list->terminal->id]=$terminals_list->terminal->name;
            }
        }
        return response()->json($resultset);
    }

    public function generateRandomString($length=4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function deleteimage(Request $request)
    {
       $test = "1";
       return json_encode($test);
    }

    public function exportFile(Request $request){
      $type="xls";
      $userid = Auth::user()->id;
      $userdetails = Users::where('id',$userid)->first(); 

      $lounge = Lounge::with(['airport','terminal']);
      if($request->has('startdate') && $request->startdate!=null)
      {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $lounge->where('created_at','>=',$startdate);
      }
      if($request->has('enddate') && $request->enddate!=null)
      {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $lounge->where('created_at','<=',$enddate);
      }
      if($request->has('status'))
      {
        $status = $request->status;
        $lounge->where('status','=',$status);
      }

      if($userdetails->user_type_id ==1)
        $products = $lounge->get();
      if($userdetails->user_type_id ==2)
        $products = $lounge->where('created_by',$userid)->get(); 

      $i=0;
      if(count($products)>0)
      {

        foreach($products as $product)
        {
          $result[$i]['Name'] = $product->name;
          $result[$i]['Airport'] = $product->airport->name;
          $result[$i]['Terminal'] = $product->terminal->name;
          $result[$i]['Phone'] = $product->phone;
          $result[$i]['Email'] = $product->email;
          $result[$i]['Lounge ID'] = $product->loungeid;
          $result[$i]['Landmark'] = $product->landmark;
          $result[$i]['Conditions'] = $product->conditions;
          $i++;
        }
      }
      else
      {
        $result[$i]['Name'] = "";
        $result[$i]['Airport'] = "";
        $result[$i]['Terminal'] = "";
        $result[$i]['Phone'] = "";
        $result[$i]['Email'] = "";
        $result[$i]['Lounge ID'] = "";
        $result[$i]['Landmark'] = "";
        $result[$i]['Conditions'] = "";
      }
      return \Excel::create('Loungereport', function($excel) use ($result) {
      $excel->sheet('sheet name', function($sheet) use ($result)
      {
          $sheet->fromArray($result);
      });
      })->download($type);
    }

    public function exportexist(Request $request){
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first(); 
        $lounge = Lounge::with(['airport','terminal']);
        if($request->has('startdate') && $request->startdate!=null)
        {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $lounge->where('created_at','>=',$startdate);
        }
        if($request->has('enddate') && $request->enddate!=null)
        {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $lounge->where('created_at','<=',$enddate);
        }
        if($request->has('status'))
        {
        $status = $request->status;
        $lounge->where('status','=',$status);
        }
        
        if($userdetails->user_type_id ==1)
            $products = $lounge->get();
        if($userdetails->user_type_id ==2)
            $products = $lounge->where('created_by',$userid)->get(); 

        $i=0;
        if(count($products)>0)
        return "1";
        else
        return "0";
    }
     public function lupload()
    {
        return view('loungeupload');
    }
     public function uploadcsv(Request $request)
    {
        $file = $request->file('csvfile');
        //dd($file);
        if($file && $file->getClientOriginalExtension() == 'csv'){

                $destinationPath = 'uploads/csv';
                $csvfile = time().'-csv.'.$file->getClientOriginalExtension();
                $file->move($destinationPath,$csvfile);
                $file_content = fopen("uploads/csv/".$csvfile,"r");
                $lined = 1;  //for first line not upload

          while($line = fgetcsv($file_content))      //getting file content
          {
            $rowcount=count($file);
            if($rowcount<=1)
            {
                  unlink($destinationPath.'/'.$csvfile);
                                     return redirect('lounge')
                                         ->with('message','File is Empty');

            }
            
            if(!empty($line[0]) && !empty($line[1]) &&  !empty($line[2]) && !empty($line[3]) && !empty($line[4]))
              {
                $linecount=count($line);
                if($linecount==5)
                {    
                        $length1=strlen($line[0]);       //getting the column value size
                        $length2=strlen($line[1]);
                        $length3=strlen($line[2]);
                        $length4=strlen($line[3]);
                        $length5=strlen($line[4]);
                      
                        if($lined==1)
                            {
                                $name='Name';
                                $phone='Phone';
                                $email='Email';
                                $loungeid='LoungesID';
                                $landmark='Landmark';
                            
                                if($name!=ucfirst(strtolower($line[0])) || $phone!=ucfirst(strtolower($line[1])) || $email!=ucfirst(strtolower($line[2])) || $loungeid!=ucfirst(strtolower($line[3]))|| $landmark!=ucfirst(strtolower($line[4])))
                                {
                                     unlink($destinationPath.'/'.$csvfile);
                                     return redirect('lounge')
                                         ->with('message','Please upload valid CSV file');
                                }
                            }
                }
                else
                {
                    unlink($destinationPath.'/'.$csvfile);
                    return redirect('lounge')->with('message','Please upload valid CSV file');
                }
 
              
                if($lined!=1)
                {

                    if($length1<=100 && $length2<=15 &&$length2>=8 && $length3<=50 && $length4<=50 && $length5<=255)           //length of column value should be less or equal 255
                    {
                        $lounge=Lounge::where('status','!=',2)->where('code',$line[3])->count();
                        if($lounge==0)
                        {
                            $lounge= new Lounge;
                            $lounge->name =$line[0];
                            $lounge->phone =$line[1];
                            $lounge->email =$line[2];
                            $lounge->loungeid =$line[3];
                            $lounge->landmark =$line[4];
                            $lounge->status =0;
                            $lounge->created_by=Auth::user()->id;
                            $lounge->save();
                        }
                         else
                        {
                            $data[]=$lined;
                        }

                    }
                     else
                    {
                     $data[]=$lined;
                    }

                }

              }

              else
              {
                 $data[]=$lined;
              }

                $lined++;
          }
            fclose($file_content);
            unlink($destinationPath.'/'.$csvfile);
            if(empty($data))
                return redirect('lounge')->withErrors('Lounge Uploaded successfully');
            else
            {
                $datas=implode(',',$data);
                return redirect('lounge')->with('message','Lines'.' '.$datas.' '.'are not uploaded');
            }

        }
        else
        {
         return redirect('lounge')->with('message','Please upload CSV file');
        }
        
    }
    public function filter_list($id)
  {
     $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
              $loungelist =  Airports::where('id',$id)->where('status','!=',2)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3 && is_object($loungelist))
            {
                if($userdetails->user_type_id ==1)
                $loungelist = Lounge::with(['airport','terminal'])->where('status','!=',2)->where('airportid',$id)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $loungelist = Lounge::with(['airport','terminal'])->where('status','!=',2)->where('airportid',$id)->where('created_by',$userid)->orderby('id','DESC')->get();
                $airports=Airports::where('status',1)->pluck('name','id');
                 $airport_id=$id;
                return view('loungelist',['loungelist'=>$loungelist,'userid'=>$userid,'airports'=>$airports,'airport_id'=>$airport_id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Lounge List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
  }
   public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Lounge::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }  

}
