<?php

namespace App\Http\Controllers;
use App\Models\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;
use Auth;
use DB;
use Validator;

class CountryController extends Controller
{

    /**
     * Display a listing of the subjects.
     *
     * @return Illuminate\View\View 
     */
    public function index()
    {
        $country =Country::where('status','!=',2)->limit(10)->offset(0)->get() ;
        return view('country.list_country', compact('country'));

    }  

    public function listing(Request $request)
    {
        $data=array();
        
        if($request->order[0]['column'] == 0)
        {
          $ordervalue = "sortname";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->order[0]['column'] == 1)
        {
          $ordervalue = "name";
          $ordertype = $request->order[0]['dir'];
        }

         if($request->order[0]['column'] == 2)
        {
          $ordervalue = "phonecode";
          $ordertype = $request->order[0]['dir'];
        }

        if($request->search['value']=="")
        {
          $totalcountry =Country::where('status','!=',2)->count() ;
          $country =Country::where('status','!=',2)->orderBy($ordervalue,$ordertype)->limit($request->length)->offset($request->start)->get() ;
        }
        else
        {
          $searchval = $request->search['value'];

          $totalcountrylist  =Country::where('status','!=',2);
          $totalcountry = $totalcountrylist->where(function ($query) use ($searchval) {
            $query->orWhere('name', 'like', '%' . $searchval . '%')
            ->orWhere('sortname', 'like', '%' . $searchval . '%')
            ->orWhere('phonecode', 'like', '%' . $searchval . '%');
          })->count();

          
          $countrylist =Country::where('status','!=',2);

          $country = $countrylist->where(function ($query) use ($searchval) {
           $query->orWhere('name', 'like', '%' . $searchval . '%')
            ->orWhere('sortname', 'like', '%' . $searchval . '%')
            ->orWhere('phonecode', 'like', '%' . $searchval . '%');
          })->orderBy($ordervalue,$ordertype)->limit(10)->offset(0)->get();

        }


        foreach($country as $countrydata)
        {
            if($countrydata->status==1)
            $set = "checked";
            else 
            $set = "";
            $resultarray = array();
            $resultarray[] = $countrydata->sortname;
            $resultarray[] = $countrydata->name;
            $resultarray[] = $countrydata->phonecode;
            $resultarray[] = '<input type="checkbox" data-toggle="toggle" data-on="Active" data-off="In Active" data-onstyle="success" data-offstyle="danger" data-size="mini" class="statuschange" id="'.$countrydata->id.'"'.$set.'>';
            $resultarray[] = '<a data-toggle="tooltip" data-placement="top" title="Edit" href="'.url('/').'/countryedit/'.$countrydata->id.'" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></a>
                            <a data-toggle="tooltip" data-placement="top" title="View" href="'.url('/').'/countryview/'.$countrydata->id.'" class="btn btn-info btn-xs"><i class="fa fa-eye"></i>
                            </a>
                            <a href="#" class="btn btn-danger btn-xs waves-effect waves-light remove-record" data-toggle="modal" data-url="'.url('/').'/countrydestroy/'.$countrydata->id.'" data-id="'.$countrydata->id.'" data-target="&#35;custom-width-modal" title="Delete" data-placement="top"><i class="fa fa-trash-o remove-record"></i></a>';
            $data[] = $resultarray;
        }

        $json_data = array(
                "draw"            => intval( $request->draw),
                "recordsTotal"    => intval( $totalcountry ),
                "recordsFiltered" => intval( $totalcountry ),
                "data"            => $data
            );

        echo json_encode($json_data);
    }
    public function create()
    {
        //dd("Fdsf");
        $pid="";
        return view('country.create',compact('pid'));
    }

    public function savecountry(Request $request)
    {
         /*dd($request);*/
            $c_name = strtolower($request->name);
            $name=ucfirst($c_name);
            $code=strtolower($request->code);
            $status=1;
            $id=$request->pid;
            //dd($id);
            if($id=="")
            $countrynew=new Country;
            else
            $countrynew=Country::find($id);
            

            $countrynew->name=$name;
            $countrynew->sortname=$code;
            $countrynew->phonecode=$request->phonecode;
            $countrynew->status=$status;
            $countrynew->save();
  /*          dd($countrynew);*/
            //Country::create($data);
             if($id=="")
            return redirect('/country')
                             ->witherrors('Country Added successfully');
              else
            return redirect('/country')
                             ->witherrors('Country Updated successfully');

        
    }
    public function edit($id)
    {
        $country = Country::findOrFail($id);
        // dd($country);
        $pid=$id;
        return view('country.create', compact('country','pid'));
    }


     public function delete($id)
    {
          
         $country= Country::find($id);
         $country->status=2;
            $country->save();
             //dd($country);
            return redirect()->back()->with('message','Country Deleted successfully');
        
    }
    
    /**
     * Show the form for creating a new subject.
     *
     * @return Illuminate\View\View
     */
    public function  view($id)
    {
        $country= Country::find($id);
        $id=$id;
        return view('country.view_country', compact('country','id'));
    }
    public function statusupdate(Request $request)
    {

        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Country::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    

}
