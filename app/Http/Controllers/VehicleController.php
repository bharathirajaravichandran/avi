<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Vehicle;
use Illuminate\Support\Facades\Auth;
use App\Models\Transport;

class VehicleController extends Controller
{
     public function list()
    {
    	$userid = Auth::user()->id;
    	$userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            
                if($userdetails->user_type_id ==1)
                	$vehicle = Vehicle::where('status','!=',2)->orderby('id','DESC')->get();
                else
                	  $vehicle = Vehicle::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
          			return view('vehicle.list',['vehicle'=>$vehicle]); 
     				   
          
    }
      public function create()
    {
    	$userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            $pid="";
            if(is_object($userdetails))
            {
            	$transport=Transport::where('status',1)->pluck('name','id');
                return view('vehicle.create',['pid'=>$pid,'transport'=>$transport]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Retail Type Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
        public function savevehicle(Request $request)
    {
    	$loginid = Auth::user()->id;
    	if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->pid;
            if($id=="" || $id==null)
            {
                $vehicle = new Vehicle;
               // $servicetype->created_at = Carbon::now()->toDateTimeString();
                $vehicle->created_by=$loginid;
                $check = 1;
                $message = "Vehicle Created Successfully";
            }
            else
            {
                $vehicle = Vehicle::where('id',$id)->where('status','!=',2)->first();
                if(is_object($vehicle))
                {
                    //$servicetype->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Vehicle Updated Successfully";
                }
            }
              if($check==1)
            {
                $vehicle->name=trim($request->name);
                $vehicle->transport_id=$request->transport;
                $vehicle->status=$request->status;
                $vehicle->save();
                return redirect('/vehicle')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Transport Update failed</h3><p>You dont have credentials to update this Vehicle type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }

        }
    }
     public function edit(Request $request,$id)
    {
    	$userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $vehicle = Vehicle::where('id',$id)->where('status','!=',2)->first();
                if(is_object($vehicle))
                {
                    if($userdetails->user_type_id ==1 || $vehicle->created_by == $userid)
                    {
                    	$pid=$id;
                    	$transport=Transport::where('status',1)->pluck('name','id');
                        return view('vehicle.create',['pid'=>$pid,'transport'=>$transport,'vehicle'=>$vehicle]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>You dont have credentials to update this service type.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    public function view(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $vehicle = Vehicle::with('transport')->where('status','!=',2)->where('id',$id)->first();
        if(is_object($vehicle) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $vehicle->created_by == $loginid)
            {
                $id=$id;
                return view('vehicle.view',['vehicle'=>$vehicle,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Vehicle failed</h3><p>You dont have credentials to view this service.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Vehicle failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }
     public function vehicledestroy(Request $request,$id)
    {
    	  $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $Vehicle = Vehicle::where('status','!=',2)->where('id',$id)->first();
            if(is_object($Vehicle))
            {
                if($userdetails->user_type_id ==1 || $Vehicle->created_by == $loginid)
                {
                    $vehicle = Vehicle::where('status','!=',2)->where('id',$id)->first();
                    $vehicle->status = 2;
                    $vehicle->save();
                    return redirect('/vehicle')->with('message','Vehicle Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Vehicle failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Vehicle failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Vehicle::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
    

}
