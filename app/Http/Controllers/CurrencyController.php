<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;

class CurrencyController extends Controller
{
    public function currency()
    {
    	$currency=Currency::where('status','!=',2)->get();
    	return view('currency.list_currency', compact('currency'));
    }
    public function create()
    {
    	$pid="";
    	return view('currency.create', compact('pid'));
    }

    public function savecurrency(Request $request)
    {
    	//dd($request->all());
    	  $c_name = strtolower($request->name);
            $name=ucfirst($c_name);
         
            
            $id=$request->pid;
            //dd($id);
            if($id=="")
            $currency=new Currency;
            else
            $currency=Currency::find($id);
            

            $currency->symbol=$request->symbol;
            $currency->code=$request->code;
            $currency->name=$name;
            $currency->status=$request->status;
            $currency->save();
  /*          dd($countrynew);*/
            //Country::create($data);
             if($id=="")
            return redirect('/currency')
                             ->witherrors('Currency Added successfully');
              else
            return redirect('/currency')
                             ->witherrors('Currency Updated successfully');

    }
    public function edit($id)
    {
    	$currency=Currency::find($id);
    	$pid=$id;
    	return view('currency.create', compact('currency','pid'));

    }
    public function destroy($id)
    {
         $currency= Currency::find($id);
         $currency->status=2;
            $currency->save();
             //dd($country);
            return redirect()->back()->with('message','Currency Deleted successfully');
    }
     public function view($id)
    {
    	$currency=Currency::find($id);
        $id=$id;
    	return view('currency.view_currency', compact('currency','id'));

    }
    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Currency::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
