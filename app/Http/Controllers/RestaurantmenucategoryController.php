<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Restaurantmenucategory;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Config;
use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class RestaurantmenucategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',3)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: restaurantmenucategorylist
    * desc: to list the restaurantmenucategory
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function restaurantmenucategorylist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $restaurantmenucategorylist = Restaurantmenucategory::where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $restaurantmenucategorylist = Restaurantmenucategory::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                return view('restaurantmenucategorylist',['restaurantmenucategorylist'=>$restaurantmenucategorylist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant Menu Category List failed</h3><p>This is not a valid data</p>';
                return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addrestaurantmenucategory
    * desc: to add the restaurantmenucategory
    * @param: none
    * method: none  
    * @return: return the restaurantmenucategory create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addrestaurantmenucategory()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                return view('addrestaurantmenucategory',['id'=>"0"]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant Menu Category Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editrestaurantmenucategory
    * desc: to edit the restaurantmenucategory Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editrestaurantmenucategory(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $restaurantmenucategorydetails = Restaurantmenucategory::where('id',$id)->where('status','!=',2)->first();
                if(is_object($restaurantmenucategorydetails))
                {
                    if($userdetails->user_type_id ==1 || $restaurantmenucategorydetails->created_by == $userid)
                    {
                        return view('addrestaurantmenucategory',['id'=>$id,'restaurantmenucategorydetails'=>$restaurantmenucategorydetails]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Restaurant Menu Category failed</h3><p>You dont have credentials to update this Restaurant Menu Category.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Restaurant Menu Category failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Restaurant Menu Category failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: saverestaurantmenucategory
    * desc: to save the restaurantmenucategory Records
    * @param: restaurantmenucategory,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function saverestaurantmenucategory(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $restaurantmenucategory = new Restaurantmenucategory;
                $restaurantmenucategory->created_at = Carbon::now()->toDateTimeString();
                $restaurantmenucategory->created_by=$loginid;
                $check = 1;
                $message = "Restaurant Menu Category Created Successfully";
            }
            else
            {
                $restaurantmenucategory = Restaurantmenucategory::where('id',$id)->where('status','!=',2)->first();
                if(is_object($restaurantmenucategory))
                {
                    $restaurantmenucategory->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Restaurant Menu Category Updated Successfully";
                }
            }
            if($check==1)
            {
                $restaurantmenucategory->name=trim($request->restaurantmenucategory);
                $restaurantmenucategory->status=$request->status;
                $restaurantmenucategory->save();
                return redirect('/restaurantmenucategory')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant Menu Category Update failed</h3><p>You dont have credentials to update this Restaurant Menu Category.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewrestaurantmenucategory
    * desc: to view the restaurantmenucategory Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function viewrestaurantmenucategory(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $restaurantmenucategorydetails = Restaurantmenucategory::where('status','!=',2)->where('id',$id)->first();
        if(is_object($restaurantmenucategorydetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $restaurantmenucategorydetails->created_by == $loginid)
            {
                $id=$id;
                return view('viewrestaurantmenucategory',['restaurantmenucategorydetails'=>$restaurantmenucategorydetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Restaurant Menu Category failed</h3><p>You dont have credentials to view this Restaurant Menu Category.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Restaurant Menu Category failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deleterestaurantmenucategory
    * desc: to delete the restaurantmenucategory Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deleterestaurantmenucategory(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $restaurantmenucategorydetails = Restaurantmenucategory::where('status','!=',2)->where('id',$id)->first();
            if(is_object($restaurantmenucategorydetails))
            {
                if($userdetails->user_type_id ==1 || $restaurantmenucategorydetails->created_by == $loginid)
                {
                    $restaurantmenucategorydetails = Restaurantmenucategory::where('status','!=',2)->where('id',$id)->first();
                    $restaurantmenucategorydetails->status = 2;
                    $restaurantmenucategorydetails->save();
                    return redirect('/restaurantmenucategory')->with('message','Restaurant Menu Category Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Restaurant Menu Category failed</h3><p>You dont have credentials to view this Restaurant Menu Category.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Restaurant Menu Category failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: restaurantmenucategoryexist
    * desc: to check whether the record is already exist or not.
    * @param: restaurantmenucategory,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function restaurantmenucategoryexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $restaurantmenucategoryname = $request->post('restaurantmenucategory');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Restaurantmenucategory::where('name','=',trim($restaurantmenucategoryname))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }    
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Restaurantmenucategory::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
