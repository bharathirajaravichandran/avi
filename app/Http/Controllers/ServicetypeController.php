<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Servicetype;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class ServicetypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',7)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: servicetypelist
    * desc: to list the Service Type
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function servicetypelist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $servicetypelist = Servicetype::where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $servicetypelist = Servicetype::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                return view('servicetypelist',['servicetypelist'=>$servicetypelist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Sertice Type List failed</h3><p>This is not a valid data</p>';
                return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addservicetype
    * desc: to add the Service Type
    * @param: none
    * method: none  
    * @return: return the Service Type create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addservicetype()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                return view('addservicetype',['id'=>"0"]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Service Type Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editcompliance
    * desc: to edit the Compliance Type Records
    * @param: $id
    * method: GET  
    * @return: return the compliance type edit form or or redirect to falilue page based on condition.
    * ReCreated by JK on 18.01.2019
    * 
    */
    public function editservicetype(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $servicetypedetails = Servicetype::where('id',$id)->where('status','!=',2)->first();
                if(is_object($servicetypedetails))
                {
                    if($userdetails->user_type_id ==1 || $servicetypedetails->created_by == $userid)
                    {
                        return view('addservicetype',['id'=>$id,'servicetypedetails'=>$servicetypedetails]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Service Type failed</h3><p>You dont have credentials to update this service type.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Service Type failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Service Type failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: saveservicetype
    * desc: to save the Service Type Records
    * @param: servicetype,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function saveservicetype(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $servicetype = new Servicetype;
                $servicetype->created_at = Carbon::now()->toDateTimeString();
                $servicetype->created_by=$loginid;
                $check = 1;
                $message = "Service Type Created Successfully";
            }
            else
            {
                $servicetype = Servicetype::where('id',$id)->where('status','!=',2)->first();
                if(is_object($servicetype))
                {
                    $servicetype->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Service Type Updated Successfully";
                }
            }
            if($check==1)
            {
                $servicetype->name=trim($request->servicetype);
                $servicetype->status=$request->status;
                $servicetype->save();
                return redirect('/servicetype')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Service Type Update failed</h3><p>You dont have credentials to update this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewservicetype
    * desc: to view the Service Type Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function viewservicetype(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $servicetypedetails = Servicetype::where('status','!=',2)->where('id',$id)->first();
        if(is_object($servicetypedetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $servicetypedetails->created_by == $loginid)
            {
                $id=$id;
                return view('viewservicetype',['servicetypedetails'=>$servicetypedetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Service Type failed</h3><p>You dont have credentials to view this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Service Type failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deleteservicetype
    * desc: to delete the Service Type Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deleteservicetype(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $servicetypedetails = Servicetype::where('status','!=',2)->where('id',$id)->first();
            if(is_object($servicetypedetails))
            {
                if($userdetails->user_type_id ==1 || $servicetypedetails->created_by == $loginid)
                {
                    $servicetypedetails = Servicetype::where('status','!=',2)->where('id',$id)->first();
                    $servicetypedetails->status = 2;
                    $servicetypedetails->save();
                    return redirect('/servicetype')->with('message','Service Type Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Service Type failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Service Type failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: servicetypeexist
    * desc: to check whether the record is already exist or not.
    * @param: servicetype,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function servicetypeexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $servicetypename = $request->post('servicetype');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Servicetype::where('name','=',trim($servicetypename))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Servicetype::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
