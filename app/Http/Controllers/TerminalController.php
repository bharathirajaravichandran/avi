<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Terminals;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class TerminalController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',6)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: terminallist
    * desc: to list the Terminal
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function terminallist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $terminallist = Terminals::where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $terminallist = Terminals::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                return view('terminallist',['terminallist'=>$terminallist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Terminal List failed</h3><p>This is not a valid data</p>';
                return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addterminal
    * desc: to add the Terminal
    * @param: none
    * method: none  
    * @return: return the Terminal create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addterminal()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                return view('addterminal',['id'=>"0"]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Terminal Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editterminal
    * desc: to edit the Terminal Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editterminal(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $terminaldetails = Terminals::where('id',$id)->where('status','!=',2)->first();
                if(is_object($terminaldetails))
                {
                    if($userdetails->user_type_id ==1 || $terminaldetails->created_by == $userid)
                    {
                        return view('addterminal',['id'=>$id,'terminaldetails'=>$terminaldetails]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Terminal failed</h3><p>You dont have credentials to update this terminal.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Terminal failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Terminal failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: saveterminal
    * desc: to save the Terminal Records
    * @param: terminal,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function saveterminal(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $terminal = new Terminals;
                $terminal->created_at = Carbon::now()->toDateTimeString();
                $terminal->created_by=$loginid;
                $check = 1;
                $message = "Terminal Created Successfully";
            }
            else
            {
                $terminal = Terminals::where('id',$id)->where('status','!=',2)->first();
                if(is_object($terminal))
                {
                    $terminal->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Terminal Updated Successfully";
                }
            }
            if($check==1)
            {
                $terminal->name=trim($request->terminal);
                $terminal->status=$request->status;
                $terminal->save();
                return redirect('/terminal')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Terminal Update failed</h3><p>You dont have credentials to update this terminal.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewterminal
    * desc: to view the Terminal Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function viewterminal(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $terminaldetails = Terminals::where('status','!=',2)->where('id',$id)->first();
        if(is_object($terminaldetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $terminaldetails->created_by == $loginid)
            {
                $id=$id;
                return view('viewterminal',['terminaldetails'=>$terminaldetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Terminal failed</h3><p>You dont have credentials to view this terminal.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Terminal failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deleteterminal
    * desc: to delete the Terminal Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deleteterminal(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $terminaldetails = Terminals::where('status','!=',2)->where('id',$id)->first();
            if(is_object($terminaldetails))
            {
                if($userdetails->user_type_id ==1 || $terminaldetails->created_by == $loginid)
                {
                    $terminaldetails = Terminals::where('status','!=',2)->where('id',$id)->first();
                    $terminaldetails->status = 2;
                    $terminaldetails->save();
                    return redirect('/terminal')->with('message','Terminal Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Terminal failed</h3><p>You dont have credentials to view this terminal.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Terminal failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: terminalexist
    * desc: to check whether the record is already exist or not.
    * @param: terminal,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function terminalexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $terminalname = $request->post('terminal');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Terminals::where('name','=',trim($terminalname))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }
    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Terminals::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
