<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Modules;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class SubadminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',11)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: subadminlist
    * desc: to list the Subadmin users
    * @param: none
    * method: none  
    * @return: return the subadmin users list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 12.02.2019
    * 
    */
    public function subadminlist()
    {
       $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();

            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $subadminuserslist = Users::where('status','!=',2)->where('user_type_id',2)->where('id','!=',$userid)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $subadminuserslist = Users::where('status','!=',2)->where('user_type_id',2)->where('id','!=',$userid)->where('created_by',$userid)->orderby('id','DESC')->get();

                return view('subadminlist',['subadminuserslist'=>$subadminuserslist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Subadmin Users List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    } 
    /*
    * 
    * name: addsubadmin
    * desc: to add the Subadmin users
    * @param: none
    * method: none  
    * @return: return the subadmin user create form or redirect to falilue page based on condition.
    * Created by JK on 12.02.2019
    * 
    */
    public function addsubadmin()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                $modulelist = Modules::where('status',1)->pluck('name','id');
                return view('addsubadminuser',['id'=>"0",'modulelist'=>$modulelist,'selectedmodules'=>array()]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Subadmin Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editsubadmin
    * desc: to edit the Subadmin User Records
    * @param: $id
    * method: GET  
    * @return: return the subadmin user edit form or or redirect to falilue page based on condition.
    * ReCreated by JK on 12.02.2019
    * 
    */
    public function editsubadmin(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                $selectedmodules = array();
                $subadminuserdetail = Users::where('id',$id)->where('status','!=',2)->first();
                if(is_object($subadminuserdetail))
                {
                    if(($userdetails->user_type_id ==1 || $subadminuserdetail->created_by == $userid) && $id!=$userid)
                    {
                        $permissionlist = Userpermissions::where('user_id',$id)->get();
                        if(count($permissionlist) > 0)
                        {
                            foreach($permissionlist as $permission_list)
                            {
                                $selectedmodules[] = $permission_list->module_id;
                            }
                        }
                        $modulelist = Modules::where('status',1)->pluck('name','id');
                        return view('addsubadminuser',['id'=>$id,'subadminuserdetail'=>$subadminuserdetail,'modulelist'=>$modulelist,'selectedmodules'=>$selectedmodules]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Subadmin User failed</h3><p>You dont have credentials to update this user.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Subadmin user Update failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Subadmin Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: savesubadmin
    * desc: to save the Subadmin user Records
    * @param: firstname,email,phone,dob,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * ReCreated by JK on 12.02.2019
    * 
    */
    public function savesubadmin(Request $request)
    {
       // dd($request->all());
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $password = $this->generateRandomString(15);
                $subadminusers = new Users;
                $subadminusers->created_at = Carbon::now()->toDateTimeString();
                $subadminusers->password=bcrypt($password);
                $subadminusers->created_by=$loginid;
                $check = 1;
                $message = "Subadmin User Created Successfully";
            }
            else
            {
                $subadminusers = Users::where('id',$id)->where('status','!=',2)->first();
                if(is_object($subadminusers))
                {
                    $subadminusers->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Subadmin User Updated Successfully";
                }
            }
            if($check==1)
            {
                $subadminusers->first_name=trim($request->first_name);
                $subadminusers->phone=trim($request->phone);
                $subadminusers->dob=date("Y-m-d",strtotime($request->dob));
                if($request->has('email'))
                $subadminusers->email=trim($request->email);
                $subadminusers->status=$request->status;
                $subadminusers->user_type_id=2;
                $subadminusers->user_type=2;
                $subadminusers->is_admin=1;
                $subadminusers->save();

                $subadminid[] = $subadminusers->id;
                $moduleslist = $request->moduleslist;

                if($request->id!=0)
                DB::table('user_permissions')->whereIn('user_id', $subadminid)->delete();
    
                if(count($moduleslist)>0)
                {
                    foreach($moduleslist as $k=>$v)
                    {
                        $userpermission = new Userpermissions;
                        $userpermission->module_id = $v;
                        $userpermission->user_id = $subadminusers->id;
                        $userpermission->created_at = Carbon::now()->toDateTimeString();
                        $userpermission->status = 1;
                        $userpermission->save();
                    }
                }

                if($id==0)
                {
                    $data['userid'] = $subadminusers->id;
                    $data['password'] = $password;
                    $this->sentactivationlink($data);
                }
                return redirect('/subadmin')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Subadmin User Update failed</h3><p>You dont have credentials to update this user.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            } 
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewsubadmin
    * desc: to view the subadmin user Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * ReCreated by JK on 12.02.2019
    * 
    */

    public function viewsubadmin(Request $request,$id)
    {
        $loginid = Auth::user()->id;
          if($loginid!="")
          {
            $userdetails = Users::where('id',$loginid)->first();
            $subadminuserdetails = Users::where('status','!=',2)->where('id',$id)->first();
            if(is_object($subadminuserdetails) && $userdetails->user_type_id !=3)
            {
                if(($userdetails->user_type_id ==1 || $subadminuserdetails->created_by == $loginid) && $id!=$loginid)
                {
                    $privilegeslist = Userpermissions::with('module')->where('user_id',$id)->get();
                    if(count($privilegeslist))
                    {
                        $privileges="";
                        foreach($privilegeslist as $privileges_list)
                        {
                            $privileges .= ($privileges!="") ? " , ".$privileges_list->module->name : $privileges_list->module->name;
                        }
                    }
                    $id=$id;
                    return view('viewsubadmin',['subadminuserdetails'=>$subadminuserdetails,'privileges'=>$privileges,'id'=>$id]);
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Subadmin User failed</h3><p>You dont have credentials to view this user.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Subadmin users failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
         return redirect('/login');
    }

    /*
    * 
    * name: deletecompliancetype
    * desc: to delete the Compliance Type Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * ReCreated by JK on 12.02.2019
    * 
    */

    public function deletesubadmin(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $subadminuserdetails = Users::where('status','!=',2)->where('id',$id)->first();
            if(is_object($subadminuserdetails))
            {
                if(($userdetails->user_type_id ==1 || $subadminuserdetails->created_by == $loginid) && $id!=$loginid)
                {
                    $subadminuserdetails->status = 2;
                    $subadminuserdetails->save();
                    return redirect('/subadmin')->with('message','Subadmin User Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Subadmin User failed</h3><p>You dont have credentials to view this subadmin user.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Subadmin User failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: subadminuserexist
    * desc: to check whether the record is already exist or not.
    * @param: email,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 12.02.2019
    * 
    */

    public function subadminuserexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $subadminemail = $request->post('email');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Users::where('email','=',trim($subadminemail))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    } 

    //------------To generate random string by JK on Feb 11 starts----//

    public function generateRandomString($length=15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //------------To generate random string by JK on Feb 11 ends----//   

    /*
    * 
    * name: sentactivationlink
    * desc: to send mail to the respective mail.
    * @param: data
    * method: none
    * @return: 
    * Created by JK on 12.02.2019
    * 
    */ 
    public function sentactivationlink($data)
    {
        if(isset($data['userid']) && is_numeric($data['userid']) )
        {
            $userid = $data['userid'];
            $user = Users::where('id',$userid)->first();
            $data = array('name'=>$user->first_name,'password'=>$data['password'],'email'=>$user->email);

            Mail::send('registrationmail', $data, function($message) use ($user) {
            $message->to($user->email, $user->first_name)->subject('AVI: Subadmin Registration');
            $message->from('avi@dci.in','AVI');
            });
        }
    }

    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $airportcheck = Users::where('id',$id)->first();
        if(is_object($airportcheck))
        {
            $airportcheck->status = $status;
            $airportcheck->save();
            return "success";
        }
        else
        return "failed"; 
    }
}
