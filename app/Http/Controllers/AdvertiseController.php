<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Advertise;
use Auth;

class AdvertiseController extends Controller
{
    public function list()
    {
    	$advert=Advertise::where('status','!=',2)->get();
    	return view('advertise.list', compact('advert'));
    }
    public function create()
    {
    	$pid="";
    	return view('advertise.create', compact('pid'));
    }
    public function savead(Request $request)
    {
    	//dd($request->all());
    	$id=$request->pid;
    	if($id=="")
    	 $ad = new Advertise;
    	else
    	  $ad =Advertise::find($id);
    	$ad->title=$request->name;
    	$ad->description=$request->description;
        $ad->status=$request->status;
    	$ad->created_by=Auth::user()->id;

    	if(is_object($ad) && $ad->image!="")
            {
                $picture_single =$ad->image;
                
            }
            else 
            {
                $picture_single = "";
                
            } 

        if($_FILES['profile_img']['name']!='' )
          {

            $files_single = $request->file('profile_img');
            
            $filename = $files_single->getClientOriginalName();
            $extension = $files_single->getClientOriginalExtension();
            $picture_single = date('His').$filename;
            $destinationPath = base_path() . '/public/uploads/adimage';
            $files_single->move($destinationPath, $picture_single);
        }
         $ad->image=$picture_single;
         $ad->save();
         if($id=="")
         return redirect('/advertisement')->witherrors('Advertisement Added successfully');
         else
         return redirect('/advertisement')->witherrors('Advertisement Updated successfully');
    }

    public function addestroy($id)
    {
    	 $advertise=Advertise::find($id);
    	 $advertise->status=2;
    	   $advertise->save();
             //dd($country);
            return redirect()->back()->with('message','Advertisement Deleted successfully');
    }
    public function view($id)
    {
    	 $advertise=Advertise::find($id);
         $id=$id;
    	return view('advertise.view', compact('advertise','id'));
    }
    public function edit($id)
    {
    	 $advertise=Advertise::find($id);
    	 $pid=$id;
    	 return view('advertise.create', compact('pid','advertise'));
    }
     public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Advertise::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
