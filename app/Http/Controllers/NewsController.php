<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\News;
use Auth;

class NewsController extends Controller
{
   public function list()
    {
    	$news=News::where('status','!=',2)->get();
    	return view('news.list', compact('news'));
    }
     public function create()
    {
    	$pid="";
    	return view('news.create', compact('pid'));
    }
    public function savenews(Request $request)
    {
   		$id=$request->pid;
    	if($id=="")
    	 $news = new News;
    	else
    	  $news =News::find($id);
    	$news->title=$request->name;
    	$news->description=$request->description;
        $news->status=$request->status;
    	$news->created_by=Auth::user()->id;

    	if(is_object($news) && $news->image!="")
            {
                $picture_single =$news->image;
                
            }
            else 
            {
                $picture_single = "";
                
            } 

        if($_FILES['profile_img']['name']!='' )
          {

            $files_single = $request->file('profile_img');
            
            $filename = $files_single->getClientOriginalName();
            $extension = $files_single->getClientOriginalExtension();
            $picture_single = date('His').$filename;
            $destinationPath = base_path() . '/public/uploads/newsimage';
            $files_single->move($destinationPath, $picture_single);
        }
         $news->image=$picture_single;
         $news->save();
         if($id=="")
         return redirect('/news')->witherrors('News Added successfully');
         else
         return redirect('/news')->witherrors('News Updated successfully');
    }
     public function edit($id)
    {
    	 $news=News::find($id);
    	 $pid=$id;
    	 return view('news.create', compact('pid','news'));
    }
    public function view($id)
    {
    	 $news=News::find($id);
         $id=$id;
    	return view('news.view', compact('news','id'));
    }
    public function newsdestroy($id)
    {
    	 $news=News::find($id);
    	 $news->status=2;
    	   $news->save();
             //dd($country);
            return redirect()->back()->with('message','News Deleted successfully');
    }
    public function statusupdate(Request $request)
    {
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = News::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
