<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Restaurant;
use App\Models\Airports;
use App\Models\Foodtype;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1 ||$userdetails->user_type_id ==2)
            $allowed = 1;

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    public function index()
    {
       $users=Users::where('status',1)->where('user_type_id',3)->count();
       $restaurant=Restaurant::where('status',1)->count();
       $airports=Airports::where('status',1)->count();
      // $users=Users::where('created_at', '>=', Carbon::now()->startOfMonth())->get();
       $userPerMonth= array();
    for ($i=1; $i<=12; $i++){
         $userPerMonth[]=DB::table('users')->whereRaw('MONTH(created_at) ='.$i)->whereYear('created_at', Carbon::now()->year)->count();

    }
     // $monthdata="101,2,120,50,60,70,1,540,50,980,0,0";
    //  dd($monthdata);
        return view('dashboard',compact('users','restaurant','airports','userPerMonth'));
    }

    /*
    * 
    * name: editprofile
    * desc: to edit the Admin User Records
    * @param: $id
    * method: GET  
    * @return: return the admin user edit form or or redirect to falilue page based on condition.
    * Created by JK on 15.02.2019
    * 
    */
    public function editprofile(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                $adminuserdetail = Users::where('id',$id)->where('status','!=',2)->first();
                if(is_object($adminuserdetail))
                {
                    if($id==$userid)
                    {
                        return view('editprofile',['id'=>$id,'adminuserdetail'=>$adminuserdetail]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Admin User failed</h3><p>You dont have credentials to update this user.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Admin user Update failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Admin Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: saveprofile
    * desc: to update the profile
    * @param: no param
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 15.02.2019
    * 
    */
    public function saveprofile(Request $request)
    {
        //dd($request->all());
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
           
            $adminusers = Users::where('id',$id)->where('status','!=',2)->first();
            if(is_object($adminusers))
            {
                $adminusers->updated_at = Carbon::now()->toDateTimeString();
                $check = 1;
                $message = "Profile Updated Successfully";
            }

            if($check==1)
            {
                $adminusers->first_name=trim($request->first_name);
                $adminusers->phone=trim($request->phone);
                $adminusers->dob=date("Y-m-d",strtotime($request->dob));

                $files_single = $request->file('profileimg');
                $picture_single="";
                 if(count($files_single)>0)
                {
                    $extension = $files_single->getClientOriginalExtension();
                    $picture_single = date('YmdHis').$this->generateRandomString(4).".".$extension;
                    $destinationPath = base_path() . '/public/uploads/userimage/';
                    $files_single->move($destinationPath, $picture_single);
                }

                $adminusers->profile_img=($picture_single!="") ? $picture_single : $adminusers->profile_img;
                $adminusers->save();
                return redirect('/editprofile/'.$loginid)->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Admin User Update failed</h3><p>You dont have credentials to update this user.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            } 

        }
        else
        return redirect('/login');
    }

    public function updatepassword(Request $request)
    {
        $loginid = Auth::user()->id;
        $pwd = $request->post('newpassword');
        $pass = 0;
        if(is_numeric($loginid))
        {
            if($request->has('newpassword'))
            {
                $usercheck = Users::where('id',$loginid)->first();
                if(is_object($usercheck))
                {
                    $usercheck->password = bcrypt($pwd);
                    $usercheck->save();
                    $pass =1;
                    $message = 'Password Reset Successfully';
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>This User has not been registered with us</h3><p>Kindly <a href="register" style="color: #3993F4;font-weight: bold;">Register</a></p>';
                }
            }
        } 
        else
        {
            $type = 2;
            $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>This User has not been registered with us</h3><p>Kindly <a href="register" style="color: #3993F4;font-weight: bold;">Register</a></p>';
        }
        if($pass ==0)
        return view('changepassword');
        else
        return view('changepassword')->witherrors($message);;
    }

    public function generateRandomString($length=4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
















