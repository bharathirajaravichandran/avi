<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Offer;
use App\Models\Offertype;
use DB;
use Auth;
class OfferController extends Controller
{
    public function list()
    {
    	$offer=Offer::where('status',1)->get();
      //  dd(Auth::user()->id);
    	 return view('offer.list', compact('offer'));
    }
    public function create()
    {
    	$offertype=Offertype::where('status',1)->pluck('name','id');
    		$service_types=DB::table('service_types')->where('status',1)->pluck('name','id');	
            //dd($offertype);
    	 $pid="";
        return view('offer.create',compact('pid','offertype','service_types'));
    }
    public function saveoffer(Request $request)
    {
    	$id=$request->pid;
    	if($id=="")
    	$offer=new Offer;
    	else
    	$offer=Offer::find($id);

    	$offer->offer_type_id=$request->offertype;
    	$offer->offer_code=$request->promocode;
    	$offer->min_purchase_value=$request->min_pur_value;
    	$offer->discount_percentage=$request->discount;
    	$offer->max_discount=$request->max_amt;
    	$offer->offer_start_date=date("Y-m-d", strtotime($request->startdate));
    	$offer->offer_end_date=date("Y-m-d", strtotime($request->enddate));
    	$offer->conditions=$request->condition;
    	$offer->service_types=$request->service_types;
        $offer->title=$request->title;
    	$offer->flat_rate=$request->flat_rate;
    	$offer->notify_to_app=$request->notify;
            $offer->created_by=Auth::user()->id;

    	 if(is_object($offer) && $offer->image!="")
            {
                $picture_single =$offer->image;
                
            }
            else 
            {
                $picture_single = "";
                
            } 

        if($_FILES['profile_img']['name']!='' )
          {

            $files_single = $request->file('profile_img');
            
            $filename = $files_single->getClientOriginalName();
            $extension = $files_single->getClientOriginalExtension();
            $picture_single = date('His').$filename;
            $destinationPath = base_path() . '/public/uploads/offerimage';
            $files_single->move($destinationPath, $picture_single);
        }
         $offer->image=$picture_single;
         $offer->status=1;
         $offer->save();
          	if($id=="")
    	return redirect('offer')->witherrors('Offer Added successfully');
    	else
    	return redirect('offer')->witherrors('Offer Updated successfully');


    }
       public function edit($id)
    {
    	$offertype=Offertype::where('status',1)->pluck('name','id');
    		$service_types=DB::table('service_types')->where('status',1)->pluck('name','id');	
    	 $pid=$id;
    	 $offer=Offer::find($id);
         //dd($offer);

        return view('offer.create',compact('pid','offertype','service_types','offer'));
    }
    public function view($id)
    {
    	 $offer=Offer::with('offer_type')->find($id);
    	$id=$id;
        return view('offer.view',compact('offer','id'));
    }
    public function offerdestroy($id)
    {
    	 $offer=Offer::find($id);
    	 $offer->status=2;
    	   $offer->save();
             //dd($country);
            return redirect()->back()->with('message','Offer Deleted successfully');
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Offer::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
