<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use App\Models\Transport;
use Illuminate\Support\Facades\Auth;

class TransportController extends Controller
{
     public function list()
    {
    	$userid = Auth::user()->id;
    	$userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            
                if($userdetails->user_type_id ==1)
                	$translist = Transport::where('status','!=',2)->orderby('id','DESC')->get();
                else
                	  $translist = Transport::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
          			return view('transport_type.list',['translist'=>$translist]); 
     				   
          
    }
     public function create()
    {
    	$userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            $pid="";
            if(is_object($userdetails))
            {
                return view('transport_type.create',['pid'=>$pid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Retail Type Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    public function savetransport(Request $request)
    {
    	$loginid = Auth::user()->id;
    	if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->pid;
            if($id=="" || $id==null)
            {
                $retails = new Transport;
               // $servicetype->created_at = Carbon::now()->toDateTimeString();
                $retails->created_by=$loginid;
                $check = 1;
                $message = "Transport Type Created Successfully";
            }
            else
            {
                $retails = Transport::where('id',$id)->where('status','!=',2)->first();
                if(is_object($retails))
                {
                    //$servicetype->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Transport Type Updated Successfully";
                }
            }
              if($check==1)
            {
                $retails->name=trim($request->name);
                $retails->status=$request->status;
                $retails->save();
                return redirect('/transport')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Transport Update failed</h3><p>You dont have credentials to update this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }

        }
    }
    public function edit(Request $request,$id)
    {
    	$userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $transport = Transport::where('id',$id)->where('status','!=',2)->first();
                if(is_object($transport))
                {
                    if($userdetails->user_type_id ==1 || $transport->created_by == $userid)
                    {
                    	$pid=$id;
                        return view('transport_type.create',['pid'=>$pid,'transport'=>$transport]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>You dont have credentials to update this service type.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Transport Type failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
     public function view(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $transport = Transport::where('status','!=',2)->where('id',$id)->first();
        if(is_object($transport) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $transport->created_by == $loginid)
            {
                $id=$id;
                return view('transport_type.view',['transport'=>$transport,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Retail Type failed</h3><p>You dont have credentials to view this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Retail Type failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    public function transportdestroy(Request $request,$id)
    {
    	  $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $trans = Transport::where('status','!=',2)->where('id',$id)->first();
            if(is_object($trans))
            {
                if($userdetails->user_type_id ==1 || $trans->created_by == $loginid)
                {
                    $trans = Transport::where('status','!=',2)->where('id',$id)->first();
                    $trans->status = 2;
                    $trans->save();
                    return redirect('/transport')->with('message','Transport Type Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Retail Type failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('site.success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Retail Type failed</h3><p>This is not a valid data</p>';
              return view('site.success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Transport::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}
