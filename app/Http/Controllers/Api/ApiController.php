<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App;
use JWTFactory;
use JWTAuth;
use Validator;
use Response;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;
use App\Services\AviServices as AviServices;


class ApiController extends Controller
{
    private $request;
    private $AviServices;
    public function __construct(Request $request)
    {   
        $this->request  = $request;
        $this->AviServices = new AviServices();        
        $this->user = new User;
    }

    public function login()
    {
       return $this->AviServices->login($this->request);
    } 
    public function forgotPassword()
    {
       return $this->AviServices->forgotPassword($this->request);
    } 
    public function register()
    {
       return $this->AviServices->register($this->request);
    }

    public function test()
    {
       return $this->AviServices->test($this->request);
    }
}
