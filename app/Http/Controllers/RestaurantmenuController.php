<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Restaurantmenu;
use App\Models\Restaurant;
use App\Models\Currency;
use App\Models\Restaurantmenucategory;
use App\Models\Foodtype;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class RestaurantmenuController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',3)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: restaurantmenulist
    * desc: to list the Facility
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function restaurantmenulist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $restaurantmenulist = Restaurantmenu::with(['restaurantname','foodtype','restaurantmenucategory','currency'])->where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $restaurantmenulist = Restaurantmenu::with(['restaurantname','foodtype','restaurantmenucategory','currency'])->where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();

              $restaurant=Restaurant::where('status',1)->pluck('name','id');

                return view('restaurantmenulist',['restaurantmenulist'=>$restaurantmenulist,'userid'=>$userid,'restaurant'=>$restaurant]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurantmenu List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addfacility
    * desc: to add the Facility
    * @param: none
    * method: none  
    * @return: return the Facility create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addrestaurantmenu(Request $request)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                if($request->id!="")
                $resid = $request->id;
                else
                $resid = "";

                if($userdetails->user_type_id ==1)
                $restaurantlist = Restaurant::where('status',1)->pluck('name','id');
                else
                $restaurantlist = Restaurant::where('status',1)->where('created_by',$userid)->pluck('name','id'); 

                $foodtypelist = Foodtype::where('status',1)->pluck('name','id');
                $restaurantmenucategory = Restaurantmenucategory::where('status',1)->pluck('name','id');
                $currentlist = Currency::where('status',1)->pluck('symbol','id');

                return view('addrestaurantmenu',['id'=>"0",'restaurantlist'=>$restaurantlist,'foodtypelist'=>$foodtypelist,'restaurantmenucategory'=>$restaurantmenucategory,'currentlist'=>$currentlist,'main_image'=>"",'image_array'=>array(),'resid'=>$resid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant Menu Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editfacility
    * desc: to edit the Facility Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editrestaurantmenu(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $restaurantmenudetails = Restaurantmenu::where('id',$id)->where('status','!=',2)->first();
                if(is_object($restaurantmenudetails))
                {
                    if($userdetails->user_type_id ==1 || $restaurantmenudetails->created_by == $userid)
                    {
                        if($restaurantmenudetails->images!="" && $restaurantmenudetails->images!='""')
                        {
                            $main_image = $restaurantmenudetails->images;
                            $image_array=explode(",",$restaurantmenudetails->images);
                        }
                        else 
                        {
                            $main_image = "";
                            $image_array = array();
                        }

                        if($userdetails->user_type_id ==1)
                        $restaurantlist = Restaurant::where('status',1)->pluck('name','id');
                        else
                        $restaurantlist = Restaurant::where('status',1)->where('created_by',$userid)->pluck('name','id'); 

                        $foodtypelist = Foodtype::where('status',1)->pluck('name','id');
                        $restaurantmenucategory = Restaurantmenucategory::where('status',1)->pluck('name','id');
                        $currentlist = Currency::where('status',1)->pluck('symbol','id');

                        return view('addrestaurantmenu',['id'=>$id,'restaurantmenudetails'=>$restaurantmenudetails,'restaurantlist'=>$restaurantlist,'foodtypelist'=>$foodtypelist,'restaurantmenucategory'=>$restaurantmenucategory,'currentlist'=>$currentlist,'main_image'=>$main_image,'image_array'=>$image_array,'resid'=>""]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>You dont have credentials to update this complianctype.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: savefacility
    * desc: to save the Facility Records
    * @param: facility,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function saverestaurantmenu(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $picture_single="";
            $multiple_image="";

            $deleted_image = $request->has('deleted_hidden_image') ? explode(",",$request->input('deleted_hidden_image')) : array();
            $hidden_image = $request->has('hidden_image') ? explode(",",$request->input('hidden_image')) : array();
            if(count($hidden_image) > 0)
            {
                $finalresult = array_diff($hidden_image, $deleted_image);
                $multiple_image=implode(",",$finalresult);
            }

            $uploaded_count = $request->input('hiddencount');
            for($i=1;$i<$uploaded_count;$i++)
            {
                if($i==2 && count($deleted_image)>0)
                {
                  //$multiple_image = $request->input('Mediatype');
                }
                else
                {
                    if($request->has('MediaFilesPathnew'.$i)) 
                    {
                        $files = $request->file('MediaFilesPathnew'.$i);
                        if(count($files)>0)
                        {
                            foreach($files as $files_single)
                            {
                                $filename = $files_single->getClientOriginalName();
                                if(!in_array($filename,$deleted_image))
                                {
                                    $extension = $files_single->getClientOriginalExtension();
                                    $picture_single = date('YmdHis').$this->generateRandomString(4).".".$extension;
                                    $multiple_image .=($multiple_image!="") ? ",".$picture_single : $picture_single;
                                    $destinationPath = base_path() . '/public/uploads/restaurantmenu';
                                    $files_single->move($destinationPath, $picture_single);
                                }
                                if(in_array($filename,$deleted_image)){
                                    $key = array_search($filename, $deleted_image);
                                    unset($deleted_image[$key]);
                                } 
                            }
                        }
                    }
                }
            }

            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $restaurantmenu = new Restaurantmenu;
                $restaurantmenu->created_at = Carbon::now()->toDateTimeString();
                $restaurantmenu->created_by=$loginid;
                $check = 1;
                $message = "Restaurant Menu Created Successfully";
            }
            else
            {
                $restaurantmenu = Restaurantmenu::where('id',$id)->where('status','!=',2)->first();
                if(is_object($restaurantmenu))
                {
                    $restaurantmenu->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Restaurant Menu Updated Successfully";
                }
            }
            if($check==1)
            {
                $restaurantmenu->name=$request->name;
                $restaurantmenu->description=$request->description;
                $restaurantmenu->price=$request->price;
                $restaurantmenu->restaurant_id=$request->restaurant_id;
                $restaurantmenu->currency_id=$request->currency_id;
                $restaurantmenu->food_type_id=$request->food_type_id;
                $restaurantmenu->restaurant_menu_category_id=$request->restaurant_menu_category_id;
                $restaurantmenu->status=$request->status;
                $restaurantmenu->images=$multiple_image;
                $restaurantmenu->save();

                return redirect('/restaurantmenu')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurant Menu Update failed</h3><p>You dont have credentials to update this Restaurant Menu.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewrestaurantmenu
    * desc: to view the Restaurantmenu Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 19.02.2019
    * 
    */

    public function viewrestaurantmenu(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $restaurantmenudetails = Restaurantmenu::with(['restaurantname','foodtype','restaurantmenucategory','currency'])->where('status','!=',2)->where('id',$id)->first();
        if(is_object($restaurantmenudetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $restaurantmenudetails->created_by == $loginid) 
            {
                $id=$id;
                return view('viewrestaurantmenu',['restaurantmenudetails'=>$restaurantmenudetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Restaurant Menu failed</h3><p>You dont have credentials to view this restaurantmenu.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Restaurant Menu failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deleterestaurantmenu
    * desc: to delete the Restaurantmenu Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deleterestaurantmenu(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $restaurantmenudetails = Restaurantmenu::where('status','!=',2)->where('id',$id)->first();
            if(is_object($restaurantmenudetails))
            {
                if($userdetails->user_type_id ==1 || $restaurantmenudetails->created_by == $loginid)
                {
                    $restaurantmenudetails->status = 2;
                    $restaurantmenudetails->save();
                    return redirect('/restaurantmenu')->with('message','Restaurant Menu Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Restaurantmenu failed</h3><p>You dont have credentials to view this restaurantmenu.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Restaurantmenu failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: facilityexist
    * desc: to check whether the record is already exist or not.
    * @param: facility,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function Restaurantmenuexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $restaurantmenuid = $request->post('name');
        $restaurantid = $request->post('restaurant_id');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Restaurantmenu::where('name','=',trim($restaurantmenuid))->where('restaurant_id',$restaurantid)->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "failed";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }   

    public function getTerminal(Request $request)
    {
        $terminals = Airportterminals::with('terminal')->where("airport_id",$request->airportid)->where('status','1')->get();
        $resultset = array();

        if(count($terminals) > 0)
        {
            foreach($terminals as $terminals_list)
            {
                $resultset[$terminals_list->terminal->id]=$terminals_list->terminal->name;
            }
        }
        return response()->json($resultset);
    }

    public function generateRandomString($length=4) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function deleteimage(Request $request)
    {
       $test = "1";
       return json_encode($test);
    }

    public function exportFile(Request $request){
      $type="xls";
      $userid = Auth::user()->id;
      $userdetails = Users::where('id',$userid)->first(); 

      $restaurantmenu = Restaurantmenu::with(['airport','terminal']);
      if($request->has('startdate') && $request->startdate!=null)
      {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $restaurantmenu->where('created_at','>=',$startdate);
      }
      if($request->has('enddate') && $request->enddate!=null)
      {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $restaurantmenu->where('created_at','<=',$enddate);
      }
      if($request->has('status'))
      {
        $status = $request->status;
        $restaurantmenu->where('status','=',$status);
      }

      if($userdetails->user_type_id ==1)
        $products = $restaurantmenu->get();
      if($userdetails->user_type_id ==2)
        $products = $restaurantmenu->where('created_by',$userid)->get(); 

      $i=0;
      if(count($products)>0)
      {

        foreach($products as $product)
        {
          $result[$i]['Name'] = $product->name;
          $result[$i]['Airport'] = $product->airport->name;
          $result[$i]['Terminal'] = $product->terminal->name;
          $result[$i]['Phone'] = $product->phone;
          $result[$i]['Email'] = $product->email;
          $result[$i]['Restaurantmenu ID'] = $product->restaurantmenuid;
          $result[$i]['Landmark'] = $product->landmark;
          $result[$i]['Conditions'] = $product->conditions;
          $i++;
        }
      }
      else
      {
        $result[$i]['Name'] = "";
        $result[$i]['Airport'] = "";
        $result[$i]['Terminal'] = "";
        $result[$i]['Phone'] = "";
        $result[$i]['Email'] = "";
        $result[$i]['Restaurantmenu ID'] = "";
        $result[$i]['Landmark'] = "";
        $result[$i]['Conditions'] = "";
      }
      return \Excel::create('Restaurantmenureport', function($excel) use ($result) {
      $excel->sheet('sheet name', function($sheet) use ($result)
      {
          $sheet->fromArray($result);
      });
      })->download($type);
    }

    public function exportexist(Request $request){
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first(); 
        $restaurantmenu = Restaurantmenu::with(['airport','terminal']);
        if($request->has('startdate') && $request->startdate!=null)
        {
        $startdate = date("Y-m-d",strtotime($request->startdate));
        $restaurantmenu->where('created_at','>=',$startdate);
        }
        if($request->has('enddate') && $request->enddate!=null)
        {
        $enddate = date("Y-m-d",strtotime($request->enddate. "+1 days"));
        $restaurantmenu->where('created_at','<=',$enddate);
        }
        if($request->has('status'))
        {
        $status = $request->status;
        $restaurantmenu->where('status','=',$status);
        }
        
        if($userdetails->user_type_id ==1)
            $products = $restaurantmenu->get();
        if($userdetails->user_type_id ==2)
            $products = $restaurantmenu->where('created_by',$userid)->get(); 

        $i=0;
        if(count($products)>0)
        return "1";
        else
        return "0";
    }

    public function filter_list($id)
    {
             $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            $restaurantmenulist = Restaurant::where('status','!=',2)->where('id',$id)->first();
            //dd( $restaurantmenulist);
            
            if(is_object($userdetails) && $userdetails->user_type_id !=3 && is_object($restaurantmenulist))
            {
                if($userdetails->user_type_id ==1)
                $restaurantmenulist = Restaurantmenu::with(['restaurantname','foodtype','restaurantmenucategory','currency'])->where('status','!=',2)->where('restaurant_id',$id)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $restaurantmenulist = Restaurantmenu::with(['restaurantname','foodtype','restaurantmenucategory','currency'])->where('status','!=',2)->where('restaurant_id',$id)->where('created_by',$userid)->orderby('id','DESC')->get();
            
              $restaurant=Restaurant::where('status',1)->pluck('name','id');
               $restaurant_id=$id;

                return view('restaurantmenulist',['restaurantmenulist'=>$restaurantmenulist,'userid'=>$userid,'restaurant'=>$restaurant,'restaurant_id'=>$restaurant_id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Restaurantmenu List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');

    }
     public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Restaurantmenu::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
}

                 