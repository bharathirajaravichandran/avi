<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Foodtype;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class FoodtypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',12)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: foodtypelist
    * desc: to list the Food Type
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function foodtypelist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $foodtypelist = Foodtype::where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $foodtypelist = Foodtype::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                return view('foodtypelist',['foodtypelist'=>$foodtypelist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Food Type List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addfoodtype
    * desc: to add the Food Type
    * @param: none
    * method: none  
    * @return: return the Food Type create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addfoodtype()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                return view('addfoodtype',['id'=>"0"]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Food Type Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editfoodtype
    * desc: to edit the Food Type Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editfoodtype(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $foodtypedetails = Foodtype::where('id',$id)->where('status','!=',2)->first();
                if(is_object($foodtypedetails))
                {
                    if($userdetails->user_type_id ==1 || $foodtypedetails->created_by == $userid)
                    {
                        return view('addfoodtype',['id'=>$id,'foodtypedetails'=>$foodtypedetails]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Food Type failed</h3><p>You dont have credentials to update this complianctype.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Food Type failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Food Type failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: savefoodtype
    * desc: to save the Food Type Records
    * @param: foodtype,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function savefoodtype(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $foodtype = new Foodtype;
                $foodtype->created_at = Carbon::now()->toDateTimeString();
                $foodtype->created_by=$loginid;
                $check = 1;
                $message = "Food Type Created Successfully";
            }
            else
            {
                $foodtype = Foodtype::where('id',$id)->where('status','!=',2)->first();
                if(is_object($foodtype))
                {
                    $foodtype->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Food Type Updated Successfully";
                }
            }
            if($check==1)
            {
                $foodtype->name=trim($request->foodtype);
                $foodtype->status=$request->status;
                $foodtype->save();
                return redirect('/foodtype')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Food Type Update failed</h3><p>You dont have credentials to update this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewfoodtype
    * desc: to view the Food Type Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function viewfoodtype(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $foodtypedetails = Foodtype::where('status','!=',2)->where('id',$id)->first();
        if(is_object($foodtypedetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $foodtypedetails->created_by == $loginid)
            {
                $id=$id;
                return view('viewfoodtype',['foodtypedetails'=>$foodtypedetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Food Type failed</h3><p>You dont have credentials to view this compliance type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Food Type failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deletefoodtype
    * desc: to delete the Food Type Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deletefoodtype(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $foodtypedetails = Foodtype::where('status','!=',2)->where('id',$id)->first();
            if(is_object($foodtypedetails))
            {
                if($userdetails->user_type_id ==1 || $foodtypedetails->created_by == $loginid)
                {
                    $foodtypedetails = Foodtype::where('status','!=',2)->where('id',$id)->first();
                    $foodtypedetails->status = 2;
                    $foodtypedetails->save();
                    return redirect('/foodtype')->with('message','Food Type Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Food Type failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Food Type failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: foodtypeexist
    * desc: to check whether the record is already exist or not.
    * @param: foodtype,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function foodtypeexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $foodtypename = $request->post('foodtype');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Foodtype::where('name','=',trim($foodtypename))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Foodtype::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
        
}
