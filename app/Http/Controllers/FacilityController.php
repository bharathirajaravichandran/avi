<?php

namespace App\Http\Controllers;

use App\Models\Users;
use App\Models\Facility;
use App\Models\Userpermissions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


use Mail;
use Validator;
use Session;
use Redirect;
use clause;


class FacilityController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $userdetails= Auth::user();
            $allowed =0;

            if($userdetails->user_type_id ==1)
            $allowed = 1;
            if($userdetails->user_type_id ==2)
            {
                $permissioncheck = Userpermissions::where('user_id',$userdetails->id)->where('module_id',10)->first();
                if(is_object($permissioncheck))
                $allowed =1;
            }

            if($allowed == 1)
            return $next($request);
            else
            return redirect('/');
        });
    }

    /*
    * 
    * name: facilitylist
    * desc: to list the Facility
    * @param: none
    * method: none  
    * @return: return the service type list with neccessary actions or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function facilitylist()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1)
                $facilitylist = Facility::where('status','!=',2)->orderby('id','DESC')->get();
                if($userdetails->user_type_id ==2)
                $facilitylist = Facility::where('status','!=',2)->where('created_by',$userid)->orderby('id','DESC')->get();
                return view('facilitylist',['facilitylist'=>$facilitylist,'userid'=>$userid]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Facility List failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: addfacility
    * desc: to add the Facility
    * @param: none
    * method: none  
    * @return: return the Facility create form or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function addfacility()
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails))
            {
                return view('addfacility',['id'=>"0"]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Facility Create failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }
    /*
    * 
    * name: editfacility
    * desc: to edit the Facility Records
    * @param: $id
    * method: GET  
    * @return: return the food type edit form or or redirect to falilue page based on condition.
    * Created by JK on 07.02.2019
    * 
    */
    public function editfacility(Request $request,$id)
    {
        $userid = Auth::user()->id;
        if($userid!="")
        {
            $userdetails = Users::where('id',$userid)->first();
            if(is_object($userdetails) && $userdetails->user_type_id !=3)
            {
                $facilitydetails = Facility::where('id',$id)->where('status','!=',2)->first();
                if(is_object($facilitydetails))
                {
                    if($userdetails->user_type_id ==1 || $facilitydetails->created_by == $userid)
                    {
                        return view('addfacility',['id'=>$id,'facilitydetails'=>$facilitydetails]);
                    }
                    else
                    {
                      $type = 2;
                      $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>You dont have credentials to update this complianctype.</p>';
                        return view('success',['type'=>$type,'message'=>$message]);
                    }
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Update Facility failed</h3><p>This is not a valid data</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: savefacility
    * desc: to save the Facility Records
    * @param: facility,status
    * method: POST  
    * @return: return to the listing page with necessary popup message.
    * Created by JK on 07.02.2019
    * 
    */
    public function savefacility(Request $request)
    {
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $check = 0;
            $userdetails = Users::where('id',$loginid)->first();
            $id=$request->id;
            if($id==0 || $id==null)
            {
                $facility = new Facility;
                $facility->created_at = Carbon::now()->toDateTimeString();
                $facility->created_by=$loginid;
                $check = 1;
                $message = "Facility Created Successfully";
            }
            else
            {
                $facility = Facility::where('id',$id)->where('status','!=',2)->first();
                if(is_object($facility))
                {
                    $facility->updated_at = Carbon::now()->toDateTimeString();
                    $check = 1;
                    $message = "Facility Updated Successfully";
                }
            }
            if($check==1)
            {
                $facility->name=trim($request->facility);
                $facility->status=$request->status;
                $facility->save();
                return redirect('/facility')->witherrors($message);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Facility Update failed</h3><p>You dont have credentials to update this service type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: viewfacility
    * desc: to view the Facility Records
    * @param: $id
    * method: GET  
    * @return: return the view page of particual record or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function viewfacility(Request $request,$id)
    {
       $loginid = Auth::user()->id;
      if($loginid!="")
      {
        $userdetails = Users::where('id',$loginid)->first();
        $facilitydetails = Facility::where('status','!=',2)->where('id',$id)->first();
        if(is_object($facilitydetails) && $userdetails->user_type_id !=3)
        {
            if($userdetails->user_type_id ==1 || $facilitydetails->created_by == $loginid)
            {
                $id=$id;
                return view('viewfacility',['facilitydetails'=>$facilitydetails,'id'=>$id]);
            }
            else
            {
                $type = 2;
                $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Facility failed</h3><p>You dont have credentials to view this compliance type.</p>';
                return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        {
          $type = 2;
          $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>View Facility failed</h3><p>This is not a valid data</p>';
          return view('success',['type'=>$type,'message'=>$message]);
        }
      }
      else
        return redirect('/login');
    }

    /*
    * 
    * name: deletefacility
    * desc: to delete the Facility Records
    * @param: $id
    * method: GET  
    * @return: return to listing page with respective message or redirect to failure page with respective message.
    * Created by JK on 07.02.2019
    * 
    */

    public function deletefacility(Request $request,$id)
    {
        $id =  $request->id;
        $loginid = Auth::user()->id;
        if($loginid!="")
        {
            $userdetails = Users::where('id',$loginid)->first();
            $facilitydetails = Facility::where('status','!=',2)->where('id',$id)->first();
            if(is_object($facilitydetails) && $userdetails->user_type_id !=3)
            {
                if($userdetails->user_type_id ==1 || $facilitydetails->created_by == $loginid)
                {
                    $facilitydetails = Facility::where('status','!=',2)->where('id',$id)->first();
                    $facilitydetails->status = 2;
                    $facilitydetails->save();
                    return redirect('/facility')->with('message','Facility Deleted successfully');
                }
                else
                {
                    $type = 2;
                    $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Facility failed</h3><p>You dont have credentials to view this service type.</p>';
                    return view('success',['type'=>$type,'message'=>$message]);
                }
            }
            else
            {
              $type = 2;
              $message = '<h3 class="md_helo">Oops !</h3><img src="'.url('/').'/img/cross.png"><h3>Delete Facility failed</h3><p>This is not a valid data</p>';
              return view('success',['type'=>$type,'message'=>$message]);
            }
        }
        else
        return redirect('/login');
    }

    /*
    * 
    * name: facilityexist
    * desc: to check whether the record is already exist or not.
    * @param: facility,id
    * method: POST  
    * @return: return sucess or failure message
    * Created by JK on 07.02.2019
    * 
    */

    public function facilityexist(Request $request)
    {
        $userid = Auth::user()->id;
        $userdetails = Users::where('id',$userid)->first();
        $facilityname = $request->post('facility');
        $id = ($request->has('id')) ? $request->post('id') : 0;

        $checkexist=Facility::where('name','=',trim($facilityname))->where('status','!=',2)->whereNOTIn('id',[$id])->first();
        
        if($checkexist)
        {
            echo "false";
            exit;
        }
        else
        {
            echo "true";
            exit;
        }
    }
    public function statusupdate(Request $request)
    {
        //dd("sf");
        $id = $request->id;
        $status = $request->status=="false" ? 0 : 1;
        $check = Facility::where('id',$id)->first();
        if(is_object($check))
        {
            $check->status = $status;
            $check->save();
            return "success";
        }
        else
        return "failed";
    }    
        
}
