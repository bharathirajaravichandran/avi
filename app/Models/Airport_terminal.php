<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Airport_terminal extends Model
{
    protected $table="airport_terminals";
    public function terminals()
    {
    	  return $this->belongsTo(Terminals::class,'terminal_id');
    }
    }
