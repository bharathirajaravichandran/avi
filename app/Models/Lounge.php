<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Lounge extends Model
{
    protected $table = 'lounge';

    public function airport()
    {
    	return $this->belongsTo(Airports::class,'airportid');
    }

     public function terminal()
    {
    	return $this->belongsTo(Terminals::class,'terminalid');
    }

}
