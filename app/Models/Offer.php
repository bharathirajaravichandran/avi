<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table='coupons';

      public function offer_type()
    {
    	  return $this->belongsTo(Offertype::class,'offer_type_id');
    }
}
