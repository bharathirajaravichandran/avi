<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Loungefacility extends Model
{
    protected $table = 'loungefacilty';

    public function facility()
    {
    	return $this->belongsTo(Facility::class,'facilityid');
    }

}
