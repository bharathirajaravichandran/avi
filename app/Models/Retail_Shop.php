<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Retail_Shop extends Model
{
    protected $table="retail_shops";
   

    public function airports()
    {
    	  return $this->belongsTo(Airports::class,'airport_id');
    } 
    public function retailtype()
    {
    	  return $this->belongsTo(Retail_type::class,'retail_type_id');
    }
     public function terminal()
    {
    	  return $this->belongsTo(Terminals::class,'airport_terminal_id');
    }

}
