<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Airports extends Model
{
    protected $table = 'airports';

    public function country()
    {
    	return $this->belongsTo(Country::class,'country_id');
    }
    public function state()
    {
    	return $this->belongsTo(State::class,'state_id');
    }
    public function city()
    {
    	return $this->belongsTo(City::class,'city_id');
    }
}
