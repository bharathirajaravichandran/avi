<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tips extends Model
{
    protected $table='tips';

    public function airport()
    {
    	  return $this->belongsTo(Airports::class,'airport_id');
    }
}
