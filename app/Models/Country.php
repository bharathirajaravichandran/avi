<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    
    protected $table="countries";
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     *
    /**
     * The database table used by the model.
     *
     * @var string
     */
   // protected $table = 'countries';

    /**
    * The database primary key value.
    *
    * @var string
    */
  
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    // protected $dates = [];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [];
    
    // /**
    //  * Get the city for this model.
    //  */
    // public function city()
    // {
    //     return $this->hasOne('App\Models\City','Country_id','Id');
    // }

    // /**
    //  * Get the parent for this model.
    //  */
    // public function parent()
    // {
    //     return $this->hasOne('App\Models\Parent','Country_id','Id');
    // }

    // /**
    //  * Get the staff for this model.
    //  */
    // public function staff()
    // {
    //     return $this->hasOne('App\Models\Staff','Country_id','Id');
    // }

    // /**
    //  * Get the state for this model.
    //  */
    // public function state()
    // {
    //     return $this->hasOne('App\Models\State','Country_id','Id');
    // }



}
