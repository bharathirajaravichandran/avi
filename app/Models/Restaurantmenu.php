<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurantmenu extends Model
{
    protected $table='restaurant_menu';

    public function restaurantname()
    {
    	  return $this->belongsTo(Restaurant::class,'restaurant_id');
    }
    public function currency()
    {
    	  return $this->belongsTo(Currency::class,'currency_id');
    }
    public function foodtype()
    {
    	  return $this->belongsTo(Foodtype::class,'food_type_id');
    }
    public function restaurantmenucategory()
    {
    	  return $this->belongsTo(Restaurantmenucategory::class,'restaurant_menu_category_id');
    }

}
