<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model
{
    protected $table='restaurants';

     public function airports()
    {
    	  return $this->belongsTo(Airports::class,'airport_id');
    } 
    public function servicetype()
    {
    	  return $this->belongsTo(Servicetype::class,'service_type_id');
    }
     public function terminal()
    {
    	  return $this->belongsTo(Terminals::class,'airport_terminal_id');
    }
}
