<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table='cities';

     public function country()
    {
    	  return $this->belongsTo(Country::class,'country_id');
    }
     public function state()
    {
    	  return $this->belongsTo(States::class,'state_id');
    }


}
