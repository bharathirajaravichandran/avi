<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
   protected $table='vehicle';    
   
    public function transport()
    {
    	  return $this->belongsTo(Transport::class,'transport_id');
    }
}
