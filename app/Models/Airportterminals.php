<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Airportterminals extends Model
{
    protected $table = 'airport_terminals';

    public function terminal()
    {
    	return $this->belongsTo(Terminals::class,'terminal_id');
    }
}
