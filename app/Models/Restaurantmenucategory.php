<?php

namespace App\Models;

use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class Restaurantmenucategory extends Model
{
    protected $table = 'restaurant_menu_categories';

}
